### Feature borrow books
A **borrower-record** (which i'ill call by `IO` in  this project) have **4 status**: **Borrowing** (It was using by someone), **Returned** (Someone borrowed it, and give they back after), **Missed** (Someone borrowed some books, just give back some books and miss some books), system will be update this `IO` with status `Missed` and auto create a new borrower-record for `missed-books`  (Or admin can mark it is **Unknown**, mean that is unknown status).

### Features will be used by only admin:
#### Create a new IO
Admin can only create a new record for a borrower only if he or she has no records with a `missed` status in the past.  
A new record will be create with `Borrowing` status.

#### Mark IO's status is unknown
IO with 'Borrowing' or 'Missed' can be updated to 'Unknown'. And admin can create new IO after.

_**Noted:** A message will be sent to slack to notify for everyone._

#### Revert unknown
In case `Mark wrong IO is unknown`, admin can revert it become to 'Missed' or 'Borrowing'.

_**Noted:** A message will be sent to slack to notify for everyone._

#### Returned books
When user gave books back, it will be one of cases:
- Number of `books-returned` == Number of `books-borrowed` -> Update status to `Returned`
- Number of `books-returned` < Number of `books-borrowed` -> Throw exception `You need to update it to 'Missed' or mark as 'Unknown'`
  - With action `is_force_missed=1` -> Update to **Missed**
  - With action `is_force_unknown=1` -> Update to **Unknown**

#### Get all IO
- Filter IO with accountId, accountName

### Feature(s) for admin and user:
#### Get IO by Id
Return user's IO (If permission is `user`) or return all of IO (if permission is `admin`).
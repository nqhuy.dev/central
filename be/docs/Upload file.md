### Feature upload file
#### Upload file in `e-book` module
File will be store in storage forever, until that e-book is deleted by author.

Step by step, FE will be handled to upload this file to `Firebase Storage`, and it will send a code to BE (code is a unique text for file, what was returned from Firebase Storage when we upload file), and BE save it with the e-book's information to database.

With trash file, i will setup a cron-job, run at 00:00 (every day) to check files in `Firebase Storage` and code (saved in database), and delete files (if the code is not exist in database).

[Model - Slide 0](https://docs.google.com/presentation/d/1aDb0nU47XsQgGhaxgIBc1NaBuIZD1FR2wJhfKzWhED4/edit#slide=id.gf903f6a990_0_5?usp=sharing)
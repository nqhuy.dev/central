#!/bin/bash
                    
read -rp 'Do we need to git pull? (y/N): ' NEED_TO_PULL
read -rp 'Do we need to run docker-compose up? (y/N): ' NEED_TO_DOCKER_COMPOSE_UP
read -rp 'Do we need to install node_modules? (y/N): ' NEED_TO_INSTALL_NODE_MODULES
if [ "$NEED_TO_PULL" == 'y' ]
then
    git pull
fi
if [ "$NEED_TO_DOCKER_COMPOSE_UP" == 'y' ]
then
    docker-compose -f docker-compose-staging.yml up -d --remove-orphans
fi
dateNow=$(date +'%Y-%m-%d.%H-%M-%S')
tar cf ~/elib.${dateNow}.tgz dist
export NODE_OPTIONS="--max-old-space-size=1024"
if [ "$NEED_TO_INSTALL_NODE_MODULES" == 'y' ]
then
    npm install
fi
rm -f .env
cp .production.env .env
nest build
pm2 restart elib_be
import {
  Body,
  Controller,
  Get,
  Param,
  Patch,
  Post,
  Query,
  Req,
  UseGuards,
} from '@nestjs/common';
import * as moment from 'moment';
import { AuthService } from '../auth/auth.service';
import RequestWithAccount from '../auth/dto/reqWithAccount.interface';
import TokenDto from '../auth/dto/token.dto';
import JwtAuthGuard from '../auth/gaurd/jwtAuth.gaurd';
import { Account } from './account.entity';
import { AccountService } from './account.service';
import CreateDto from './dto/create.dto';
import CreateByAdminDto from './dto/createByAdmin.dto';
import GetByIdParamsDto from './dto/getById.dto';
import { IdDto } from './dto/id.dto';
import PasswordDto from './dto/password.dto';
import SearchParamsDto from './dto/searchParams.dto';
import UpdateDto from './dto/update.dto';
import UpdateStatusDto from './dto/updateStatus.dto';

@Controller('accounts')
export class AccountController {
  constructor(
    private readonly accountService: AccountService,
    private readonly authService: AuthService,
  ) {}

  // Search accounts
  @Get()
  @UseGuards(JwtAuthGuard)
  async searchAccount(
    @Req() { user }: RequestWithAccount,
    @Query() searchParams: SearchParamsDto,
  ): Promise<{ data: Account[]; count: number }> {
    const search = searchParams.search?.trim() || '';
    const role = searchParams.role || '*';
    const page = searchParams.page || 1;
    const numOfPage = searchParams.numOfPage || 10;
    try {
      const [accounts, count] = await this.accountService.searchAccounts(
        search,
        role,
        page,
        numOfPage,
        user.role,
      );
      return { data: accounts, count };
    } catch (e) {
      throw e;
    }
  }

  // Get account by id
  @Get(':id')
  @UseGuards(JwtAuthGuard)
  async getAccountById(
    @Req() { user }: RequestWithAccount,
    @Param() { id }: GetByIdParamsDto,
  ): Promise<Account> {
    try {
      if (id === 'me') {
        return await this.accountService.findById(user.id, 2, user.role);
      }
      return await this.accountService.findById(id, 2, user.role);
    } catch (e) {
      throw e;
    }
  }

  /**
   * Send request to create an account
   * @param {CreateDto} data Data to create an account
   * @returns {number} Status of action
   * n (n > 0): Success - Will be respond in n hours
   * 0  : Locked
   * -1 : Already exist as an account
   */
  @Post()
  async createAccount(
    @Body() { email, name, std_code, school }: CreateDto,
  ): Promise<number> {
    try {
      const account = await this.accountService.findByEmail(email, 0);
      if (account && account.is_waiting_accept) {
        const timeNow = moment();
        const timeLast = moment(account.created_at).add(72, 'hours');
        const diffHours = timeLast.diff(timeNow, 'hours', true);
        if (diffHours < 0) {
          // Update to locked after 72h if admin no action
          await this.accountService.lock(account.id);
          return 0;
        }
        return diffHours;
      }
      if (account) return -1;
      await this.accountService.create({ email, name, std_code, school });
      // By default, admin will be replied after 48h
      return 48;
    } catch (e) {
      throw e;
    }
  }

  @Post('create-account')
  @UseGuards(JwtAuthGuard)
  async createAccountByAdmin(
    @Body() { email, name, role }: CreateByAdminDto,
    @Req() { user }: RequestWithAccount,
  ): Promise<boolean> {
    try {
      const info = await this.accountService.createByAdmin(
        { email, name, role },
        user,
      );
      return info;
    } catch (e) {
      throw e;
    }
  }

  @Patch(':id')
  @UseGuards(JwtAuthGuard)
  async updateInformation(
    @Param() { id }: IdDto,
    @Body() data: UpdateDto,
    @Req() { user }: RequestWithAccount,
  ): Promise<Account> {
    try {
      const account = await this.accountService.update(id, data, user);
      return account;
    } catch (e) {
      throw e;
    }
  }

  /**
   * Update status for an account - Admin only
   * @param { user } - Info account
   * @param { id, action } - account's id need to update with 'action'
   * @returns Account
   */
  @Patch(':id/status/:action')
  @UseGuards(JwtAuthGuard)
  async updateStatus(
    @Req() { user }: RequestWithAccount,
    @Param() { id, action }: UpdateStatusDto,
  ): Promise<boolean> {
    try {
      const accountId = id === 'me' ? user.id : id;
      const account = await this.accountService.updateStatusById(
        accountId,
        action,
        user.role,
      );
      return account;
    } catch (e) {
      throw e;
    }
  }

  @Patch('create-password/:token')
  async createPassword(
    @Body() { password }: PasswordDto,
    @Param() { token }: TokenDto,
  ): Promise<boolean> {
    try {
      const status = await this.accountService.updatePasswordWithAuthToken(
        token,
        password,
      );
      return status;
    } catch (e) {
      console.log(e);
      throw e;
    }
  }
}

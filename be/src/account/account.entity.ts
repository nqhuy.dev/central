import {
  Column,
  Entity,
  PrimaryColumn,
  CreateDateColumn,
  UpdateDateColumn,
  DeleteDateColumn,
  OneToMany,
  BeforeInsert,
} from 'typeorm';
import { Exclude } from 'class-transformer';
import { nanoid } from 'nanoid';
import { Auth } from 'src/auth/auth.entity';
import { Role } from './account.enum';
import { EBook } from '../e-book/e-book.entity';
import { BorrowerRecord } from '../borrower-record/borrower-record.entity';

@Entity({ name: 'account' })
export class Account {
  @PrimaryColumn({
    name: 'id',
  })
  public id: string;

  @Column({
    name: 'email',
    unique: true,
  })
  public email: string;

  @Exclude()
  @Column({
    name: 'password',
    default: '',
  })
  public password: string;

  @Column()
  public name: string;

  @Column({
    name: 'std_code',
    default: '',
  })
  public std_code: string;

  @Column({
    name: 'school',
    default: '',
  })
  public school: string;

  @Column({
    name: 'role',
    enum: Role,
    default: Role.User,
  })
  public role: string;

  @Exclude()
  @Column({
    name: 'access_sessions',
    default: '',
  })
  public access_sessions: string;

  @Column({
    name: 'is_locked',
    default: false,
  })
  public is_locked: boolean;

  @Column({
    name: 'is_waiting_accept',
    default: true,
  })
  public is_waiting_accept: boolean;

  @CreateDateColumn()
  created_at: Date;

  @UpdateDateColumn()
  updated_at: Date;

  @DeleteDateColumn()
  deleted_at: Date;

  // Relation
  @OneToMany(() => Auth, (auth) => auth.account)
  auth: Auth[];

  @OneToMany(() => EBook, (eBook) => eBook.account)
  ebooks: EBook[];

  @OneToMany(() => BorrowerRecord, (borrowerRecords) => borrowerRecords.account)
  borrower_record: BorrowerRecord[];

  @BeforeInsert()
  updateID() {
    const idStr = nanoid(16);
    this.id = `ACCx${idStr}`;
  }
}

import { forwardRef, Inject, Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Http400Exception } from 'utils/Exceptions/http400.exception';
import roles from '../../utils/constants/roles';
import defaultValues from '../../utils/constants/defaultValues';
import { Account } from './account.entity';
import { AccountRepository } from './account.repository';
import CreateDto from './dto/create.dto';
import * as moment from 'moment';
import * as bcrypt from 'bcrypt';
import UpdatePasswordDto from './dto/updatePassword.dto';
import { In } from 'typeorm';
import { Action, Role } from './account.enum';
import { Http403Exception } from '../../utils/Exceptions/http403.exception';
import { AuthService } from '../auth/auth.service';
import { MailService } from '../mail/mail.service';
import { ConfigService } from '@nestjs/config';
import CreateAccountDto from './dto/createAccount.dto';
import UpdateDto from './dto/update.dto';
import CreateByAdminDto from './dto/createByAdmin.dto';

const { isAdmin, rolesCanView, ROLES } = roles;
const { HASH_SALT_DEFAULT } = defaultValues;

@Injectable()
export class AccountService {
  constructor(
    @InjectRepository(AccountRepository)
    private readonly accountRepository: AccountRepository,
    @Inject(forwardRef(() => AuthService))
    private readonly authService: AuthService,
    private readonly mailService: MailService,
    private readonly configSrv: ConfigService,
  ) {}

  public async searchAccounts(
    search: string,
    role: string,
    page: number,
    numOfPage: number,
    currentRole: string,
  ): Promise<[accounts: Account[], count: number]> {
    const skip = (Number(page) - 1) * numOfPage;
    const allRolesCanView = rolesCanView(currentRole);
    const roles =
      role === '*'
        ? allRolesCanView
        : (allRolesCanView.includes(role) && [role]) || [];
    const otherFields = isAdmin(currentRole)
      ? ['account.is_locked', 'account.is_waiting_accept']
      : [];
    const query = this.accountRepository
      .createQueryBuilder('account')
      .select([
        'account.id',
        'account.email',
        'account.name',
        'account.role',
        'account.created_at',
        ...otherFields,
      ])
      .where('(email ilike :search or name ilike :search)', {
        search: `%${search}%`,
      })
      .andWhere('role IN (:...roles)', { roles: roles })
      .skip(skip)
      .take(numOfPage);
    const [accounts, count] = isAdmin(currentRole)
      ? await query.getManyAndCount()
      : await query
          .andWhere('(is_locked <> true AND is_waiting_accept <> true)')
          .getManyAndCount();
    return [accounts, count];
  }

  /**
   * Find account by id
   * @param {string} id Account id
   * @param {0|1|2} status 0: Find only - 1: Alert conflict when exist - 2: Alert when not found
   * @param {string} findWithRole Find with role (admin or user); Default is highest permission
   */
  async findById(
    id: string,
    status: number,
    findWithRole = ROLES[0],
  ): Promise<Account> {
    const allRolesCanView = rolesCanView(findWithRole);
    const otherRules = isAdmin(findWithRole)
      ? {}
      : { is_locked: false, is_waiting_accept: false };
    const acc = await this.accountRepository.findOne(id, {
      select: isAdmin(findWithRole)
        ? [
            'id',
            'email',
            'name',
            'std_code',
            'school',
            'role',
            'is_locked',
            'is_waiting_accept',
            'created_at',
          ]
        : [
            'id',
            'email',
            'name',
            'std_code',
            'school',
            'role',
            'created_at',
            'is_locked',
          ],
      where: {
        role: In(allRolesCanView),
        ...otherRules,
      },
    });
    switch (status || 0) {
      case 1:
        if (acc)
          throw new Http400Exception(`Account '#${id}' existed in database`);
        break;
      case 2:
        if (!acc)
          throw new Http400Exception(
            `Account '#${id}' isn't exist in database`,
          );
        break;
    }
    return acc;
  }

  /**
   * Find account by email
   * @param {string} email Email
   * @param {0|1|2} status 0: Find only - 1: Alert conflict when exist - 2: Alert when not found
   */
  async findByEmail(email: string, status = 0): Promise<Account> {
    const acc = await this.accountRepository.findOne({ email });
    switch (status) {
      case 1:
        if (acc) throw new Http400Exception('Email was existed in database');
        break;
      case 2:
        if (!acc) throw new Http400Exception("Email doesn't exist in database");
        break;
    }
    return acc;
  }

  public async create(
    info: CreateAccountDto,
    password = '',
    isWaitingAccept = true,
    role = '',
  ): Promise<Account> {
    const newAccount = this.accountRepository.create(info);
    if (password !== '') {
      // Set password for this account if exist password
      const encryptedPwd = await this.encryptedPwd(password);
      newAccount.password = encryptedPwd;
    }
    if (role !== '') {
      // Set role for this account if exist role
      newAccount.role = role;
    }
    newAccount.is_waiting_accept = isWaitingAccept;
    await this.accountRepository.save(newAccount);
    return newAccount;
  }

  public async createByAdmin(
    info: CreateByAdminDto,
    admin: Account,
  ): Promise<boolean> {
    if (!isAdmin(admin.role)) {
      throw new Http403Exception('Only admin can do it');
    }
    const existAccount = await this.accountRepository.findOne({
      email: info.email,
    });
    if (existAccount) {
      throw new Http400Exception(
        'An account with this email was exist in database',
      );
    }
    const newAccount = this.accountRepository.create({
      ...info,
      is_waiting_accept: false,
    });
    const account = await this.accountRepository.save(newAccount);
    // Handle send email create password
    const tokenExist = await this.authService.findByAccount(
      account.id,
      'createPassword',
    );
    if (tokenExist) {
      await this.authService.updateToUsed(tokenExist.id);
    }
    const token = await this.authService.createAuth(
      'createPassword',
      account,
      24,
    );
    const status = await this.mailService.send(
      account,
      'invite',
      token,
      admin,
      info.role,
    );
    console.log('SEND MAIL:', status);
    return !!newAccount;
  }

  public async getAccessSession(accountId: string): Promise<number> {
    const account = await this.accountRepository.findOne(accountId);
    if (!account) {
      throw new Http400Exception('Invalid request');
    }
    // Account got when login -> Always exist -> Not need to validate
    const { access_sessions } = account;
    const timeNow = moment();
    const availableSessions = access_sessions
      .split(',')
      .filter((aS) => aS && Number(aS) > timeNow.unix()); // Filter session expired
    const availableSessionsLength = availableSessions.length;
    if (availableSessionsLength) {
      // Save available sessions and return last available access-session
      await this.accountRepository.save({
        ...account,
        access_sessions: availableSessions.join(','),
      });
      return Number(availableSessions[availableSessionsLength - 1]);
    }
    // Create new session
    const newSession = timeNow
      .clone()
      .add(Number(this.configSrv.get('JWT_EXPIRATION_DATE')), 'days');
    console.log(
      `Create new access-session ----> [${timeNow.unix()} -> ${newSession.unix()}]`,
    );
    availableSessions.push(`${newSession.unix()}`);
    await this.accountRepository.save({
      ...account,
      access_sessions: availableSessions.join(','),
    });
    return newSession.unix();
  }

  public async validAccessSession(
    accountId: string,
    accessSession: number,
  ): Promise<boolean> {
    const currentUnix = moment().unix();
    console.log(
      accountId,
      '-->',
      accessSession,
      moment.unix(accessSession).format('HH:mm - DD/MM/YY'),
      '|',
      currentUnix,
      moment().format('HH:mm - DD/MM/YY'),
      '>',
      moment().diff(moment.unix(accessSession), 'd'),
    );
    const account = await this.accountRepository.findOne(accountId);
    return (
      currentUnix <= accessSession &&
      account.access_sessions.split(',').includes(`${accessSession}`)
    );
  }

  public async update(
    id: string,
    info: UpdateDto,
    user: Account,
  ): Promise<Account> {
    if (!isAdmin(user.role) && user.id !== id) {
      throw new Http403Exception(
        'You can not update information for another account',
      );
    }
    const account = await this.accountRepository.findOne(id, {
      select: [
        'id',
        'name',
        'std_code',
        'school',
        'email',
        'role',
        'created_at',
        'is_locked',
        'is_waiting_accept',
      ],
    });
    account.name = info.name;
    account.std_code = info.std_code;
    account.school = info.school;
    await this.accountRepository.save(account);
    return account;
  }

  public async updatePassword(info: UpdatePasswordDto): Promise<Account> {
    const newPost = this.accountRepository.create(info);
    await this.accountRepository.save(newPost);
    return newPost;
  }

  public async updatePasswordWithAuthToken(
    token: string,
    password: string,
  ): Promise<boolean> {
    const { account } = await this.authService.getAuthByToken(token, true);
    const salt = this.configSrv.get('PASS_HASH_SALT') || HASH_SALT_DEFAULT;
    account.password = await bcrypt.hash(password, Number(salt));
    await this.accountRepository.save(account);
    return true;
  }

  public async lock(id: string): Promise<Account> {
    const account = await this.accountRepository.findOne(id);
    account.is_locked = true;
    await this.accountRepository.save(account);
    return account;
  }

  public async updateStatusById(
    id: string,
    action: string,
    role: string,
  ): Promise<boolean> {
    if (action === Action.Lock || action === Action.Unlock) {
      if (!isAdmin(role)) {
        throw new Http403Exception('Only admin can do it');
      }
      const updateStatus = await this.updateStatus(id, action, role);
      return !!updateStatus;
    }
    if (action !== Action.Active) {
      throw new Http400Exception('No action was found to handle');
    }
    // Active an account
    const account = await this.findById(id, 2, role);
    account.is_waiting_accept = false;
    await this.accountRepository.save(account);
    const tokenExist = await this.authService.findByAccount(
      account.id,
      'createPassword',
    );
    if (tokenExist) {
      throw new Http400Exception("Looks like you've done this action before");
    }
    const token = await this.authService.createAuth(
      'createPassword',
      account,
      24,
    );
    const status = await this.mailService.send(
      account,
      'createPassword',
      token,
    );
    console.log('SEND MAIL:', status);
    return true;
  }

  private async updateStatus(
    id: string,
    action: string,
    role: string,
  ): Promise<Account> {
    const account = await this.findById(id, 2, role);
    account.is_locked = action === Action.Lock;
    await this.accountRepository.save(account);
    return account;
  }

  private async encryptedPwd(password: string): Promise<string> {
    const hashSalt = this.configSrv.get('PASS_HASH_SALT') || HASH_SALT_DEFAULT;
    const encryptedPassword = await bcrypt.hash(password, Number(hashSalt));
    return encryptedPassword;
  }
}

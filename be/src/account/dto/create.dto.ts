import { IsEmail, IsString } from 'class-validator';
export class CreateDto {
  @IsEmail()
  email: string;

  @IsString()
  name: string;

  @IsString()
  std_code: string;

  @IsString()
  school: string;
}

export default CreateDto;

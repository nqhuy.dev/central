import { IsEmail, IsOptional, IsString } from 'class-validator';
export class CreateAccountDto {
  @IsEmail()
  email: string;

  @IsString()
  name: string;

  @IsOptional()
  @IsString()
  std_code: string;

  @IsOptional()
  @IsString()
  school: string;
}

export default CreateAccountDto;

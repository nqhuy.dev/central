import { IsEmail, IsString } from 'class-validator';
export class CreateByAdminDto {
  @IsEmail()
  email: string;

  @IsString()
  name: string;

  @IsString()
  role: string;
}

export default CreateByAdminDto;

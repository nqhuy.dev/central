import { IsOptional, IsString } from 'class-validator';

export class SearchParamsDto {
  @IsOptional()
  @IsString()
  search: string;

  @IsOptional()
  @IsString()
  role: string;

  @IsOptional()
  @IsString()
  page: number;

  @IsOptional()
  @IsString()
  numOfPage: number;
}

export default SearchParamsDto;

import { IsOptional, IsString } from 'class-validator';
export class UpdateDto {
  @IsString()
  name: string;

  @IsOptional()
  @IsString()
  std_code: string;

  @IsOptional()
  @IsString()
  school: string;
}

export default UpdateDto;

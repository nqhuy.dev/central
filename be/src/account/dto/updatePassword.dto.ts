export class UpdatePasswordDto {
  email: string;
  oldPassword: string;
  newPassword: string;
}

export default UpdatePasswordDto;

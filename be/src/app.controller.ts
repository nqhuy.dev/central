import { Controller, Get } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import * as moment from 'moment';
import { Role } from './account/account.enum';
import { AccountService } from './account/account.service';
import { AppService } from './app.service';

@Controller()
export class AppController {
  constructor(
    private readonly appService: AppService,
    private readonly configService: ConfigService,
    private readonly accountService: AccountService,
  ) {}

  @Get()
  getHello(): string {
    return this.appService.getHello();
  }

  @Get('init')
  public async initApp(): Promise<string> {
    const email = this.configService.get('APP_ACCOUNT_USER');
    const name = this.configService.get('APP_ACCOUNT_NAME') || 'Admin';
    const password = this.configService.get('APP_ACCOUNT_PASS');

    try {
      await this.accountService.findByEmail(email, 1);
      const adminAccount = await this.accountService.create(
        { email, name, std_code: '', school: '' },
        password,
        false,
        Role.Admin,
      );
      if (!adminAccount) {
        return 'Ohh! Where am I! Can I see me!?';
      }
      return 'Thank for awake me up!';
    } catch (err) {
      if (err && err.message === 'Email was existed in database') {
        return `So early, now is ${moment().format('HH:mm - DD/MM/YY')}`;
      }
      console.log('[ERR] - Error when run init: ', err);
      return 'Ohh! I think i have COVID-19 virus!!';
    }
  }
}

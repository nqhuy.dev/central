import { Module } from '@nestjs/common';
import { AccountModule } from './account/account.module';
import { AuthModule } from './auth/auth.module';
import { ConfigModule } from '@nestjs/config';
import * as Joi from '@hapi/joi';
import { DatabaseModule } from './database.module';
import { MailModule } from './mail/mail.module';
import { BookModule } from './book/book.module';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { EBookModule } from './e-book/e-book.module';
import { BorrowerRecordModule } from './borrower-record/borrower-record.module';
import { IntegrateModule } from './integrate/integrate.module';
import { HttpModule } from '@nestjs/axios';
import { CronjobModule } from './cronjob/cronjob.module';
import { ScheduleModule } from '@nestjs/schedule';
import { SocketModule } from './socket/socket.module';
import { TaskModule } from './task/task.module';

@Module({
  imports: [
    ConfigModule.forRoot({
      validationSchema: Joi.object({
        POSTGRES_HOST: Joi.string().required(),
        POSTGRES_PORT: Joi.number().required(),
        POSTGRES_USER: Joi.string().required(),
        POSTGRES_PASSWORD: Joi.string().required(),
        POSTGRES_DB: Joi.string().required(),
        PORT: Joi.number(),
        APP_ACCOUNT_USER: Joi.string().required(),
        APP_ACCOUNT_PASS: Joi.string().required(),
        JWT_SECRET: Joi.string().required(),
        JWT_EXPIRATION_DATE: Joi.string().required(),
        MAIL_HOST: Joi.string().required(),
        MAIL_PORT: Joi.number().required(),
        MAIL_USER: Joi.string().required(),
        MAIL_PASS: Joi.string().required(),
        MAIL_FROM: Joi.string().required(),
        INTEGRATE_SLACK_TOKEN_VALID: Joi.string().required(),
        INTEGRATE_SLACK_INCOMING_HOOK: Joi.string().required(),
      }),
      envFilePath: '.env',
    }),
    ScheduleModule.forRoot(),
    HttpModule.registerAsync({
      useFactory: () => ({
        timeout: 5000,
        maxRedirects: 5,
      }),
    }),
    DatabaseModule,
    AccountModule,
    AuthModule,
    MailModule,
    BookModule,
    EBookModule,
    BorrowerRecordModule,
    IntegrateModule,
    CronjobModule,
    SocketModule,
    TaskModule,
  ],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {}

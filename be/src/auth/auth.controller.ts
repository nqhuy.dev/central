import {
  Req,
  Controller,
  Post,
  Res,
  UseGuards,
  Param,
  HttpCode,
} from '@nestjs/common';
import { Response } from 'express';
import { Account } from '../account/account.entity';
import { AccountService } from '../account/account.service';
import { AuthService } from './auth.service';
import RequestWithAccount from './dto/reqWithAccount.interface';
import TokenDto from './dto/token.dto';
import JwtAuthGuard from './gaurd/jwtAuth.gaurd';
import { LocalAuthGuard } from './gaurd/localAuth.gaurd';

@Controller('auth')
export class AuthController {
  constructor(
    private readonly authService: AuthService,
    private readonly accountService: AccountService,
  ) {}

  @HttpCode(200)
  @UseGuards(LocalAuthGuard)
  @Post('login')
  async login(@Req() request: RequestWithAccount, @Res() response: Response) {
    try {
      const { user: account } = request;
      const accessSession = await this.accountService.getAccessSession(
        account.id,
      );
      const { cookie, token } = this.authService.genToken({
        accountId: account.id,
        accessSession,
      });
      // Hide important data
      account.password = undefined;
      account.access_sessions = undefined;
      account.is_waiting_accept = undefined;
      account.is_locked = undefined;
      console.log(
        `------ LOGIN --------\n${account.name} - ${account.email} ---> ${token}\n----------`,
      );
      response.setHeader('Set-Cookie', cookie);
      return response.send(account);
    } catch (err) {
      throw err;
    }
  }

  @UseGuards(JwtAuthGuard)
  @Post('logout')
  async logout(@Res() response: Response) {
    const { cookie } = this.authService.genToken();
    response.setHeader('Set-Cookie', cookie);
    return response.sendStatus(200);
  }

  @HttpCode(200)
  @UseGuards(LocalAuthGuard)
  @Post('login-v2')
  async loginV2(
    @Req() request: RequestWithAccount,
  ): Promise<{ account: Account; token: string }> {
    try {
      const { user: account } = request;
      const accessSession = await this.accountService.getAccessSession(
        account.id,
      );
      const { token } = this.authService.genToken({
        accountId: account.id,
        accessSession,
      });
      // Hide important data
      account.password = undefined;
      account.access_sessions = undefined;
      account.is_waiting_accept = undefined;
      account.is_locked = undefined;
      return {
        account,
        token,
      };
    } catch (err) {
      throw err;
    }
  }

  @Post('valid-token/:token')
  async validToken(
    @Param() { token }: TokenDto,
  ): Promise<{ email: string; purpose: string }> {
    try {
      const authInfo = await this.authService.getAuthByToken(token);
      const hiddenEmail = getHiddenEmail(authInfo.account.email);
      const res = {
        email: hiddenEmail,
        purpose: authInfo.used_for,
      };
      return res;
    } catch (err) {
      throw err;
    }
  }
}

function getHiddenEmail(email: string): string {
  const [prepend, append] = email.split('@');
  const newPre = prepend.slice(prepend.length - 4, prepend.length);
  const newApp = append.split('.')[0];
  return `***${newPre}@${newApp}.***`;
}

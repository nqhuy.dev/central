import {
  Column,
  Entity,
  PrimaryColumn,
  CreateDateColumn,
  UpdateDateColumn,
  DeleteDateColumn,
  JoinColumn,
  ManyToOne,
  BeforeInsert,
} from 'typeorm';
import { Exclude } from 'class-transformer';
import { nanoid } from 'nanoid';
import { Account } from 'src/account/account.entity';

@Entity({ name: 'auth' })
export class Auth {
  @PrimaryColumn({
    name: 'id',
  })
  public id: string;

  @Exclude()
  @Column({
    name: 'token',
  })
  public token: string;

  @Column({
    name: 'expired_in',
    default: 15,
  })
  expired_in: number;

  @Column({
    name: 'used_for',
    enum: ['resetPassword', 'createPassword'],
    default: 'createPassword',
  })
  used_for: string;

  @Column({
    name: 'is_used',
    default: false,
  })
  public is_used: boolean;

  @CreateDateColumn()
  created_at: Date;

  @UpdateDateColumn()
  updated_at: Date;

  @DeleteDateColumn()
  deleted_at: Date;

  // Relation
  @ManyToOne(() => Account)
  @JoinColumn()
  account: Account;

  @BeforeInsert()
  updateBeforeInsert() {
    const idStr = nanoid(16);
    this.id = `AUTx${idStr}`;
    this.token = nanoid(32);
  }
}

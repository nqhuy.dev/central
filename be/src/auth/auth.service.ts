import { forwardRef, Inject, Injectable } from '@nestjs/common';
import { AccountService } from 'src/account/account.service';
import LoginDto from './dto/login.dto';
import * as bcrypt from 'bcrypt';
import Crypto from 'crypto-js';
import { Http400Exception } from 'utils/Exceptions/http400.exception';
import { Http403Exception } from 'utils/Exceptions/http403.exception';
import { JwtService } from '@nestjs/jwt';
import { ConfigService } from '@nestjs/config';
import genToken from './dto/genToken';
import { InjectRepository } from '@nestjs/typeorm';
import { AuthRepository } from './auth.repository';
import { Account } from '../account/account.entity';
import { Auth } from './auth.entity';
import * as moment from 'moment';
const MAP_NUM_PURPOSE = ['invite', 'reset_password'];

const messages = {
  account_not_exist: 'Username or password is incorrect',
  account_locked:
    'Your account has been locked! Contact to admin to have more information',
  account_waiting_accept:
    'Your request is waiting to accept by admin! Please wait...!',
  token_expired: 'Token was expired',
  token_not_exist: 'Token does not exist or has been used',
};

@Injectable()
export class AuthService {
  constructor(
    @InjectRepository(AuthRepository)
    private authRepository: AuthRepository,
    @Inject(forwardRef(() => AccountService))
    private accountService: AccountService,
    private readonly configService: ConfigService,
    private readonly jwtService: JwtService,
  ) {}

  async login({ email, password }: LoginDto) {
    try {
      const account = await this.accountService.findByEmail(email, 2);
      if (account.is_locked) {
        throw new Http403Exception(messages.account_locked);
      }
      if (account.is_waiting_accept) {
        throw new Http403Exception(messages.account_waiting_accept);
      }
      const isValid = await bcrypt.compare(password, account.password);
      if (!isValid) {
        throw new Http400Exception(messages.account_not_exist);
      }
      return account;
    } catch (e) {
      throw e;
    }
  }

  public genToken(payload: genToken = null): { token: string; cookie: string } {
    const authKey = this.configService.get('AUTH_KEY');
    if (!payload) {
      return {
        token: '',
        cookie: `${authKey}=; Domain=; Path=/; Max-Age=; `,
      };
    }
    const expireDate = Number(
      this.configService.get('JWT_EXPIRATION_DATE') || 7,
    );
    const maxAge = expireDate * 60 * 60 * 24;
    const domain = this.configService.get('DOMAIN') || 'localhost';
    const token = this.jwtService.sign(payload);
    const cookie = `${authKey}=${token}; Domain=${domain}; Path=/; Max-Age=${maxAge}; `;
    return { token, cookie };
  }

  public async createAuth(
    usedFor: string,
    usedBy: Account,
    timeExp: number,
  ): Promise<string> {
    const newAuth = this.authRepository.create();
    newAuth.expired_in = timeExp;
    newAuth.used_for = usedFor;
    newAuth.account = usedBy;
    const data = await this.authRepository.save(newAuth);
    return data.token;
  }

  /**
   * Used to get and update AuthToken to used (if need).
   * @param token Token
   * @param isSetToUsed Is need to set it to used? Default is false
   * @returns Auth model
   */
  public async getAuthByToken(
    token: string,
    isSetToUsed = false,
  ): Promise<Auth> {
    const info = await this.authRepository
      .createQueryBuilder('auth')
      .leftJoinAndSelect('auth.account', 'account')
      .select([
        'auth.used_for',
        'auth.created_at',
        'auth.expired_in',
        'auth.is_used',
        'auth.id',
        'account.email',
        'account.name',
        'account.id',
      ])
      .where('token = :token', { token })
      .getOne();
    if (!info || info.is_used) {
      throw new Http400Exception(messages.token_not_exist);
    }
    const expTime = moment(info.created_at).add(info.expired_in, 'hours');
    const diff = moment(moment()).diff(expTime, 'second');
    if (diff > 0) {
      throw new Http400Exception(messages.token_expired);
    }
    if (isSetToUsed) {
      const authTokenNeedUpdate = await this.authRepository.findOne(info.id);
      authTokenNeedUpdate.is_used = true;
      await this.authRepository.save(authTokenNeedUpdate);
    }
    return info;
  }

  public async findByAccount(
    accountId: string,
    purpose: string,
  ): Promise<Auth> {
    const info = await this.authRepository.find({
      relations: ['account'],
      where: {
        account: {
          id: accountId,
        },
        used_for: purpose,
      },
    });
    return info.length ? info[0] : null;
  }

  public async updateToUsed(id: string): Promise<boolean> {
    const authToken = await this.authRepository.findOne(id);
    authToken.is_used = true;
    await this.authRepository.save(authToken);
    return true;
  }

  /**
   * Used to validate an authenticate token
   * @param accountId Identify of account
   * @param keys The key, what sent to user
   * @returns true if validated | Error if invalid
   */
  public async isValid(accountId: string, keys: string): Promise<boolean> {
    const [purposeNum, tokenEncrypted] = keys.includes('@')
      ? keys.split('@')
      : ['not-num', ''];
    if (typeof purposeNum !== 'number' || tokenEncrypted === '') {
      throw new Http400Exception('Your token is invalid!');
    }
    const purpose = this.getPurposeText(Number(purposeNum));
    const token = Crypto.AES.decrypt(
      tokenEncrypted,
      `${accountId}-${this.configService.get('HASH_TOKEN')}`,
    );
    const auth = await this.authRepository.findOne({
      where: {
        account: accountId,
        usedFor: purpose,
        token,
        isUsed: false,
      },
      relations: ['account'],
    });
    if (!auth) {
      throw new Http400Exception('Your token is invalid!');
    }
    return true;
  }

  private getPurposeText(num: number): string {
    const res = MAP_NUM_PURPOSE[num];
    if (!res) {
      throw new Http400Exception('Your token is invalid');
    }
    return res;
  }
}

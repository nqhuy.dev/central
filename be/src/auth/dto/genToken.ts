import { IsNumber, IsOptional, IsString } from 'class-validator';

export class genToken {
  @IsOptional()
  @IsString()
  accountId: string;

  @IsOptional()
  @IsNumber()
  accessSession: number;
}

export default genToken;

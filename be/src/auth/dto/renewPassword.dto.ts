export class RenewPasswordDto {
  email: string;
  oldPassword: string;
  newPassword: string;
}

export default RenewPasswordDto;

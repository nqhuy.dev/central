import { ExtractJwt, Strategy } from 'passport-jwt';
import { PassportStrategy } from '@nestjs/passport';
import { Injectable } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import { Request } from 'express';
import { AccountService } from '../account/account.service';
import roles from '../../utils/constants/roles';
import genToken from './dto/genToken';
import { Http401Exception } from '../../utils/Exceptions/http401.exception';

const { ROLES } = roles;

@Injectable()
export class JwtStrategy extends PassportStrategy(Strategy) {
  constructor(
    private readonly configService: ConfigService,
    private readonly accountService: AccountService,
  ) {
    super({
      jwtFromRequest: ExtractJwt.fromExtractors([
        (req: Request) => {
          const authKey = configService.get('AUTH_KEY');
          // console.log(req?.cookies?.Authentication);
          if (req?.params && req.params[authKey]) {
            return req.params[authKey];
          }
          return req?.cookies && req.cookies[authKey]
            ? req.cookies[authKey]
            : '';
        },
      ]),
      secretOrKey: configService.get('JWT_SECRET'),
    });
  }

  async validate(payload: genToken) {
    const { accessSession, accountId } = payload;
    console.log(accessSession);
    const account = await this.accountService.findById(accountId, 0, ROLES[0]);
    // console.log(account);
    if (!account) {
      throw new Http401Exception('Unauthorized.');
    }
    const isValidSession = await this.accountService.validAccessSession(
      accountId,
      accessSession,
    );
    console.log(accessSession);
    if (!isValidSession) {
      throw new Http401Exception(
        'Your session is invalid! Please re-login to continue.',
      );
    }
    return account;
  }
}

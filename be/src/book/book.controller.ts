import {
  Body,
  Controller,
  Get,
  Param,
  Patch,
  Post,
  Query,
  Req,
  UseGuards,
} from '@nestjs/common';
import RequestWithAccount from '../auth/dto/reqWithAccount.interface';
import JwtAuthGuard from '../auth/gaurd/jwtAuth.gaurd';
import { Book } from './book.entity';
import { BookService } from './book.service';
import { BookCategory } from './category.entity';
import { BookDto } from './dto/book.dto';
import { IdDto } from './dto/id.dto';
import SearchNameDto from './dto/name.dto';
import { BookSearchDto } from './dto/search.dto';
import { StatusDto } from './dto/status.dto';
import { BookSource } from './source.entity';

@Controller('books')
export class BookController {
  constructor(private readonly bookSrv: BookService) {}

  @Get()
  @UseGuards(JwtAuthGuard)
  public async search(
    @Req() { user }: RequestWithAccount,
    @Query() searchInfo: BookSearchDto,
  ): Promise<{ data: Book[]; count: number }> {
    const page = searchInfo.page || 1;
    const numOfPage = searchInfo.numOfPage || 10;
    try {
      const [books, count] = await this.bookSrv.search(
        searchInfo,
        page,
        numOfPage,
        user.role,
      );
      return { data: books, count };
    } catch (e) {
      throw e;
    }
  }

  @Get(':id')
  @UseGuards(JwtAuthGuard)
  public async getById(
    @Req() { user }: RequestWithAccount,
    @Param() { id }: IdDto,
  ): Promise<Book> {
    try {
      const book = await this.bookSrv.getById(id, user.role);
      return book;
    } catch (e) {
      throw e;
    }
  }

  @Post()
  @UseGuards(JwtAuthGuard)
  public async create(
    @Req() { user }: RequestWithAccount,
    @Body() bookInfo: BookDto,
  ): Promise<Book> {
    try {
      const newBook = await this.bookSrv.create(bookInfo, user);
      return newBook;
    } catch (e) {
      throw e;
    }
  }

  @Patch(':id')
  @UseGuards(JwtAuthGuard)
  public async updateInfo(
    @Param() { id }: IdDto,
    @Body() bookInfo: BookDto,
  ): Promise<Book> {
    try {
      const newBook = await this.bookSrv.updateById(id, bookInfo);
      return newBook;
    } catch (e) {
      throw e;
    }
  }

  @Patch(':id/status')
  @UseGuards(JwtAuthGuard)
  public async updateStatus(
    @Param() { id }: IdDto,
    @Body() statusInfo: StatusDto,
  ): Promise<Book> {
    try {
      const newBook = await this.bookSrv.updateStatusById(id, statusInfo);
      return newBook;
    } catch (e) {
      throw e;
    }
  }

  // Categories - Sources
  @Get('i/categories')
  @UseGuards(JwtAuthGuard)
  public async getCategories(
    @Query() { search }: SearchNameDto,
  ): Promise<{ data: BookCategory[]; count: number }> {
    const name = search && search.trim() ? search.trim() : '';
    try {
      const [data, count] = await this.bookSrv.getCategories(name);
      return { data, count };
    } catch (e) {
      throw e;
    }
  }

  @Get('i/sources')
  @UseGuards(JwtAuthGuard)
  public async getSources(
    @Query() { search }: SearchNameDto,
  ): Promise<{ data: BookSource[]; count: number }> {
    const name = search && search.trim() ? search.trim() : '';
    try {
      const [data, count] = await this.bookSrv.getSources(name);
      return { data, count };
    } catch (e) {
      throw e;
    }
  }
}

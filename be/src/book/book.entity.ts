import {
  Column,
  Entity,
  PrimaryColumn,
  CreateDateColumn,
  UpdateDateColumn,
  DeleteDateColumn,
  JoinColumn,
  ManyToOne,
  ManyToMany,
  JoinTable,
  BeforeInsert,
} from 'typeorm';
import { nanoid } from 'nanoid';
import { Account } from 'src/account/account.entity';
import { BookCategory } from './category.entity';
import { BookSource } from './source.entity';
import { BorrowerRecord } from '../borrower-record/borrower-record.entity';

@Entity({ name: 'book' })
export class Book {
  @PrimaryColumn({
    name: 'id',
  })
  public id: string;

  @Column({
    name: 'name',
  })
  name: string;

  @Column({
    name: 'description',
    default: '',
  })
  description: string;

  @Column({
    name: 'author',
  })
  author: string;

  @Column({
    name: 'is_owned',
    default: true,
  })
  is_owned: boolean;

  @Column({
    name: 'is_archived',
    default: false,
  })
  is_archived: boolean;

  @CreateDateColumn()
  created_at: Date;

  @UpdateDateColumn()
  updated_at: Date;

  @DeleteDateColumn()
  deleted_at: Date;

  // Relation
  @ManyToOne(() => BookCategory)
  @JoinColumn()
  category: BookCategory;

  @ManyToOne(() => BookSource)
  @JoinColumn()
  source: BookSource;

  @ManyToOne(() => Account)
  @JoinColumn()
  created_by: Account;

  @ManyToMany(() => BorrowerRecord, (borrowerRecord) => borrowerRecord.books)
  @JoinTable()
  borrower_record: BorrowerRecord[];

  @BeforeInsert()
  updateID() {
    const idStr = nanoid(16);
    this.id = `BOKx${idStr}`;
  }
}

import { forwardRef, Module } from '@nestjs/common';
import { BookService } from './book.service';
import { BookController } from './book.controller';
import { BookRepository } from './book.repository';
import { TypeOrmModule } from '@nestjs/typeorm';
import { BookSourceRepository } from './source.repository';
import { BookCategoryRepository } from './category.repository';
import { AuthModule } from '../auth/auth.module';
import { BorrowerRecordModule } from '../borrower-record/borrower-record.module';

@Module({
  imports: [
    TypeOrmModule.forFeature([
      BookRepository,
      BookSourceRepository,
      BookCategoryRepository,
    ]),
    forwardRef(() => BorrowerRecordModule),
    AuthModule,
  ],
  providers: [BookService],
  controllers: [BookController],
  exports: [BookService, TypeOrmModule],
})
export class BookModule {}

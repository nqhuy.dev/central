import { forwardRef, Inject, Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import roles from '../../utils/constants/roles';
import { Http400Exception } from '../../utils/Exceptions/http400.exception';
import { Account } from '../account/account.entity';
import { BorrowerRecordService } from '../borrower-record/borrower-record.service';
import { Book } from './book.entity';
import { Status } from './book.enum';
import { BookRepository } from './book.repository';
import { BookCategory } from './category.entity';
import { BookCategoryRepository } from './category.repository';
import { BookDto } from './dto/book.dto';
import { BookSearchDto } from './dto/search.dto';
import { StatusDto } from './dto/status.dto';
import { BookSource } from './source.entity';
import { BookSourceRepository } from './source.repository';

const { ROLES } = roles;

const messages = {
  book_not_exist: 'Can not find information for this book',
};

@Injectable()
export class BookService {
  constructor(
    @InjectRepository(BookRepository)
    private readonly bookRepository: BookRepository,
    @InjectRepository(BookSourceRepository)
    private readonly sourceRepository: BookSourceRepository,
    @InjectRepository(BookCategoryRepository)
    private readonly categoryRepository: BookCategoryRepository,
    @Inject(forwardRef(() => BorrowerRecordService))
    private readonly borrowerRecordSrv: BorrowerRecordService,
  ) {}

  public async search(
    info: BookSearchDto,
    page: number,
    numOfPage: number,
    currentRole: string,
  ): Promise<[d: Book[], c: number]> {
    const {
      name,
      author,
      source,
      createdBy,
      category,
      isOwned,
      isArchived,
      keyword,
    } = info;
    const skip = (Number(page) - 1) * numOfPage;
    const otherFields =
      currentRole === ROLES[0]
        ? [
            'book.is_owned',
            'book.is_archived',
            'book.source',
            'book.created_by',
          ]
        : [];
    let query = this.bookRepository
      .createQueryBuilder('book')
      .select([
        'book.id',
        'book.name',
        'book.description',
        'book.author',
        'book.created_at',
        'source.name',
        'source.description',
        'category.name',
        'category.description',
        'borrower_record.id',
        'borrower_record.current_status',
        'borrower_record.updated_at',
        'account.id',
        'account.name',
        ...otherFields,
      ])
      .leftJoinAndSelect('book.source', 'source')
      .leftJoinAndSelect('book.category', 'category')
      .leftJoin('book.borrower_record', 'borrower_record')
      .leftJoin('borrower_record.account', 'account')
      .where('(book.name ilike :name or book.description ilike :name)', {
        name: `%${name || ''}%`,
      });

    const keywordSearch = (keyword || '').trim();

    if (author && author.trim()) {
      query = query.andWhere('author = :author', {
        author: `%${author || ''}%`,
      });
    }
    if (source && source.trim()) {
      query = query.andWhere('source.name = :source', {
        source: (source || '').trim(),
      });
    }
    if (category && category.trim()) {
      query = query.andWhere('category.name = :category', {
        category: (category || '').trim(),
      });
    }
    if (keywordSearch !== '') {
      query = query.andWhere(
        '(book.name ilike :keyword or book.description ilike :keyword or author = :keyword or source.name = :keyword or category.name = :keyword)',
        {
          keyword: `%${keywordSearch}%`,
        },
      );
    }
    if (isOwned !== undefined) {
      query = query.andWhere('is_owned = :isOwned', {
        isOwned: isOwned === Status.True,
      });
    }
    if (isArchived !== undefined && currentRole !== ROLES[0]) {
      query = query.andWhere('is_archived = :isArchived', {
        isArchived: isArchived === Status.True,
      });
    }
    if (createdBy && createdBy.trim()) {
      query = query.andWhere(
        '(book.created_by.name = :createdBy or book.created_by.id = :createdBy)',
        { createdBy },
      );
    }
    const [books, count] = await query
      .skip(skip)
      .take(numOfPage)
      .getManyAndCount();
    return [books, count];
  }

  public async checkBooks(
    ids: string[],
  ): Promise<{ valid: Book[]; validIds: string[]; invalidIds: string[] }> {
    const uniqueIds = ids.filter((v, i) => ids.indexOf(v) === i);
    const books = await this.bookRepository.findByIds(uniqueIds);
    const validIds = books.map((book) => book.id);
    if (books.length === uniqueIds.length) {
      return { valid: books, validIds, invalidIds: [] };
    }
    const invalidIds = uniqueIds.filter((id) => !validIds.includes(id));
    return { valid: books, validIds, invalidIds: invalidIds };
  }

  public async getById(id: string, currentRole: string): Promise<Book> {
    const book =
      currentRole === ROLES[0]
        ? await this.bookRepository
            .createQueryBuilder('book')
            .select([
              'book.id',
              'book.name',
              'description',
              'author',
              'category',
              'source',
              'created_at',
              'created_by',
              'is_archived',
              'is_owned',
              'book.updated_at',
              'borrower_record.id',
              'borrower_record.current_status',
              'borrower_record.updated_at',
              'account.id',
              'account.name',
            ])
            .leftJoinAndSelect('book.source', 'source')
            .leftJoinAndSelect('book.category', 'category')
            .leftJoin('book.borrower_record', 'borrower_record')
            .leftJoin('borrower_record.account', 'account')
            .where('(book.id = :id)', { id })
            .getOne()
        : await this.bookRepository.findOne({
            relations: ['source', 'category'],
            select: [
              'id',
              'name',
              'description',
              'author',
              'category',
              'source',
              'created_at',
            ],
            where: { id, is_archived: false },
          });
    if (!book) {
      throw new Http400Exception(messages.book_not_exist);
    }
    return book;
  }

  // [NEED_CHECK] -> This function is out-of-date and the result is as same as getById
  public async getByIdAndStatus(
    id: string,
    currentRole: string,
  ): Promise<{ book: Book; isReady: boolean }> {
    const book =
      currentRole !== ROLES[0]
        ? await this.bookRepository.findOne(id, {
            relations: ['source', 'category', 'created_by'],
            select: [
              'id',
              'name',
              'description',
              'author',
              'category',
              'source',
              'created_at',
              'created_by',
              'is_archived',
              'is_owned',
              'updated_at',
            ],
          })
        : await this.bookRepository.findOne({
            relations: ['source', 'category'],
            select: [
              'id',
              'name',
              'description',
              'author',
              'category',
              'source',
              'created_at',
            ],
            where: { id, is_archived: false },
          });
    if (!book) {
      throw new Http400Exception(messages.book_not_exist);
    }
    // Check status of book
    const borrowerRecordHasBook = await this.borrowerRecordSrv.findWithBookId(
      id,
    );
    // console.log(borrowerRecordHasBook);
    return { book, isReady: !borrowerRecordHasBook };
  }

  public async getCategories(
    s: string,
  ): Promise<[d: BookCategory[], c: number]> {
    return await this.categoryRepository
      .createQueryBuilder('category')
      .where('name ilike :name', { name: `%${s}%` })
      .getManyAndCount();
  }

  public async getSources(s: string): Promise<[d: BookSource[], c: number]> {
    return await this.sourceRepository
      .createQueryBuilder('source')
      .where('name ilike :name', { name: `%${s}%` })
      .getManyAndCount();
  }

  public async create(info: BookDto, createdBy: Account): Promise<Book> {
    const { name, description, author, isOwned, source, category } = info;
    const newBook = this.bookRepository.create();
    newBook.name = name;
    newBook.description = description;
    newBook.author = author;
    newBook.is_owned = isOwned === Status.True;
    newBook.created_by = createdBy;
    // Source & Category
    newBook.source = await this.sourceRepository.getOrCreateIfNotExist(source);
    newBook.category = await this.categoryRepository.getOrCreateIfNotExist(
      category,
    );

    return await this.bookRepository.save(newBook);
  }

  public async updateById(id: string, info: BookDto): Promise<Book> {
    const { name, description, author, isOwned, source, category } = info;
    const book = await this.bookRepository.findOne(id, {
      relations: ['source', 'category'],
    });
    if (!book) {
      throw new Http400Exception(messages.book_not_exist);
    }
    book.name = name;
    book.description = description || '';
    book.author = author;
    book.is_owned = isOwned === Status.True;
    // Source & Category
    book.source = await this.sourceRepository.getOrCreateIfNotExist(source);
    book.category = await this.categoryRepository.getOrCreateIfNotExist(
      category,
    );

    return await this.bookRepository.save(book);
  }

  public async updateStatusById(
    id: string,
    { status }: StatusDto,
  ): Promise<Book> {
    const book = await this.bookRepository.findOne(id);
    if (!book) {
      throw new Http400Exception(messages.book_not_exist);
    }
    book.is_archived = status === Status.True;
    await this.bookRepository.save(book);
    const bookResponse = await this.bookRepository
      .createQueryBuilder('book')
      .select([
        'book.id',
        'book.name',
        'book.description',
        'book.author',
        'category',
        'source',
        'book.created_at',
        'book.created_by',
        'book.is_archived',
        'book.is_owned',
        'book.updated_at',
        'borrower_record.id',
        'borrower_record.current_status',
        'borrower_record.updated_at',
        'account.id',
        'account.name',
      ])
      .leftJoinAndSelect('book.source', 'source')
      .leftJoinAndSelect('book.category', 'category')
      .leftJoin('book.borrower_record', 'borrower_record')
      .leftJoin('borrower_record.account', 'account')
      .where('(book.id = :id)', { id })
      .getOne();
    return bookResponse;
  }
}

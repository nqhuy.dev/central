import { Repository, EntityRepository } from 'typeorm';
import { BookCategory } from './category.entity';

@EntityRepository(BookCategory)
export class BookCategoryRepository extends Repository<BookCategory> {
  public async getOrCreateIfNotExist(nameOrId: string): Promise<BookCategory> {
    const categoryExist = await this.findOne({
      where: [{ name: nameOrId }, { id: nameOrId }],
    });
    if (categoryExist) return categoryExist;
    const newCategory = this.create();
    newCategory.name = nameOrId;
    return await this.save(newCategory);
  }
}

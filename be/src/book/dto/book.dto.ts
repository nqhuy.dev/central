import { IsString, IsOptional, IsEnum } from 'class-validator';
import { Status } from '../book.enum';

export class BookDto {
  @IsString()
  name: string;

  @IsOptional()
  @IsString()
  description: string;

  @IsString()
  author: string;

  @IsString()
  category: string;

  @IsString()
  source: string;

  @IsOptional()
  @IsString()
  @IsEnum(Status)
  isOwned: string;
}

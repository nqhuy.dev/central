import { IsOptional, IsString } from 'class-validator';

export default class SearchNameDto {
  @IsOptional()
  @IsString()
  search: string;
}

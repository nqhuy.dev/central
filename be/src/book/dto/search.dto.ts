import { IsString, IsOptional, IsNumber } from 'class-validator';

export class BookSearchDto {
  @IsOptional()
  @IsString()
  name: string;

  @IsOptional()
  @IsString()
  author: string;

  @IsOptional()
  @IsString()
  category: string;

  @IsOptional()
  @IsString()
  source: string;

  @IsOptional()
  @IsString()
  isOwned: string;

  @IsOptional()
  @IsString()
  isArchived: string;

  @IsOptional()
  @IsString()
  createdBy: string;

  @IsOptional()
  @IsNumber()
  page: number;

  @IsOptional()
  @IsNumber()
  numOfPage: number;

  @IsOptional()
  @IsString()
  keyword: string;
}

import { IsString, IsEnum } from 'class-validator';
import { Status } from '../book.enum';

export class StatusDto {
  @IsString()
  @IsEnum(Status)
  status: string;
}

import {
  BeforeInsert,
  Column,
  Entity,
  JoinColumn,
  OneToMany,
  PrimaryColumn,
} from 'typeorm';
import { nanoid } from 'nanoid';
import { Book } from './book.entity';

@Entity({ name: 'book_source' })
export class BookSource {
  @PrimaryColumn({
    name: 'id',
  })
  public id: string;

  @Column({
    name: 'name',
    unique: true,
  })
  public name: string;

  @Column({
    name: 'description',
    default: '',
  })
  public description: string;

  // Relation
  @OneToMany(() => Book, (book) => book.source)
  @JoinColumn()
  books: Book;

  @BeforeInsert()
  updateBeforeInsert() {
    const idStr = nanoid(16);
    this.id = `BOKxSRCx${idStr}`;
  }
}

import { Repository, EntityRepository } from 'typeorm';
import { BookSource } from './source.entity';

@EntityRepository(BookSource)
export class BookSourceRepository extends Repository<BookSource> {
  public async getOrCreateIfNotExist(nameOrId: string): Promise<BookSource> {
    const sourceExist = await this.findOne({
      where: [{ name: nameOrId }, { id: nameOrId }],
    });
    if (sourceExist) return sourceExist;
    const newSource = this.create();
    newSource.name = nameOrId;
    return await this.save(newSource);
  }
}

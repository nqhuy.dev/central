import { Test, TestingModule } from '@nestjs/testing';
import { BorrowerRecordController } from './borrower-record.controller';

describe('BorrowerRecordController', () => {
  let controller: BorrowerRecordController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [BorrowerRecordController],
    }).compile();

    controller = module.get<BorrowerRecordController>(BorrowerRecordController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});

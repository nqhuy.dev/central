import {
  Body,
  Controller,
  Get,
  Param,
  Patch,
  Post,
  Query,
  Req,
  UseGuards,
} from '@nestjs/common';
import RequestWithAccount from '../auth/dto/reqWithAccount.interface';
import JwtAuthGuard from '../auth/gaurd/jwtAuth.gaurd';
import { BorrowerRecord } from './borrower-record.entity';
import { Action } from './borrower-record.enum';
import { BorrowerRecordService } from './borrower-record.service';
import { ActionDto } from './dto/action.dto';
import { CreateDto } from './dto/creat.dto';
import { IdDto } from './dto/id.dto';
import { SearchDto } from './dto/search.dto';
import { StatusDto } from './dto/status.dto';
import { UpdateBooksDto } from './dto/updateBooks.dto';

@Controller('io')
export class BorrowerRecordController {
  constructor(private readonly borrowerRecordSrv: BorrowerRecordService) {}

  @Post()
  @UseGuards(JwtAuthGuard)
  public async create(
    @Body() createInfo: CreateDto,
    @Req() { user }: RequestWithAccount,
  ): Promise<BorrowerRecord> {
    try {
      const info = await this.borrowerRecordSrv.create(createInfo, user);
      return info;
    } catch (e) {
      throw e;
    }
  }

  // Mark as 'Unknown' - Revert
  @Patch(':id/action/:action')
  @UseGuards(JwtAuthGuard)
  public async updateStatusById(
    @Param() { id, action }: ActionDto,
    @Query() { status }: StatusDto,
    @Req() { user }: RequestWithAccount,
  ): Promise<BorrowerRecord> {
    try {
      const info =
        action === Action.MarkAsUnknown
          ? await this.borrowerRecordSrv.markUnknown(id, user.role)
          : await this.borrowerRecordSrv.revertUnknown(id, status, user.role);
      return info;
    } catch (e) {
      throw e;
    }
  }

  @Patch(':id/return-io')
  @UseGuards(JwtAuthGuard)
  public async returnBooks(
    @Param() { id }: IdDto,
    @Body() { books, forceMissed, forceUnknown }: UpdateBooksDto,
    @Req() { user }: RequestWithAccount,
  ) {
    try {
      const info = await this.borrowerRecordSrv.returnIO(
        id,
        books || [],
        forceMissed || false,
        forceUnknown || false,
        user,
      );
      return info;
    } catch (e) {
      throw e;
    }
  }

  @Get()
  @UseGuards(JwtAuthGuard)
  public async searchIOs(
    @Query() searchInfo: SearchDto,
    @Req() { user }: RequestWithAccount,
  ) {
    try {
      const { search, account, isBorrowing, page, numOfPage } = searchInfo;
      const [data, count] = await this.borrowerRecordSrv.search(
        search,
        account,
        isBorrowing === '1',
        page || 1,
        numOfPage || 20,
        user,
      );
      return { data, count };
    } catch (e) {
      throw e;
    }
  }

  @Get(':id')
  @UseGuards(JwtAuthGuard)
  public async getById(
    @Param() { id }: IdDto,
    @Req() { user }: RequestWithAccount,
  ) {
    try {
      const info = await this.borrowerRecordSrv.getById(id, user);
      return info;
    } catch (e) {
      throw e;
    }
  }
}

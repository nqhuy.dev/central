import {
  BeforeInsert,
  Column,
  CreateDateColumn,
  DeleteDateColumn,
  Entity,
  ManyToMany,
  ManyToOne,
  PrimaryColumn,
  UpdateDateColumn,
} from 'typeorm';
import { nanoid } from 'nanoid';
import { Account } from '../account/account.entity';
import { Book } from '../book/book.entity';
import { Status } from './borrower-record.enum';

@Entity({ name: 'borrower_record' })
export class BorrowerRecord {
  @PrimaryColumn({
    name: 'id',
  })
  public id: string;

  @Column({
    name: 'note',
    default: '',
  })
  public note: string;

  @Column({
    name: 'note_history',
    default: '',
  })
  public note_history: string;

  @Column({
    name: 'current_status',
    default: Status.Borrowing,
  })
  current_status: string;

  @Column({
    name: 'returned_at',
    default: 0,
  })
  public returned_at: number;

  @CreateDateColumn()
  created_at: Date;

  @UpdateDateColumn()
  updated_at: Date;

  @DeleteDateColumn()
  deleted_at: Date;

  // Relation
  @ManyToOne(() => Account, (account) => account.borrower_record)
  account: Account;

  @ManyToMany(() => Book, (book) => book.borrower_record)
  books: Book[];

  @BeforeInsert()
  updateBeforeInsert() {
    const idStr = nanoid(16);
    this.id = `IOx${idStr}`;
  }
}

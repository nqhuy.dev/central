export enum Status {
  Borrowing = 'borrowing',
  Returned = 'returned',
  // Missed = 'missed',
  Unknown = 'unknown',
}

export enum Action {
  MarkAsUnknown = 'mark-as-unknown',
  Revert = 'revert',
}

export enum Bool {
  True = '1',
  False = '0',
}

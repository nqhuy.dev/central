import { forwardRef, Module } from '@nestjs/common';
import { BorrowerRecordService } from './borrower-record.service';
import { BorrowerRecordController } from './borrower-record.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { BorrowerRecordRepository } from './borrower-record.repository';
import { AccountModule } from '../account/account.module';
import { BookModule } from '../book/book.module';
import { ConfigModule } from '@nestjs/config';

@Module({
  imports: [
    TypeOrmModule.forFeature([BorrowerRecordRepository]),
    AccountModule,
    forwardRef(() => BookModule),
    ConfigModule,
  ],
  providers: [BorrowerRecordService],
  controllers: [BorrowerRecordController],
  exports: [BorrowerRecordService, TypeOrmModule],
})
export class BorrowerRecordModule {}

import { Repository, EntityRepository } from 'typeorm';
import { BorrowerRecord } from './borrower-record.entity';

@EntityRepository(BorrowerRecord)
export class BorrowerRecordRepository extends Repository<BorrowerRecord> {}

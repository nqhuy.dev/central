import { Test, TestingModule } from '@nestjs/testing';
import { BorrowerRecordService } from './borrower-record.service';

describe('BorrowerRecordService', () => {
  let service: BorrowerRecordService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [BorrowerRecordService],
    }).compile();

    service = module.get<BorrowerRecordService>(BorrowerRecordService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});

import { forwardRef, Inject, Injectable } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import { InjectRepository } from '@nestjs/typeorm';
import * as moment from 'moment';
import roles from '../../utils/constants/roles';
import { Http311Exception } from '../../utils/Exceptions/http311.exception';
import { Http400Exception } from '../../utils/Exceptions/http400.exception';
import { Http403Exception } from '../../utils/Exceptions/http403.exception';
import { Account } from '../account/account.entity';
import { AccountService } from '../account/account.service';
import { BookService } from '../book/book.service';
import { BorrowerRecord } from './borrower-record.entity';
import { Bool, Status } from './borrower-record.enum';
import { BorrowerRecordRepository } from './borrower-record.repository';
import { CreateDto } from './dto/creat.dto';

const { isAdmin } = roles;

@Injectable()
export class BorrowerRecordService {
  constructor(
    @InjectRepository(BorrowerRecordRepository)
    private readonly borrowerRecordRepository: BorrowerRecordRepository,
    @Inject(forwardRef(() => BookService))
    private readonly bookSrv: BookService,
    private readonly accountSrv: AccountService,
    private readonly configSrv: ConfigService,
  ) {}

  public async create(
    info: CreateDto,
    admin: Account,
  ): Promise<BorrowerRecord> {
    if (!isAdmin(admin.role)) {
      throw new Http403Exception();
    }
    // Get and check borrower
    const borrower = await this.accountSrv.findById(info.accountId, 2);
    const borrowingItem = await this.borrowerRecordRepository.findOne({
      relations: ['account', 'books'],
      where: { account: borrower, current_status: Status.Borrowing },
    });
    if (borrowingItem && info.isJoinWithBorrowing !== Bool.True) {
      throw new Http400Exception(
        `This user have a 'borrower-record' in the past (#${borrowingItem.id}), please finish it to create a new (or join with it)!`,
      );
    }
    // Check books
    const { valid, invalidIds } = await this.bookSrv.checkBooks(info.books);
    if (invalidIds.length) {
      throw new Http400Exception(
        `Can not find information for books with ids [${invalidIds.join(
          ', ',
        )}]`,
      );
    }
    if (borrowingItem) {
      borrowingItem.note =
        info.note ||
        `Add [${valid.map((book) => book.id).join(', ')}] at ${moment(
          'HH:mm - DD/MM/YYYY',
        )}`;
      borrowingItem.books = [...borrowingItem.books, ...valid];
      await this.borrowerRecordRepository.save(borrowingItem);
      return borrowingItem;
    }
    const newBR = this.borrowerRecordRepository.create();
    newBR.note = info.note;
    newBR.account = borrower;
    newBR.books = valid;
    await this.borrowerRecordRepository.save(newBR);
    return newBR;
  }

  // Return IO - >>>>> [NEED TO UPDATE LOGIC]
  public async returnIO(
    id: string,
    bookIds: string[],
    forceMissed: boolean,
    forceUnknown: boolean,
    admin: Account,
  ): Promise<BorrowerRecord> {
    if (!isAdmin(admin.role)) {
      throw new Http403Exception();
    }
    const io = await this.borrowerRecordRepository.findOne(id, {
      relations: ['account'],
      where: { current_status: Status.Borrowing },
    });
    // Check IO
    if (!io) {
      throw new Http400Exception(
        `Record #${id} was not exist (or not in 'Borrowing' status)`,
      );
    }
    const borrower = await this.accountSrv.findById(io.account.id, 2);
    // Get IO full information
    const ioInfo = await this.getById(id, io.account);
    // Check books
    const { valid, invalidIds, validIds } = await this.bookSrv.checkBooks(
      bookIds,
    );
    if (invalidIds.length) {
      throw new Http400Exception(
        `Can't find information for books with ids [${invalidIds.join(', ')}]`,
      );
    }
    // Compare books-borrowed and book-return
    if (valid.length < ioInfo.books.length) {
      // Missed some book
      const missedBooks = ioInfo.books
        .map((book) => book.id)
        .filter((id) => !validIds.includes(id));
      const returnedBooks = ioInfo.books
        .map((book) => book.id)
        .filter((id) => !validIds.includes(id));
      if (returnedBooks.length === 0) {
        throw new Http400Exception(
          `No books was found to update to 'returned' [${returnedBooks.join(
            ', ',
          )}]`,
        );
      }
      // Mark 'returned' for valid book
      io.current_status = Status.Returned;
      await this.borrowerRecordRepository.save(io);
      if (forceUnknown && missedBooks.length) {
        const unknownBr = this.borrowerRecordRepository.create();
        unknownBr.note = `Forced to unknown for #${ioInfo.id}`;
        unknownBr.account = borrower;
        unknownBr.books = ioInfo.books.filter(
          (book) => !validIds.includes(book.id),
        );
        unknownBr.current_status = Status.Unknown;
        await this.borrowerRecordRepository.save(unknownBr);
        unknownBr.account.password = undefined;
        unknownBr.account.access_sessions = undefined;
        unknownBr.account.is_waiting_accept = undefined;
        unknownBr.account.is_locked = undefined;
        return unknownBr;
      }
      if (forceMissed && missedBooks.length) {
        const missedBr = this.borrowerRecordRepository.create();
        missedBr.note = `Missed from #${ioInfo.id}`;
        missedBr.account = borrower;
        missedBr.books = ioInfo.books.filter(
          (book) => !validIds.includes(book.id),
        );
        missedBr.current_status = Status.Borrowing;
        await this.borrowerRecordRepository.save(missedBr);
        missedBr.account.password = undefined;
        missedBr.account.access_sessions = undefined;
        missedBr.account.is_waiting_accept = undefined;
        missedBr.account.is_locked = undefined;
        return missedBr;
      }
      throw new Http311Exception(
        `Maybe this user missed some book [${missedBooks.join(', ')}]`,
      );
    }
    io.current_status = Status.Returned;
    await this.borrowerRecordRepository.save(io);
    // Remove important field
    io.account.password = undefined;
    io.account.access_sessions = undefined;
    io.account.is_waiting_accept = undefined;
    io.account.is_locked = undefined;
    return io;
  }

  // Mark unknown or revert
  public async markUnknown(id: string, role: string): Promise<BorrowerRecord> {
    if (!isAdmin(role)) {
      throw new Http403Exception();
    }
    const io = await this.borrowerRecordRepository.findOne(id);
    if (!io) {
      throw new Http400Exception(`Record #${id} was not exist`);
    }
    // Valid current status
    if (io.current_status === Status.Returned) {
      throw new Http400Exception('This books are returned');
    }
    if (io.current_status === Status.Unknown) {
      throw new Http400Exception(
        `This book is already in '${Status.Unknown}' status`,
      );
    }
    io.current_status = Status.Unknown;
    // Send notify to slack
    return await this.borrowerRecordRepository.save(io);
  }

  public async revertUnknown(
    id: string,
    status: string,
    role: string,
  ): Promise<BorrowerRecord> {
    if (!isAdmin(role)) {
      throw new Http403Exception();
    }
    const io = await this.borrowerRecordRepository.findOne(id, {
      where: {
        current_status: Status.Unknown,
      },
    });
    if (!io) {
      throw new Http400Exception(
        `Record #${id} was not exist or not in 'Unknown' status`,
      );
    }
    if (status === Status.Unknown || status === Status.Returned) {
      throw new Http400Exception('Invalid status to revert');
    }
    io.current_status = status;
    // Send a notify to slack
    return await this.borrowerRecordRepository.save(io);
  }

  // Search all IO
  public async search(
    search: string,
    account: string,
    isBorrowing = false,
    page: number,
    numOfPage: number,
    user: Account,
  ): Promise<[d: BorrowerRecord[], c: number]> {
    const isRoleAdmin = isAdmin(user.role);
    const skip = (Number(page) - 1) * numOfPage;
    let query = this.borrowerRecordRepository
      .createQueryBuilder('borrower_record')
      .leftJoinAndSelect('borrower_record.books', 'book')
      .leftJoinAndSelect('borrower_record.account', 'account')
      .select([
        'borrower_record.id',
        'borrower_record.returned_at',
        'borrower_record.current_status',
        'borrower_record.created_at',
        'account.id',
        'account.name',
        'book.id',
        'book.name',
        'book.author',
      ]);
    if (search && search.trim() !== '') {
      query = query.where(
        'account.name ilike :search or account.id ilike :search',
        {
          search: `%${search}%`,
        },
      );
    }
    if (account && account.trim() !== '') {
      query = query.andWhere('account.id = :account', { account });
    }
    if (isBorrowing) {
      query = query.andWhere('borrower_record.current_status = :status', {
        status: Status.Borrowing,
      });
    }
    if (!isRoleAdmin) {
      query = query.andWhere('account.id = :account_id', {
        account_id: user.id,
      });
    }
    const [data, count] = await query
      .skip(skip)
      .take(numOfPage)
      .getManyAndCount();
    return [data, count];
  }

  public async advancedSearch(remainDates: number): Promise<BorrowerRecord[]> {
    const maxBorrowDays = Number(this.configSrv.get('MAX_BORROW_DATE') || 30);
    const deadline = moment()
      .subtract(maxBorrowDays - remainDates, 'days')
      .format('DD MM YYYY');
    const data = await this.borrowerRecordRepository
      .createQueryBuilder('borrower_record')
      .leftJoinAndSelect('borrower_record.account', 'account')
      .leftJoinAndSelect('borrower_record.books', 'book')
      .select([
        'borrower_record.id',
        'borrower_record.created_at',
        'book',
        'account',
        "TO_CHAR(borrower_record.created_at, 'DD MM YYYY')",
      ])
      .where("TO_CHAR(borrower_record.created_at, 'DD MM YYYY') = :deadline", {
        deadline,
      })
      .getMany();
    return data;
  }

  // Get IO by ID
  public async getById(id: string, user: Account): Promise<BorrowerRecord> {
    let query = this.borrowerRecordRepository
      .createQueryBuilder('borrower_record')
      .leftJoinAndSelect('borrower_record.books', 'book')
      .leftJoinAndSelect('borrower_record.account', 'account')
      .select([
        'borrower_record.id',
        'account.id',
        'account.name',
        'book.id',
        'book.name',
      ])
      .where('borrower_record.id = :record_id', { record_id: id });
    if (!isAdmin(user.role)) {
      query = query.andWhere('account.id = :account_id', {
        account_id: user.id,
      });
    }
    const data = await query.getOne();
    return data;
  }

  public async findWithBookId(
    id: string,
    ioStatus = Status.Borrowing,
  ): Promise<BorrowerRecord> {
    const info = await this.borrowerRecordRepository
      .createQueryBuilder('borrower_record')
      .leftJoinAndSelect('borrower_record.account', 'account')
      .leftJoinAndSelect('borrower_record.books', 'book')
      .where('book.id = :book_id', { book_id: id })
      .andWhere('current_status = :status', { status: ioStatus })
      .getOne();
    return info;
  }
}

import { IsEnum, IsString } from 'class-validator';
import { Action } from '../borrower-record.enum';

export class ActionDto {
  @IsString()
  id: string;

  @IsString()
  @IsEnum(Action)
  action: string;
}

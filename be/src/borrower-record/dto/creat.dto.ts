import { IsArray, IsEnum, IsOptional, IsString } from 'class-validator';
import { Bool } from '../borrower-record.enum';

export class CreateDto {
  @IsOptional()
  @IsString()
  @IsEnum(Bool)
  isJoinWithBorrowing: string;

  @IsOptional()
  @IsString()
  note: string;

  @IsArray()
  books: string[];

  @IsString()
  accountId: string;
}

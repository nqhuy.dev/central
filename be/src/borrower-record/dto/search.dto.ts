import { IsString, IsOptional, IsNumber } from 'class-validator';

export class SearchDto {
  @IsOptional()
  @IsString()
  search: string;

  @IsOptional()
  @IsString()
  account: string;

  @IsOptional()
  @IsString()
  isBorrowing: string;

  @IsOptional()
  @IsNumber()
  page: number;

  @IsOptional()
  @IsNumber()
  numOfPage: number;
}

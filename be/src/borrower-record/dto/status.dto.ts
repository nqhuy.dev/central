import { IsEnum, IsOptional, IsString } from 'class-validator';
import { Status } from '../borrower-record.enum';

export class StatusDto {
  @IsOptional()
  @IsString()
  @IsEnum(Status)
  status: string;
}

import { IsArray, IsBoolean, IsOptional } from 'class-validator';

export class UpdateBooksDto {
  @IsArray()
  books: string[];

  @IsOptional()
  @IsBoolean()
  forceMissed: boolean;

  @IsOptional()
  @IsBoolean()
  forceUnknown: boolean;
}

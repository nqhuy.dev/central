import { Module } from '@nestjs/common';
import { ConfigModule } from '@nestjs/config';
import { BorrowerRecordModule } from '../borrower-record/borrower-record.module';
import { IntegrateModule } from '../integrate/integrate.module';
import { MailModule } from '../mail/mail.module';
import { CronjobService } from './cronjob.service';

@Module({
  imports: [ConfigModule, BorrowerRecordModule, IntegrateModule, MailModule],
  providers: [CronjobService],
})
export class CronjobModule {}

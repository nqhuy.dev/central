import { Injectable, Logger } from '@nestjs/common';
import { Cron } from '@nestjs/schedule';
import email from '../../utils/constants/email';
import * as moment from 'moment';
import { BorrowerRecordService } from '../borrower-record/borrower-record.service';
import { IntegrateService } from '../integrate/integrate.service';
import { MailService } from '../mail/mail.service';

const { REMIND_RETURN } = email;

@Injectable()
export class CronjobService {
  constructor(
    private readonly borrowerRecordSrv: BorrowerRecordService,
    private readonly integrateSrv: IntegrateService,
    private readonly mailSrv: MailService,
  ) {}

  private readonly logger = new Logger(CronjobService.name);

  @Cron('0 7 * * *', {
    name: 'Daily Reminder',
    timeZone: 'Asia/Ho_Chi_Minh',
  }) // 7am every days
  async remindReturnBooks() {
    this.logger.log(`[${moment.now()}] - Remind user return their books -----`);
    const IOs = await this.borrowerRecordSrv.advancedSearch(7);
    const accounts = IOs.map((d) => d.account);
    const msgBlocks = [];
    const msgBlocksException = [];
    if (!accounts.length) {
      msgBlocks.push({
        content: `\`\`\`No user need to remind return books today\`\`\``,
      });
      await this.integrateSrv.sendMessageToSlack(
        null,
        null,
        'Daily remind return books (in 7 days)',
        msgBlocks,
      );
      this.logger.log(`No accounts was found to send remind!`);
      return;
    }
    msgBlocks.push({
      content: `\`\`\`${accounts.join('\n')}\`\`\`\`\``,
    });
    this.logger.log(`[-] Have ${accounts.length} user need to be remind today`);
    const errEmails = [];
    accounts.forEach(async (acc) => {
      const status = await this.mailSrv.send(acc, REMIND_RETURN, {
        days: 7,
      });
      if (status) {
        this.logger.log(`    -> Sent an email to ${acc.email}`);
      } else {
        this.logger.log(`    -> Failed to send an email to ${acc.email}`);
        errEmails.push(`[${acc.id}] - ${acc.email} - ${acc.name}`);
      }
    });
    if (errEmails.length) {
      msgBlocksException.push({
        content: `\`\`\`${errEmails.join('\n')}\`\`\``,
      });
      await this.integrateSrv.sendMessageToSlack(
        null,
        null,
        'Daily remind return books (in 7 days) - Email errors',
        msgBlocksException,
      );
    }
  }
}

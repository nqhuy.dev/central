import { IsString } from 'class-validator';

export default class CreateReportDto {
  @IsString()
  title: string;

  @IsString()
  content: string;

  @IsString()
  bookId: string;
}

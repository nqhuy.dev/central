import {
  IsEnum,
  IsOptional,
  IsString,
  MaxLength,
  MinLength,
} from 'class-validator';
import { Type } from '../e-book.enum';

export default class CreateDto {
  @IsString()
  @MinLength(4)
  @MaxLength(50)
  name: string;

  @IsString()
  @IsEnum(Type)
  type: string;

  @IsOptional()
  @IsString()
  fileCode: string;

  @IsOptional()
  @IsString()
  content: string;
}

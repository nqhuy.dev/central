import { IsString, IsEnum } from 'class-validator';
import { Action } from '../e-book.enum';

export class IdStatusDto {
  @IsString()
  id: string;

  @IsString()
  @IsEnum(Action)
  action: string;
}

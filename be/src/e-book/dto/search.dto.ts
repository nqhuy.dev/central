import { IsEnum, IsNumber, IsOptional, IsString } from 'class-validator';
import { Status, Type } from '../e-book.enum';

export class SearchDto {
  @IsOptional()
  @IsString()
  search: string;

  @IsOptional()
  @IsEnum(Type)
  @IsString()
  type: string;

  @IsOptional()
  @IsEnum(Status)
  @IsString()
  isWaitingAccept: string;

  @IsOptional()
  @IsEnum(Status)
  @IsString()
  isArchived: string;

  @IsOptional()
  @IsString()
  createdBy: string;

  @IsOptional()
  @IsNumber()
  page: number;

  @IsOptional()
  @IsNumber()
  numOfPage: number;
}

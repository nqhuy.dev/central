import { Repository, EntityRepository } from 'typeorm';
import { EBookReport } from './report.entity';

@EntityRepository(EBookReport)
export class EBookReportRepository extends Repository<EBookReport> {}

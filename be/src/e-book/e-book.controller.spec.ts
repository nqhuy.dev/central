import { Test, TestingModule } from '@nestjs/testing';
import { EBookController } from './e-book.controller';

describe('EBookController', () => {
  let controller: EBookController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [EBookController],
    }).compile();

    controller = module.get<EBookController>(EBookController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});

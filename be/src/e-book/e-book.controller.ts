import {
  Body,
  Controller,
  Get,
  Param,
  Patch,
  Post,
  Query,
  Req,
  UseGuards,
} from '@nestjs/common';
import RequestWithAccount from '../auth/dto/reqWithAccount.interface';
import JwtAuthGuard from '../auth/gaurd/jwtAuth.gaurd';
import CreateReportDto from './dto/create-report.dto';
import CreateDto from './dto/create.dto';
import { IdDto } from './dto/id.dto';
import { IdStatusDto } from './dto/idStatus.dto';
import { SearchDto } from './dto/search.dto';
import { EBook } from './e-book.entity';
import { Status } from './e-book.enum';
import { EBookService } from './e-book.service';
import { EBookReport } from './report.entity';

@Controller('e-books')
export class EBookController {
  constructor(private readonly eBookSrv: EBookService) {}

  @Post()
  @UseGuards(JwtAuthGuard)
  public async create(
    @Req() { user }: RequestWithAccount,
    @Body() bookInfo: CreateDto,
  ) {
    try {
      const result = await this.eBookSrv.create(bookInfo, user);
      result.is_archived = undefined;
      return result;
    } catch (e) {
      throw e;
    }
  }

  @Get()
  @UseGuards(JwtAuthGuard)
  public async search(
    @Query() searchInfo: SearchDto,
    @Req() { user }: RequestWithAccount,
  ): Promise<{ data: EBook[]; count: number }> {
    try {
      const {
        search,
        type,
        createdBy,
        isArchived,
        isWaitingAccept,
        page,
        numOfPage,
      } = searchInfo;
      const is_waiting_accept =
        isWaitingAccept === Status.True ? 'true' : 'false';
      const is_archived = isArchived === Status.True ? 'true' : 'false';
      const [data, count] = await this.eBookSrv.search(
        search || '',
        type,
        createdBy,
        isArchived ? is_archived : '*',
        isWaitingAccept ? is_waiting_accept : '*',
        page || 1,
        numOfPage || 20,
        user,
      );
      return { data, count };
    } catch (e) {
      throw e;
    }
  }

  @Get(':id')
  @UseGuards(JwtAuthGuard)
  public async getById(
    @Param() { id }: IdDto,
    @Req() { user }: RequestWithAccount,
  ): Promise<EBook> {
    try {
      const book = await this.eBookSrv.getById(id, user);
      return book;
    } catch (e) {
      throw e;
    }
  }

  @Patch(':id/action/:action')
  @UseGuards(JwtAuthGuard)
  public async updateStatusById(
    @Param() { id, action }: IdStatusDto,
    @Req() { user }: RequestWithAccount,
  ): Promise<boolean> {
    try {
      const book = await this.eBookSrv.updateStatusById(id, action, user);
      return book;
    } catch (e) {
      throw e;
    }
  }

  // Report
  @Post('i/report')
  @UseGuards(JwtAuthGuard)
  public async createReport(
    @Body() info: CreateReportDto,
    @Req() { user }: RequestWithAccount,
  ): Promise<EBookReport> {
    try {
      const data = await this.eBookSrv.report(info, user);
      return data;
    } catch (e) {
      throw e;
    }
  }

  @Get('i/reports')
  @UseGuards(JwtAuthGuard)
  public async getReports(
    @Req() { user }: RequestWithAccount,
  ): Promise<{ data: EBookReport[]; count: number }> {
    try {
      const [data, count] = await this.eBookSrv.getReports(user);
      return { data, count };
    } catch (e) {
      throw e;
    }
  }

  @Get('i/reports/:id')
  @UseGuards(JwtAuthGuard)
  public async getReportByID(
    @Param() { id }: IdDto,
    @Req() { user }: RequestWithAccount,
  ): Promise<EBookReport> {
    try {
      const data = await this.eBookSrv.getReportById(id, user);
      return data;
    } catch (e) {
      throw e;
    }
  }
}

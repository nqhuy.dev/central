import {
  Column,
  Entity,
  PrimaryColumn,
  CreateDateColumn,
  UpdateDateColumn,
  DeleteDateColumn,
  JoinColumn,
  ManyToOne,
  BeforeInsert,
} from 'typeorm';
import { nanoid } from 'nanoid';
import { Account } from 'src/account/account.entity';
import { Type } from './e-book.enum';

@Entity({ name: 'e_book' })
export class EBook {
  @PrimaryColumn({
    name: 'id',
  })
  public id: string;

  @Column({
    name: 'name',
  })
  name: string;

  @Column({
    name: 'type',
    enum: Type,
    default: Type.Text,
  })
  type: string;

  @Column({
    name: 'content',
  })
  content: string;

  @Column({
    name: 'is_waiting_accept',
    default: true,
  })
  is_waiting_accept: boolean;

  @Column({
    name: 'is_archived',
    default: false,
  })
  is_archived: boolean;

  @CreateDateColumn()
  created_at: Date;

  @UpdateDateColumn()
  updated_at: Date;

  @DeleteDateColumn()
  deleted_at: Date;

  // Relation
  @ManyToOne(() => Account)
  @JoinColumn()
  account: Account;

  @BeforeInsert()
  updateBeforeInsert() {
    const idStr = nanoid(16);
    this.id = `EBKx${idStr}`;
  }
}

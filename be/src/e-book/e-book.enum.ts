export enum Status {
  True = '1',
  False = '0',
}

export enum ReportStatus {
  Waiting = 'waiting',
  Handling = 'handling',
  Done = 'done',
}

export enum Type {
  File = 'file',
  Text = 'text',
}

export enum Action {
  Accept = 'accept',
  Archive = 'archive',
  UnArchive = 'un-archive',
  Delete = 'delete',
}

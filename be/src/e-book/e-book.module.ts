import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { AccountModule } from '../account/account.module';
import { EBookReportRepository } from './e-book-report.repository';
import { EBookController } from './e-book.controller';
import { EBookRepository } from './e-book.repository';
import { EBookService } from './e-book.service';

@Module({
  imports: [
    TypeOrmModule.forFeature([EBookRepository, EBookReportRepository]),
    AccountModule,
  ],
  controllers: [EBookController],
  providers: [EBookService],
  exports: [EBookService],
})
export class EBookModule {}

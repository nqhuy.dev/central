import { Repository, EntityRepository } from 'typeorm';
import { EBook } from './e-book.entity';

@EntityRepository(EBook)
export class EBookRepository extends Repository<EBook> {}

import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Account } from '../account/account.entity';
import CreateDto from './dto/create.dto';
import { EBook } from './e-book.entity';
import { Action, Type } from './e-book.enum';
import { EBookRepository } from './e-book.repository';
import roles from '../../utils/constants/roles';
import { Http400Exception } from '../../utils/Exceptions/http400.exception';
import { Http403Exception } from '../../utils/Exceptions/http403.exception';
import CreateReportDto from './dto/create-report.dto';
import { EBookReport } from './report.entity';
import { EBookReportRepository } from './e-book-report.repository';
import { Role } from '../account/account.enum';

const { isAdmin, isNeedAdminCheck } = roles;

@Injectable()
export class EBookService {
  constructor(
    @InjectRepository(EBookRepository)
    private readonly ebookRepository: EBookRepository,
    @InjectRepository(EBookReportRepository)
    private readonly eBookReportRepository: EBookReportRepository,
  ) {}

  public async create(info: CreateDto, created_by: Account): Promise<EBook> {
    const { name, type, content, fileCode } = info;
    if (
      (type === Type.File && (!fileCode || !fileCode.trim())) ||
      (type === Type.Text && (!content || !content.trim()))
    ) {
      throw new Http400Exception('Invalid input');
    }
    const newBook = this.ebookRepository.create();
    newBook.name = name;
    newBook.type = type;
    newBook.content = type === Type.Text ? content : info.fileCode;
    newBook.is_waiting_accept = isNeedAdminCheck(created_by.role);
    newBook.account = created_by;

    return await this.ebookRepository.save(newBook);
  }

  public async search(
    search: string,
    type: string,
    created_by: string,
    is_archived: string,
    is_waiting_accept: string,
    page: number,
    numOfPage: number,
    user: Account,
  ): Promise<[d: EBook[], c: number]> {
    const skip = (Number(page) - 1) * numOfPage;
    const isRoleAdmin = isAdmin(user.role);
    const otherFields = isRoleAdmin
      ? ['e_book.is_archived', 'e_book.is_waiting_accept']
      : [];
    let query = this.ebookRepository
      .createQueryBuilder('e_book')
      .leftJoinAndSelect('e_book.account', 'account')
      .select([
        'e_book.id',
        'e_book.name',
        'e_book.type',
        'e_book.created_at',
        'account.id',
        'account.name',
        ...otherFields,
      ])
      .where('e_book.name ilike :search', {
        search: `%${search}%`,
      });
    // Other condition
    if (type) {
      query = query.andWhere('e_book.type = :type ', {
        type,
      });
    }
    if (isRoleAdmin) {
      if (is_archived === 'false' || is_archived === 'true') {
        query = query.andWhere('e_book.is_archived = :is_archived ', {
          is_archived,
        });
      }
      if (is_archived === 'false' || is_archived === 'true') {
        query = query.andWhere(
          'e_book.is_waiting_accept = :is_waiting_accept',
          {
            is_waiting_accept,
          },
        );
      }
      if (created_by) {
        query = query.andWhere('account.name = :created_by', { created_by });
      }
    }
    const queryWithPagination = query.skip(skip).take(numOfPage);
    const [eBooks, count] = isRoleAdmin
      ? await queryWithPagination.getManyAndCount()
      : await queryWithPagination
          .andWhere(
            '((e_book.is_archived <> true OR account.id = :accountId) AND e_book.is_waiting_accept <> true)',
            { accountId: user.id },
          )
          .getManyAndCount();
    return [eBooks, count];
  }

  public async getById(bookId: string, user: Account): Promise<EBook> {
    const isRoleAdmin = isAdmin(user.role);
    const book = await this.ebookRepository.findOne(bookId, {
      relations: ['account'],
      select: isRoleAdmin
        ? [
            'id',
            'name',
            'type',
            'content',
            'created_at',
            'updated_at',
            'is_archived',
            'is_waiting_accept',
            'account',
          ]
        : ['id', 'name', 'type', 'content', 'created_at', 'updated_at'],
      where: isRoleAdmin
        ? []
        : [
            { is_archived: true, 'account.id': user.id },
            { is_waiting_accept: true, 'account.id': user.id },
          ],
    });
    if (!book) {
      throw new Http400Exception("Invalid book's id");
    }
    // Remove important fields
    book.account.password = undefined;
    book.account.access_sessions = undefined;
    book.account.is_waiting_accept = undefined;
    book.account.is_locked = undefined;
    book.is_archived = isRoleAdmin ? book.is_archived : undefined;
    book.is_waiting_accept = isRoleAdmin ? book.is_waiting_accept : undefined;
    return book;
  }

  public async updateStatusById(
    bookId: string,
    action: string,
    user: Account,
  ): Promise<boolean> {
    const book = await this.ebookRepository.findOne(bookId);
    if (!book) {
      throw new Http400Exception("Invalid book's id");
    }
    if (action === Action.Delete) {
      book.deleted_at = new Date();
      await this.ebookRepository.save(book);
      return true;
    }
    const isRoleAdmin = isAdmin(user.role);
    if (!isRoleAdmin) {
      throw new Http403Exception(
        "You don't have permission to handle this action",
      );
    }
    if (action === Action.Accept) {
      book.is_waiting_accept = false;
    }
    if (action === Action.Archive || action === Action.UnArchive) {
      book.is_archived = action === Action.Archive;
    }
    await this.ebookRepository.save(book);
    return true;
  }

  // Report E-Book
  public async report(
    info: CreateReportDto,
    user: Account,
  ): Promise<EBookReport> {
    const { title, content, bookId } = info;
    const ebook = await this.ebookRepository.findOne(bookId, {
      where: {
        is_archived: false,
      },
    });
    if (!ebook) {
      throw new Http400Exception("Invalid e-book's id");
    }
    const report = this.eBookReportRepository.create();
    report.title = title;
    report.content = content;
    report.ebook = ebook;
    report.account = user;
    // [SEND_NOTIFY_SLACK]
    await this.eBookReportRepository.save(report);
    return report;
  }

  public async getReportById(id: string, user: Account): Promise<EBookReport> {
    const isUser = user.role === Role.User;
    let queryReport = this.eBookReportRepository
      .createQueryBuilder('report_eb')
      .where('report_eb.id = :id', { id })
      .leftJoinAndSelect('report_eb.account', 'account')
      .leftJoinAndSelect('report_eb.ebook', 'e_book')
      .select([
        'report_eb.id',
        'report_eb.title',
        'report_eb.content',
        'report_eb.created_at',
        'report_eb.updated_at',
        'report_eb.current_status',
        'e_book.id',
        'e_book.name',
        'account.id',
        'account.name',
      ]);
    if (isUser) {
      queryReport = queryReport.andWhere('account.id = :account_id', {
        account_id: user.id,
      });
    }
    const report = await queryReport.getOne();
    if (!report) {
      throw new Http400Exception(
        `Report #${id} is not exist ${
          isUser ? "in your report's history" : ''
        }`,
      );
    }
    return report;
  }

  public async getReports(
    user: Account,
  ): Promise<[d: EBookReport[], c: number]> {
    const isUser = user.role === Role.User;
    let queryReport = this.eBookReportRepository
      .createQueryBuilder('report_eb')
      .leftJoinAndSelect('report_eb.account', 'account')
      .leftJoinAndSelect('report_eb.ebook', 'e_book')
      .select([
        'report_eb.id',
        'report_eb.title',
        'report_eb.content',
        'report_eb.created_at',
        'report_eb.updated_at',
        'report_eb.current_status',
        'e_book.id',
        'e_book.name',
        'account.id',
        'account.name',
      ]);
    if (isUser) {
      queryReport = queryReport.andWhere('account.id = :account_id', {
        account_id: user.id,
      });
    }
    const [reports, count] = await queryReport.getManyAndCount();
    return [reports, count];
  }
}

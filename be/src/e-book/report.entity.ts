import {
  Column,
  Entity,
  PrimaryColumn,
  CreateDateColumn,
  UpdateDateColumn,
  DeleteDateColumn,
  JoinColumn,
  ManyToOne,
  BeforeInsert,
} from 'typeorm';
import { nanoid } from 'nanoid';
import { Account } from 'src/account/account.entity';
import { ReportStatus } from './e-book.enum';
import { EBook } from './e-book.entity';

@Entity({ name: 'report_eb' })
export class EBookReport {
  @PrimaryColumn({
    name: 'id',
  })
  public id: string;

  @Column({
    name: 'title',
  })
  title: string;

  @Column({
    name: 'content',
  })
  content: string;

  @Column({
    name: 'current_status',
    default: ReportStatus.Waiting,
  })
  current_status: string;

  @CreateDateColumn()
  created_at: Date;

  @UpdateDateColumn()
  updated_at: Date;

  @DeleteDateColumn()
  deleted_at: Date;

  // Relation
  @ManyToOne(() => EBook)
  @JoinColumn()
  ebook: EBook;

  @ManyToOne(() => Account)
  @JoinColumn()
  account: Account;

  @BeforeInsert()
  updateBeforeInsert() {
    const idStr = nanoid(16);
    this.id = `EBKxRPx${idStr}`;
  }
}

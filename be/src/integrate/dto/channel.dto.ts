import { IsString } from 'class-validator';

export class ChannelDto {
  @IsString()
  channel: string;
}

import { IsString } from 'class-validator';

export class SlackMsgBlockDto {
  @IsString()
  type: string;

  @IsString()
  content: string;

  @IsString()
  url: string;
}

import { Test, TestingModule } from '@nestjs/testing';
import { IntegrateController } from './integrate.controller';

describe('IntegrateController', () => {
  let controller: IntegrateController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [IntegrateController],
    }).compile();

    controller = module.get<IntegrateController>(IntegrateController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});

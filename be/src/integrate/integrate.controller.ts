import { Body, Controller, Logger, Post } from '@nestjs/common';
import { Http400Exception } from '../../utils/Exceptions/http400.exception';
import { IntegrateService } from './integrate.service';

@Controller('integrate')
export class IntegrateController {
  constructor(private readonly integrateSrv: IntegrateService) {}

  private readonly logger = new Logger(IntegrateController.name);

  @Post('slash-command')
  public async incomingHook(@Body() body: any) {
    const { token, command, text, response_url } = body;
    try {
      if (!body || !token || !command || !text || !response_url) {
        throw new Http400Exception(
          'Payload is not enough fields to handle this action!',
        );
      }
      await this.integrateSrv.handle(
        command,
        token,
        text.split(' '),
        response_url,
      );
    } catch (e) {
      this.logger.error(e);
      if (e && e.status && e.message) {
        return `*ERROR* - ${e.status}: ${e.message}`;
      }
      return 'Something went wrong when handle this action';
    }
  }
}

import { HttpModule } from '@nestjs/axios';
import { Module } from '@nestjs/common';
import { ConfigModule } from '@nestjs/config';
import { AccountModule } from '../account/account.module';
import { BookModule } from '../book/book.module';
import { BorrowerRecordModule } from '../borrower-record/borrower-record.module';
import { EBookModule } from '../e-book/e-book.module';
import { IntegrateController } from './integrate.controller';
import { IntegrateService } from './integrate.service';

@Module({
  imports: [
    HttpModule,
    AccountModule,
    BookModule,
    EBookModule,
    BorrowerRecordModule,
    ConfigModule,
  ],
  controllers: [IntegrateController],
  providers: [IntegrateService],
  exports: [IntegrateService],
})
export class IntegrateModule {}

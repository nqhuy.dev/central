import { Test, TestingModule } from '@nestjs/testing';
import { IntegrateService } from './integrate.service';

describe('IntegrateService', () => {
  let service: IntegrateService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [IntegrateService],
    }).compile();

    service = module.get<IntegrateService>(IntegrateService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});

import { HttpService } from '@nestjs/axios';
import { Injectable, Logger } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import { AxiosResponse } from 'axios';
import { lastValueFrom, map, Observable } from 'rxjs';
import { Http400Exception } from '../../utils/Exceptions/http400.exception';
import { AccountService } from '../account/account.service';
import { BookService } from '../book/book.service';
import { BorrowerRecordService } from '../borrower-record/borrower-record.service';
import { EBookService } from '../e-book/e-book.service';
import { SlackMsgBlockDto } from './dto/slack-message-block.dto';

const commandInfo = {
  '/elib': {
    accounts: {
      email: {
        description: 'Account - Get account by email',
      },
    },
    io: {
      remainIn: {
        description: 'IO - Get all users have to return books in n days',
      },
    },
  },
};

@Injectable()
export class IntegrateService {
  constructor(
    private readonly httpService: HttpService,
    private readonly configSrv: ConfigService,
    private readonly accountSrv: AccountService,
    private readonly bookSrv: BookService,
    private readonly ebookSrv: EBookService,
    private readonly recordSrv: BorrowerRecordService,
  ) {}

  private readonly logger = new Logger(IntegrateService.name);

  public async handle(
    command: string,
    token: string,
    params: Array<string>,
    callback_url: string,
  ) {
    const validToken = this.configSrv.get('INTEGRATE_SLACK_TOKEN_VALID') || '';
    const isValidToken = validToken === token;
    if (!isValidToken) {
      throw new Http400Exception(
        'Invalid token for this command! Contact to admin to known more!',
      );
    }
    if (params.length !== 3) {
      throw new Http400Exception(
        'Invalid parameter (Need to have 3 parameters: [module] [action] [info])',
      );
    }
    if (!commandInfo[command]) {
      throw new Http400Exception(`Command \`/${command}\` is invalid!`);
    }
    if (!commandInfo[command][params[0]]) {
      throw new Http400Exception(`Module \`/${params[0]}\` is invalid!`);
    }
    if (!commandInfo[command][params[0]][params[1]]) {
      throw new Http400Exception(`Action \`/${params[1]}\` is invalid!`);
    }
    // Handle command
    const msgBlocks = [];
    if (params[0] === 'accounts') {
      if (params[1] === 'email') {
        const account = await this.accountSrv.findByEmail(params[2], 2);
        msgBlocks.push({
          content: `\`\`\`\nName:  ${account.name}\nEmail: ${account.email}\`\`\``,
        });
      }
    }
    if (params[0] === 'io') {
      if (params[1] === 'remainIn') {
        const ios = await this.recordSrv.advancedSearch(Number(params[2]));
        const msgs = ios.map(
          (i) =>
            `[${i.account.email}] - ${i.account.name} - ${i.books.length} books`,
        );
        if (msgs.length) {
          msgBlocks.push({
            content: `\`\`\`Have ${msgs.length} users.\n------\n${msgs.join(
              '\n',
            )}\`\`\``,
          });
        } else {
          msgBlocks.push({
            content: `No user have to return their books in ${params[2]} days`,
          });
        }
      }
    }
    const { description } = commandInfo[command][params[0]][params[1]];
    await this.sendMessageToSlack(callback_url, null, description, msgBlocks);
  }

  public async sendMessageToSlack(
    hook: string,
    name: string,
    description: string,
    blocks: Array<SlackMsgBlockDto>,
  ) {
    const contentBlocks = blocks.map((b) => {
      if (b.type === 'divider') return { type: 'divider' };
      if (b.type === 'link')
        return {
          type: 'actions',
          elements: [
            {
              type: 'button',
              text: {
                type: 'plain_text',
                text: b.content,
                url: b.url,
              },
            },
          ],
        };
      return {
        type: 'section',
        text: {
          type: 'mrkdwn',
          text: b.content,
        },
      };
    });
    const dataBlocks = [
      {
        type: 'section',
        text: {
          type: 'mrkdwn',
          text: description,
        },
      },
      {
        type: 'divider',
      },
      ...contentBlocks,
    ];
    const body = {
      username: name,
      blocks: dataBlocks,
    };
    const url = hook || this.configSrv.get('INTEGRATE_SLACK_INCOMING_HOOK');
    const action = this.postAction(url, body).pipe(map((res) => res.data));
    try {
      await lastValueFrom(action);
    } catch (e) {
      this.logger.debug(e);
      throw new Http400Exception('Invalid payload from slack');
    }
  }

  private postAction(url: string, body: any): Observable<AxiosResponse> {
    return this.httpService.post(url, body, {
      headers: { 'Content-Type': 'application/json' },
    });
  }
}

import { MailerService } from '@nestjs-modules/mailer';
import { Injectable } from '@nestjs/common';
import { Account } from '../account/account.entity';
import email from '../../utils/constants/email';
import { Http503Exception } from '../../utils/Exceptions/http503.exception';
import { ConfigService } from '@nestjs/config';

const { CREATE_PASSWORD, REMIND_RETURN, INVITE, MAP_PURPOSE_INFO } = email;

@Injectable()
export class MailService {
  constructor(
    private mailerService: MailerService,
    private readonly configSrv: ConfigService,
  ) {}

  async send(user: Account, purpose: string, ...options) {
    const emailInfo = MAP_PURPOSE_INFO[purpose];
    if (!emailInfo) {
      throw new Http503Exception('Invalid purpose to send email to user');
    }
    const variables = this.generateData(purpose, options);

    return await this.mailerService.sendMail({
      to: user.email,
      subject: emailInfo.title,
      template: `./${emailInfo.template}`,
      context: {
        name: user.name,
        ...variables,
      },
    });
  }

  private generateData(purpose: string, info: any) {
    if (purpose === CREATE_PASSWORD) {
      const token = info[0];
      const domain = this.configSrv.get('APP_FRONTEND_URL') || '';
      return {
        url: `${domain}/auth/create-password?token=${token}`,
      };
    }
    if (purpose === INVITE) {
      const token = info[0];
      const domain = this.configSrv.get('APP_FRONTEND_URL') || '';
      const url = `${domain}/auth/create-password?token=${token}`;
      const inviter = info[1];
      return {
        url,
        inviter: `${inviter.name} (${inviter.email})`,
        role: info[2],
      };
    }
    if (purpose === REMIND_RETURN) {
      const { days } = info;
      return { days };
    }
    return {};
  }
}

import { IsEnum, IsOptional, IsString } from 'class-validator';
import { Level } from '../task.enum';
export class CreateDto {
  @IsString()
  title: string;

  @IsString()
  desc: string;

  @IsOptional()
  level: string;
}

export default CreateDto;

import { Body, Controller, Get, Param, Post } from '@nestjs/common';
import CreateDto from './dto/create.dto';
import GetByIdParamsDto from './dto/idParam.dto';
import { Task } from './task.entity';
import { TaskService } from './task.service';

@Controller('task')
export class TaskController {
  constructor(private readonly taskService: TaskService) {}

  @Get('/:id')
  async getAllTask(
    @Param() { id }: GetByIdParamsDto,
  ): Promise<{ data: Task[]; count: number }> {
    try {
      const [data, count] = await this.taskService.getAll(id);
      return {
        data,
        count,
      };
    } catch (e) {
      throw e;
    }
  }

  @Post('/:id')
  async createTask(
    @Param() { id }: GetByIdParamsDto,
    @Body() data: CreateDto,
  ): Promise<Task> {
    try {
      const task = await this.taskService.addNew(id, { ...data });
      return task;
    } catch (e) {
      throw e;
    }
  }
}

import { Column, Entity, PrimaryColumn, BeforeInsert } from 'typeorm';
import { nanoid } from 'nanoid';
import { Level } from './task.enum';

@Entity({ name: 'task' })
export class Task {
  @PrimaryColumn({
    name: 'id',
  })
  public id: string;

  @Column()
  public title: string;

  @Column()
  public description: string;

  @Column({
    name: 'level',
    enum: Level,
    default: Level.Normal,
  })
  public level: string;

  @Column()
  public author: string;

  @Column({
    name: 'is_done',
    default: false,
  })
  public is_done: boolean;

  @BeforeInsert()
  updateID() {
    const idStr = nanoid(16);
    this.id = `TSKx${idStr}`;
  }
}

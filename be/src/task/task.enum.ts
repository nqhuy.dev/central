export enum Role {
  Admin = 'admin',
  User = 'user',
}

export enum Level {
  Important = 0,
  Medium = 1,
  Normal = 2,
}

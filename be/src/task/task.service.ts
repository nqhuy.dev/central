import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Task } from './task.entity';
import { TaskRepository } from './task.repository';
import CreateDto from './dto/create.dto';

@Injectable()
export class TaskService {
  constructor(
    @InjectRepository(TaskRepository)
    private readonly taskRepository: TaskRepository,
  ) {}

  public async getAll(author: string): Promise<[data: Task[], count: number]> {
    const result = await this.taskRepository
      .createQueryBuilder()
      .orderBy('level', 'ASC')
      .where('author = :author', { author })
      .getMany();

    return [result, result.length];
  }

  public async addNew(id: string, data: CreateDto): Promise<Task> {
    const newTask = new Task();
    newTask.title = data.title;
    newTask.description = data.desc;
    newTask.level = data.level;
    newTask.author = id;
    const result = await this.taskRepository.save(newTask);
    return result;
  }

  public async delete(id: string, author: string): Promise<boolean> {
    const task = await this.taskRepository.delete({
      id,
      author,
    });
    return !!task;
  }
}

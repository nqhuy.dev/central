import { Exception } from './exception';
import { HttpStatus } from './httpStatus.enum';

/**
 * BAD REQUEST - INVALID REQUEST
 */
export class Http400Exception extends Exception {
  constructor(message = '') {
    super(HttpStatus.BAD_REQUEST, message);
  }
}

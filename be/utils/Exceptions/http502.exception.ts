import { HttpStatus } from './httpStatus.enum';
import { Exception } from './exception';

/**
 * UNKNOWN ERROR APPEAR WHEN HANDLING USER'S REQUEST - 502
 */
export class Http502Exception extends Exception {
  constructor(message = '') {
    super(HttpStatus.BAD_GATEWAY, message);
  }
}

const CREATE_PASSWORD = 'createPassword';
const REMIND_RETURN = 'remindReturn';
const INVITE = 'invite';

const MAP_PURPOSE_INFO = {
  [CREATE_PASSWORD]: {
    title: 'Create new password',
    template: 'createPassword',
  },
  [INVITE]: {
    title: 'You are invited to join eLib-application',
    template: 'invite',
  },
  [REMIND_RETURN]: {
    title: 'Remind return books',
    template: 'remindReturn',
  },
};

export default {
  MAP_PURPOSE_INFO,
  INVITE,
  CREATE_PASSWORD,
  REMIND_RETURN,
};

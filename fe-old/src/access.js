/**
 * @see https://umijs.org/zh-CN/plugins/plugin-access
 * */
export default function access(initialState) {
  const { currentAccount } = initialState || {};
  return {
    canAdmin: currentAccount && currentAccount.role === 'admin',
  };
}

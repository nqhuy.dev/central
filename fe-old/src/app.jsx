import React from 'react';
import { PageLoading } from '@ant-design/pro-layout';
import { history } from 'umi';
import * as Sentry from '@sentry/browser';
import { Integrations } from '@sentry/tracing';
import {
  AppstoreAddOutlined, BookTwoTone, FileSearchOutlined, TeamOutlined,
} from '@ant-design/icons';
import { getCurrentInfo } from './services/account';
import RightContent from './components/RightContent';
import Footer from './components/Footer';

const nonAuthorizedPath = [
  '/auth',
  '/about',
];
/** 获取用户信息比较慢的时候会展示一个 loading */

export const initialStateConfig = {
  loading: <PageLoading />,
};
/**
 * @see  https://umijs.org/zh-CN/plugins/plugin-initial-state
 * */

const getCurrentAccount = async () => {
  const account = await getCurrentInfo();
  return account;
};

export async function getInitialState() {
  try {
    // Sentry
    Sentry.init({
      dsn: 'https://cb58aac1efca4adf8ad33e183a1a170d@o1073611.ingest.sentry.io/6073320',
      integrations: [new Integrations.BrowserTracing()],

      // Set tracesSampleRate to 1.0 to capture 100%
      // of transactions for performance monitoring.
      // We recommend adjusting this value in production
      tracesSampleRate: 1.0,
    });

    // Get current account by default
    // Should be use cookie with 'HttpOnly' flag to prevent XSS attack
    const isHaveToken = document.cookie.includes('_authElib');

    if (!nonAuthorizedPath.includes(history.location.pathname)) {
      if (!isHaveToken) {
        history.push(nonAuthorizedPath[0]);
        return { settings: {} };
      }
      const currentAccount = await getCurrentAccount();
      if (!currentAccount) {
        history.push(nonAuthorizedPath[0]);
        return { settings: {} };
      }
      return {
        getCurrentAccount,
        currentAccount,
        settings: {},
      };
    }

    return {
      getCurrentAccount,
      currentAccount: {},
      settings: {},
    };
  } catch (e) {
    // eslint-disable-next-line no-console
    console.log(e);
    return { settings: {} };
  }
} // ProLayout 支持的api https://procomponents.ant.design/components/layout

export const layout = ({ initialState }) => {
  if (!initialState || !initialState.currentAccount) {
    return null;
  }
  const menuItems = initialState.currentAccount.role === 'admin'
    ? [
      {
        path: '/admin/books',
        name: 'booksMgnt',
        icon: <BookTwoTone />,
        exact: true,
      },
      {
        path: '/admin/io',
        name: 'ioMgnt',
        icon: <AppstoreAddOutlined />,
        exact: true,
      },
      {
        path: '/admin/accounts',
        name: 'accountsMgnt',
        icon: <TeamOutlined />,
        exact: true,
      },
      {
        path: '/admin/e-books',
        name: 'ebooksMgnt',
        icon: <FileSearchOutlined />,
        exact: true,
      },
    ]
    : [
      {
        path: '/books',
        name: 'books',
        icon: 'book',
        exact: true,
      },
    ];
  return {
    rightContentRender: () => <RightContent />,
    disableContentMargin: false,
    waterMarkProps: {
      content: initialState.currentAccount.name,
    },
    menuDataRender: (items) => {
      console.log(items);
      return menuItems;
    },
    footerRender: () => <Footer />,
    onPageChange: () => {
      // const { location } = history; // 如果没有登录，重定向到 login

      // if (!initialState?.currentAccount && location.pathname !== loginPath) {
      //   history.push(loginPath);
      // }
    },
    links: [],
    menuHeaderRender: undefined,
    unAccessible: <div>unAccessible</div>,
    ...initialState?.settings,
  };
};

import React from 'react';
import { connect } from 'dva';
import PropTypes from 'prop-types';
import { Redirect } from 'react-router-dom';
import { history, useModel } from 'umi';

const AuthWrapper = function AuthWrapper({ children, ...props }) {
  const { initialState: { currentAccount } } = useModel('@@initialState');
  const { account: { current }, dispatch } = props;
  if (!current && !currentAccount) {
    const isSeenAbout = localStorage.getItem('ELIB_SEEN_ABOUT');
    if (!isSeenAbout) {
      return <Redirect to="/about" />;
    }
    return <Redirect to="/auth" />;
  }

  let currentAccountRole = 'user';
  if (!current) {
    // Re-update currentAccount in store if it is null
    dispatch({ type: 'account/saveCurrent', payload: currentAccount });
    currentAccountRole = currentAccount.role;
  } else {
    currentAccountRole = current.role;
  }

  if (history.location.pathname === '/') {
    return <Redirect to={`/${currentAccountRole}`} />;
  }
  if (React.isValidElement(children)) {
    return React.cloneElement(children, props);
  }
  // eslint-disable-next-line no-console
  console.error('Nothing to render. Contact to admin to known more');
  return null;
};

AuthWrapper.propTypes = {
  account: PropTypes.object,
  dispatch: PropTypes.func,
  children: PropTypes.node,
};

AuthWrapper.defaultProps = {
  account: {
    current: {},
  },
  dispatch: () => {},
  children: () => {},
};

export default connect(({ account }) => ({ account }))(AuthWrapper);

export default {
  // 'menu.welcome': 'Welcome',
  // For admin page
  'menu.booksMgnt': 'Book Management',
  'menu.ioMgnt': 'IO Management',
  'menu.accountsMgnt': 'Account Management',
  'menu.ebooksMgnt': 'eBook Management',
  'menu.books': 'My books',
};

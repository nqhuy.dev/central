import { notification } from 'antd';
import {
  getCurrentInfo,
  authValid,
} from '@/services/account';

export default {
  namespace: 'account',

  state: {
    current: null,
    accounts: {},
  },

  effects: {
    * getCurrentInfo ({ isNotify }, { call, put }) {
      const response = yield call(getCurrentInfo, payload);
      if (!response || response.status) {
        if (isNotify) {
          notification.error({ message: response.message || 'Something went wrong when try to get your information' });
        }
        return false;
      }
      yield put({
        type: 'saveCurrent',
        payload: response,
      });
      return response;
    },

    * authLogin ({ payload }, { call, put }) {
      try {
        const response = yield call(authValid, payload);
        if (!response || response.status) {
          if (payload.isNotify) {
            notification.error({ message: response.message || 'Something went wrong when try to validate your account' });
          }
          return false;
        }
        yield put({
          type: 'saveCurrent',
          payload: response,
        });
        return response.role;
      } catch (e) {
        notification.error({ message: 'Something went wrong when trying to validate your account' });
        console.error(e);
        return false;
      }
    }
  },

  reducers: {
    saveCurrent(state, { payload }) {
      return {
        ...state,
        current: payload,
        accounts: {
          ...state.accounts,
          [payload.id]: payload,
        },
      };
    },
  },
};

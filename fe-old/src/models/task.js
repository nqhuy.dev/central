import { notification } from 'antd';
import {
  getAll,
  add,
} from '../services/task';

export default {
  namespace: 'task',

  state: {
    task: {},
  },

  effects: {
    * getAll({ payload }, { call, put }) {
      const response = yield call(getAll, payload);
      if (!response || response.status) {
        notification.error({ message: response ? response.message : 'Something went wrong when try to get your information' });
        return false;
      }
      const saveData = {};
      response.data.forEach((d) => {
        saveData[d.id] = d;
      });
      yield put({
        type: 'saveTasks',
        payload: saveData,
      });
      return response;
    },
    * add({ payload }, { call, put }) {
      const response = yield call(add, payload);
      if (!response || response.status) {
        notification.error({ message: response ? response.message : 'Something went wrong when try to get your information' });
        return false;
      }
      yield put({
        type: 'saveTask',
        payload: response,
      });
      return response;
    },
  },

  reducers: {
    saveTask(state, { payload }) {
      return {
        ...state,
        task: {
          ...state.task,
          [payload.id]: payload,
        },
      };
    },
    saveTasks(state, { payload }) {
      return {
        ...state,
        task: {
          ...state.task,
          ...payload,
        },
      };
    },
  },
};

import catchError from '@/utils/ErrorBoundary';
import {
  Button, Checkbox, Input, Select,
} from 'antd';
import React from 'react';

import styles from './index.less';

const statusOptions = [
  { value: '0', label: 'Important' },
  { value: '1', label: 'Medium' },
  { value: '2', label: 'Normal' },
];

@catchError
class Task extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      isDone: false,
      currentLevel: '2',
      title: '',
      desc: '',
    };
  }

  checkTask = (e) => {
    e.stopPropagation();
    this.setState({ isDone: !!e.target.checked });
  };

  handleSelectLevel = (value) => {
    this.setState({ currentLevel: value });
  };

  onChangeInput = field => (value) => {
    this.setState({ [field]: value.target.value });
  }

  onSubmit = () => {
    const { title, desc, currentLevel } = this.state;
    const { onAdd } = this.props;
    onAdd({ title, desc, level: currentLevel });
  }
 
  render() {
    const { add, data = {} } = this.props;
    const { isDone, currentLevel } = this.state;
    return (
      <div
        className={`${styles.task} ${add ? styles.add : ''}`}
        data-done={isDone}
        data-level={currentLevel}
      >
        <div className={styles.taskTitleAndStatus}>
          <div className={styles.taskTitle}>
            { add ? <Input placeholder="Task title here..." onChange={this.onChangeInput('title')} /> : data.title }
          </div>
          <div className={styles.taskStatus}>
            <Checkbox onClick={this.checkTask} />
          </div>
        </div>
        <div className={styles.taskContent}>
          <Input.TextArea defaultValue={data.description} onChange={this.onChangeInput('desc')} placeholder="What will you need to do to finish this task?" onClick={this.handleNoClickable} />
        </div>
        <div className={styles.taskActions}>
          <Select
            options={statusOptions}
            value={currentLevel}
            defaultValue={data.level}
            onChange={this.handleSelectLevel}
            onClick={this.handleNoClickable}
          />
          { add && <Button type="primary" onClick={this.onSubmit}>Save this task</Button> }
        </div>
      </div>
    );
  }
}

export default Task;

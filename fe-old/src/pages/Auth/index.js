import { SwapLeftOutlined, SwapRightOutlined } from '@ant-design/icons';
import {
  Form, Input, Button,
} from 'antd';
import { connect } from 'dva';
import React from 'react';
import { history } from 'umi';
import styles from './styles.less';

const formKeys = ['login', 'sendRequest'];

@connect(({ loading }) => ({
  isAuthValidating: loading.effects['account/authLogin'],
}))
class Authenticate extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      currentForm: formKeys[0],
    };
  }

  handleLogin = (values) => {
    const { dispatch } = this.props;
    console.log(values);
    dispatch({
      type: 'account/authLogin',
      payload: values,
    }).then(res => {
      if (res) {
        localStorage.setItem('ACC_ROLE', res);
        history.push(`/${res}`);
      }
    });
  }

  renderFormInfo = () => {
    const { currentForm } = this.state;
    if (currentForm === 'login') {
      return (
        <>
          <div className={styles.formInfoTitle}>Login</div>
          <div className={styles.formInfoContent}>Continue with your email, password and let's discovery your world!</div>
          <Button onClick={() => this.setState({ currentForm: 'send-request' })} type="link">
            <SwapLeftOutlined />
            Send request
          </Button>
        </>
      )
    }
    return (
      <>
        <div className={styles.formInfoTitle}>Send request</div>
        <div className={styles.formInfoContent}>Send a request to admin to create your account now!</div>
        <Button onClick={() => this.setState({ currentForm: 'login' })} type="link">
          Login
          <SwapRightOutlined style={{ color: 'var(--color-white-2' }} />
        </Button>
      </>
    )
  }

  render() {
    const { currentForm } = this.state;
    const { isAuthValidating } = this.props;
    return (
      <div className={styles.authenticate} data-form-active={currentForm}>
        <div className={styles.formsSwitch}>
          <Form onFinish={this.handleLogin} requiredMark={false} className="login">
            <Form.Item name="email" rules={[{ required: true, message: 'This field is required' }]}>
              <Input placeholder="Username" />
            </Form.Item>
            <Form.Item name="password" rules={[{ required: true, message: 'This field is required' }]}>
              <Input.Password placeholder="Password" />
            </Form.Item>
            <Button className="action" type="primary" htmlType="submit" loading={isAuthValidating}>Login</Button>
          </Form>

          <Form onFinish={this.handleSendRequest} requiredMark={false} className="send-request">
            <Form.Item name="mssv" rules={[{ required: true, message: 'This field is required' }]}>
              <Input placeholder="MSSV" />
            </Form.Item>
            <Form.Item name="username" rules={[{ required: true, message: 'This field is required' }]}>
              <Input placeholder="Username" />
            </Form.Item>
            <Button className="action" danger loading>Send</Button>
          </Form>
        </div>
        <div className={styles.formInfo}>
          {this.renderFormInfo()}
        </div>
      </div>
    );
  }
}

export default Authenticate;

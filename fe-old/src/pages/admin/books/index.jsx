import moment from 'moment';
import React from 'react';

class BooksPage extends React.Component {
  componentDidMount() {
    // Set is seen about in localStorage
    localStorage.setItem('ELIB_SEEN_ABOUT', moment().format('HH:mm:ss - DD/MM/YY'));
  }

  render() {
    return (
      <div>
        This is a page to manage books and only access by admin
      </div>
    );
  }
}

export default BooksPage;

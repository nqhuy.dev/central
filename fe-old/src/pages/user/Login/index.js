import {
  Checkbox, Form, Input, Button,
} from 'antd';
import React from 'react';
import styles from './Authenticate.less';

const formKeys = ['login', 'sendRequest'];

class Authenticate extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      currentForm: formKeys[0],
    };
  }

  render() {
    console.log(styles, 'hghgh');
    const { currentForm } = this.state;
    return (
      <div className={styles.authenticate} data-form-active={currentForm}>
        <div className="forms-switch">
          <Form onFinish={this.handleLogin} requiredMark={false} className="login">
            <Form.Item name="username" rules={[{ required: true, message: 'This field is required' }]}>
              <Input placeholder="Username" />
            </Form.Item>
            <Form.Item name="password" rules={[{ required: true, message: 'This field is required' }]}>
              <Input.Password placeholder="Password" />
            </Form.Item>
            <Form.Item name="keepLonger" noStyle valuePropName="checked">
              <Checkbox>Keep me logged in a long time</Checkbox>
            </Form.Item>
            <Button className="action" type="primary" loading>Login</Button>
          </Form>

          <Form onFinish={this.handleSendRequest} requiredMark={false} className="send-request">
            <Form.Item name="mssv" rules={[{ required: true, message: 'This field is required' }]}>
              <Input placeholder="MSSV" />
            </Form.Item>
            <Form.Item name="username" rules={[{ required: true, message: 'This field is required' }]}>
              <Input placeholder="Username" />
            </Form.Item>
            <Button className="action" danger loading>Send</Button>
          </Form>
        </div>
        <div className="form-info" />
      </div>
    );
  }
}

export default Authenticate;

import request from '../utils/request';

export async function getCurrentInfo() {
  return request('/accounts/me');
}

export async function authValid(data) {
  return request('/auth/login', {
    method: 'POST',
    data,
  })
}

import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Empty } from 'antd';

class ErrorBoundary extends Component {
  constructor(props) {
    super(props);

    this.state = {
      isError: false
    };
  }

  componentDidCatch(error, info) {
    console.log('------------------------');
    console.log(error, info);
    console.log('------------------------');
    this.setState({
      isError: true
    });
  }

  render() {
    if (this.state.isError) {
      return (
        <Empty description="Error detected" />
      );
    }
    return this.props.children;
  }
}

ErrorBoundary.propTypes = {
  children: PropTypes.node
};

const catchError = InsideComponent => props => (
  <ErrorBoundary>
    <InsideComponent {...props} />
  </ErrorBoundary>
);

export { ErrorBoundary };

export default catchError;

import { extend } from 'umi-request';
import { notification } from 'antd';

/**
 *
 */
const errorHandler = (error) => {
  const { response } = error;
  const { status } = response || {};

  if (status === 401) {
    notification.error({ message: 'Your session is expired, re-login to continue!' });
    return response;
  }
  return response;
};

/**
 * App request
 */
const request = extend({
  // eslint-disable-next-line no-undef
  prefix: API_PREFIX_URL,
  errorHandler,
  credentials: 'include',
});

export default request;

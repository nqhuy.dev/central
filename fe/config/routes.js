export default [
  // {
  //   path: '/about',
  //   exact: true,
  //   layout: false,
  //   component: './About'
  // },
  // {
  //   path: '/study',
  //   exact: true,
  //   layout: false,
  //   component: './Study'
  // },
  {
    path: '/auth',
    layout: false,
    routes: [
      {
        path: '/auth/:action',
        exact: true,
        layout: false,
        component: './Auth',
      },
      {
        path: '*',
        component: './exception/404'
      }
    ]
  },
  {
    path: '/',
    component: '../components/AuthWrapper',
    hideInMenu: true,
    hideChildrenInMenu: false,
    routes: [
      {
        path: '/',
        exact: true,
        layout: false,
        component: './user',
      },
      {
        path: '/admin',
        redirect: '/admin/books'
      },
      {
        path: '/admin/books',
        component: './admin/books',
      },
      {
        path: '/admin/io/:action',
        component: './admin/io',
      },
      {
        path: '/admin/accounts',
        component: './admin/accounts',
      },
      {
        path: '/admin/e-books',
        component: './admin/eBooks',
      },
      {
        path: '*',
        component: './exception/404'
      }
    ]
  },
  {
    path: '*',
    component: './exception/404'
  }
  // {
  //   path: '/user',
  //   layout: false,
  //   routes: [
  //     {
  //       path: '/user',
  //       routes: [
  //         {
  //           name: 'login',
  //           path: '/user/login',
  //           component: './user/Login',
  //         },
  //       ],
  //     },
  //     {
  //       component: './404',
  //     },
  //   ],
  // },
  // {
  //   path: '/welcome',
  //   name: 'welcome',
  //   icon: 'smile',
  //   component: './Welcome',
  // },
  // {
  //   path: '/admin',
  //   name: 'admin',
  //   icon: 'crown',
  //   access: 'canAdmin',
  //   component: './Admin',
  //   routes: [
  //     {
  //       path: '/admin/sub-page',
  //       name: 'sub-page',
  //       icon: 'smile',
  //       component: './Welcome',
  //     },
  //     {
  //       component: './404',
  //     },
  //   ],
  // },
];

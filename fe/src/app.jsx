import React, { useState } from 'react';
import { PageLoading } from '@ant-design/pro-layout';
import { history } from 'umi';
import * as Sentry from '@sentry/browser';
import { Integrations } from '@sentry/tracing';
import {
  AppstoreAddOutlined, BookOutlined, FileSearchOutlined, TeamOutlined, UserOutlined,
} from '@ant-design/icons';
import { getCurrentInfo } from './services/accounts';
import RightContent from './components/RightContent';
import Footer from './components/Footer';
import { ROLE_MAP } from './utils/constants/account';

const nonAuthorizedPath = [
  '/auth/sign-in',
  '/auth/register',
  '/auth/create-password',
  // '/about',
];

export const initialStateConfig = {
  loading: <PageLoading />,
};
/**
 * @see  https://umijs.org/zh-CN/plugins/plugin-initial-state
 * */

export async function getInitialState() {
  try {
    // Sentry
    /*
    Sentry.init({
      dsn: 'https://cb58aac1efca4adf8ad33e183a1a170d@o1073611.ingest.sentry.io/6073320',
      integrations: [new Integrations.BrowserTracing()],

      // Set tracesSampleRate to 1.0 to capture 100%
      // of transactions for performance monitoring.
      // We recommend adjusting this value in production
      tracesSampleRate: 1.0,
    });
    */

    // Get current account by default
    // Should be use cookie with 'HttpOnly' flag to prevent XSS attack
    const isHaveToken = document.cookie.includes('_authElib');

    if (!nonAuthorizedPath.includes(history.location.pathname)) {
      if (!isHaveToken) {
        history.push(nonAuthorizedPath[0]);
        return { settings: {} };
      }
      const response = await getCurrentInfo();
      if (!response || response.status) {
        if (response && response.status === 401) {
          history.push(nonAuthorizedPath[0]);
          return { settings: {} };
        }
      }
      return {
        getCurrentAccount: getCurrentInfo,
        currentAccount: response,
        settings: {},
      };
    }

    return {
      getCurrentAccount: getCurrentInfo,
      currentAccount: {},
      settings: {},
    };
  } catch (e) {
    // eslint-disable-next-line no-console
    console.error(e);
    return { settings: {} };
  }
} // ProLayout 支持的api https://procomponents.ant.design/components/layout

export const layout = ({ initialState }) => {
  if (!initialState || !initialState.currentAccount) {
    return {};
  }
  const menuItems = () => {
    if (initialState.currentAccount.role === ROLE_MAP.admin) {
      return [
        {
          path: '/admin/books',
          name: 'booksMgnt',
          icon: <BookOutlined />,
          exact: true,
        },
        {
          path: '/admin/accounts',
          name: 'accountsMgnt',
          icon: <TeamOutlined />,
          exact: true,
        },
        {
          path: '/admin/io',
          name: 'ioMgnt',
          icon: <AppstoreAddOutlined />,
          exact: true,
          routes: [
            {
              path: '/admin/io/list',
              name: 'list',
            },
            {
              path: '/admin/io/actions',
              name: 'actions',
            },
          ]
        },
        // {
        //   path: '/admin/e-books',
        //   name: 'ebooksMgnt',
        //   icon: <FileSearchOutlined />,
        //   exact: true,
        // },
      ];
    }
    if (initialState.currentAccount.role === ROLE_MAP.user) {
      return [
        {
          path: '/books',
          name: 'info',
          icon: <UserOutlined />,
          exact: true,
        },
      ];
    }
    return [];
  }
  // const isForceCollapsed = !initialState || !initialState.currentAccount || initialState.currentAccount.role !== ROLE_MAP.admin;

  return {
    rightContentRender: () => <RightContent />,
    disableContentMargin: false,
    waterMarkProps: {
      content: initialState.currentAccount.name,
    },
    menuDataRender: menuItems,
    footerRender: () => false,
    onPageChange: () => {
      const { location } = history;
      if ((!initialState?.currentAccount || !Object.keys(initialState.currentAccount).length) && !nonAuthorizedPath.includes(location.pathname)) {
        history.push(nonAuthorizedPath[0]);
        return;
      }
      const { role } = initialState.currentAccount;
      if (ROLE_MAP.admin === role && !(location.pathname || '').includes(`/${ROLE_MAP.admin}`)) {
        history.push('/admin');
        return;
      }

      if (ROLE_MAP.admin !== role && (location.pathname || '').includes(`/${ROLE_MAP.admin}`)) {
        history.push('/');
        return;
      }
    },  
    links: [],
    menuHeaderRender: undefined,
    unAccessible: <div>unAccessible</div>,
    ...initialState?.settings,
  };
};

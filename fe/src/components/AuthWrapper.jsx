import React from 'react';
import { connect } from 'dva';
import PropTypes from 'prop-types';
import { Redirect } from 'react-router-dom';
import { history, useModel } from 'umi';
import { ROLE_MAP } from '@/utils/constants/account';

const AuthWrapper = function AuthWrapper({ children, ...props }) {
  const { initialState } = useModel('@@initialState');
  const { accounts: { current }, dispatch } = props;
  if ((!current || !Object.keys(current).length) && (!initialState || !initialState.currentAccount || !Object.keys(initialState.currentAccount).length)) {
    // const isSeenAbout = localStorage.getItem('ELIB_SEEN_ABOUT');
    // if (!isSeenAbout) {
    //   return <Redirect to="/about" />;
    // }
    return <Redirect to="/auth/sign-in" />;
  }

  let currentAccountRole = 'user';
  if (!current || !Object.keys(current).length) {
    const { currentAccount } = initialState;
    dispatch({ type: 'account/saveDetail', payload: currentAccount });
    currentAccountRole = currentAccount.role;
  } else {
    currentAccountRole = current.role;
  }

  if (history.location.pathname === '/' && currentAccountRole !== ROLE_MAP.user) {
    return <Redirect to={`/${currentAccountRole}`} />;
  }
  if (React.isValidElement(children)) {
    return React.cloneElement(children, props);
  }
  // eslint-disable-next-line no-console
  console.error('Nothing to render. Contact to admin to known more');
  return null;
};

AuthWrapper.propTypes = {
  accounts: PropTypes.object,
  dispatch: PropTypes.func,
  children: PropTypes.node,
};

AuthWrapper.defaultProps = {
  accounts: {
    current: {},
  },
  dispatch: () => {},
  children: () => {},
};

export default connect(({ accounts }) => ({ accounts }))(AuthWrapper);

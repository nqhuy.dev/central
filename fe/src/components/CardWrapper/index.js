import catchError from '@/utils/ErrorBoundary';
import { LoadingOutlined, SearchOutlined } from '@ant-design/icons';
import { Button, Card, Input } from 'antd';
import React from 'react';

import styles from './index.less';

@catchError
class CardWrapper extends React.Component {
  renderCardHeader = () => {
    const { onSearch, actions } = this.props;
    return (
      <div className={styles.cardHeader}>
        {
          onSearch
            ? (
              <div className={styles.search}>
                <Input
                  size={onSearch.size || 'middle'}
                  placeholder={onSearch.placeholder || 'Find something...'}
                  onChange={({ target }) => onSearch.search(target.value || '')}
                  suffix={onSearch.loading ? <LoadingOutlined spin /> : <SearchOutlined />}
                />
              </div>
            )
            : null
        }
        <div className={styles.actions}>
          {
            actions
              ? actions.map(act => (
                <Button key={`card-wrapper-${act.type}-${act.size}`} size={act.size || 'middle'} type={act.type || 'default'} icon={act.icon || null} onClick={act.click}>
                  {act.content || ''}
                </Button>
              ))
              : null
          }
        </div>
      </div>
    )
  }

  render() {
    const { onSearch, actions, children } = this.props;
    return (
      <Card className={styles.cardWrapper}>
        { onSearch || actions ? this.renderCardHeader() : null }
        <div className={styles.cardContent}>
          { children }
        </div>
      </Card>
    )
  }
}

export default CardWrapper

import { Modal } from 'antd';
import React from 'react';

import styles from './styles.less';

/**
 * Modal center
 * We use it to have the same of modal in system -> Consistently
 */
class ModalCustom extends React.Component {
  render() {
    const { visible, position, title, className, onCancel, children, ...modalProps } = this.props;
    if (!visible) return null;
    return (
      <Modal
        visible={visible}
        title={title}
        onCancel={onCancel}
        className={`${styles.modalCustom} ${className || ''}`}
        data-modal-custom
        data-modal-position={position || 'center'}
        footer={false}
        {...modalProps}
      >
        {children}
      </Modal>
    )
  }
}

export default ModalCustom;

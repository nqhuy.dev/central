import React, { useCallback } from 'react';
import { useDispatch } from 'react-redux';
import { LogoutOutlined, SettingOutlined, UserOutlined } from '@ant-design/icons';
import { Avatar, Menu, Spin } from 'antd';
import PropTypes from 'prop-types';
import { history, useModel } from 'umi';
import { stringify } from 'querystring';
import HeaderDropdown from '../HeaderDropdown';
import styles from './index.less';
import { connect } from 'dva';

/**
 * 退出登录，并且将当前的 url 保存
 */
const loginOut = async () => {
  if (window.location.pathname !== '/auth/sign-in') {
    history.push('/auth/sign-in');
  }
};

const AvatarDropdown = function AvatarDropdown({ menu, dispatch }) {
  const { initialState, setInitialState } = useModel('@@initialState');
  const onMenuClick = useCallback(
    (event) => {
      const { key } = event;

      if (key === 'logout') {
        setInitialState((s) => {
          s.currentAccount = {};
        });
        dispatch({ type: 'accounts/authLogout' });
        loginOut();
        return;
      }

      history.push(`/account/${key}`);
    },
    [setInitialState],
  );
  const loading = (
    <span className={`${styles.action} ${styles.account}`}>
      <Spin
        size="small"
        style={{
          marginLeft: 8,
          marginRight: 8,
        }}
      />
    </span>
  );

  if (!initialState) {
    return loading;
  }

  const { currentAccount } = initialState;

  if (!currentAccount || !currentAccount.name) {
    return loading;
  }

  const menuHeaderDropdown = (
    <Menu className={styles.menu} selectedKeys={[]} onClick={onMenuClick}>
      {/* {menu && (
        <Menu.Item key="center">
          <UserOutlined />
          My account
        </Menu.Item>
      )}
      {menu && (
        <Menu.Item key="settings">
          <SettingOutlined />
          Setting
        </Menu.Item>
      )}
      {menu && <Menu.Divider />} */}

      <Menu.Item key="logout">
        <LogoutOutlined />
        Logout
      </Menu.Item>
    </Menu>
  );
  return (
    <HeaderDropdown overlay={menuHeaderDropdown}>
      <span className={`${styles.action} ${styles.account}`}>
        <Avatar size="small" className={styles.avatar} src={currentAccount.avatar} alt="avatar" />
        <span className={`${styles.name} anticon`}>{currentAccount.name}</span>
      </span>
    </HeaderDropdown>
  );
};

AvatarDropdown.propTypes = {
  menu: PropTypes.any,
};

AvatarDropdown.defaultProps = {
  menu: null,
};

export default connect()(AvatarDropdown);

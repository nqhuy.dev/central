import { Select } from 'antd';
import React, { useState } from 'react';

import styles from './styles.less';

const SelectAdd = ({ options, onSearch, onSelect, addText, ...props }) => {
  const [isShowAddOption, setIsShowAddOption] = useState(false);
  const [keyword, setKeyword] = useState('');
  const [customOption, setCustomOption] = useState([]);

  const onCustomSearch = async (keyword) => {
    setKeyword(keyword);
    await onSearch(keyword);
    if (keyword && keyword.trim() && !(options || []).length) {
      setIsShowAddOption(true);
    } else {
      setIsShowAddOption(false);
    }
  };

  const onCustomSelect = async (_, option) => {
    await onSearch('');
    if (onSelect) {
      onSelect(_, option);
    }
    if (!option || !option.value) return;
    if (isShowAddOption) {
      const newCustomOption = { ...option, label: option.value };
      setIsShowAddOption(false);
      setCustomOption([newCustomOption]);
    }
  };

  const optionRender = isShowAddOption
    ? [{ value: keyword, label: `${addText || 'Add'} "${keyword}"` }]
    : [...options, ...customOption];

  return (
    <Select options={optionRender} onSearch={onCustomSearch} onSelect={onCustomSelect} dropdownClassName={styles.selectAddDropdown} showSearch {...props} />
  )
}

export default SelectAdd;

export default {
  // 'menu.welcome': 'Welcome',
  // For admin page
  'menu.booksMgnt': 'Book Management',
  'menu.ioMgnt': 'IO Management',
  'menu.ioMgnt.list': 'List',
  'menu.ioMgnt.actions': 'Actions',
  'menu.accountsMgnt': 'Account Management',
  'menu.ebooksMgnt': 'eBook Management',
  'menu.info': 'Information',
};

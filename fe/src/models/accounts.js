import { notification } from 'antd';
import {
  getCurrentInfo,
  getDetailById,
  getAll,
  updateAccount,
  updateAccountStatus,
  authValid,
  authLogout,
  register,
  createByAdmin,
  createPassword,
} from '@/services/accounts';

import {
  getAll as getIO,
} from '@/services/io';

export default {
  namespace: 'accounts',

  state: {
    accountMap: {},
    accountList: [],
    current: {},
  },

  effects: {
    * authLogin ({ payload }, { call, put }) {
      try {
        const response = yield call(authValid, payload);
        if (!response || response.status) {
          if (payload.isNotify) {
            notification.error({ message: response.message || 'Something went wrong when try to validate your account' });
          }
          return false;
        }
        yield put({
          type: 'saveCurrent',
          payload: { ...response, isMe: true } ,
        });
        return response;
      } catch (e) {
        notification.error({ message: 'Something went wrong when trying to validate your account' });
        console.error(e);
        return false;
      }
    },
    * authLogout ({ payload }, { call, put }) {
      const response = yield call(authLogout);
      if (!response || response.status) {
        if (payload.isNotify) {
          notification.error({ message: response.message || 'Something went wrong when try to logout your account' });
        }
        return false;
      }
      yield put({
        type: 'resetState',
        payload: response,
      });
      return true;
    },

    * getCurrentInfo ({ isNotify, ...payload }, { call, put }) {
      const response = yield call(getCurrentInfo, payload);
      if (!response || response.status) {
        if (isNotify) {
          notification.error({ message: response.message || 'Something went wrong when try to get your information' });
        }
        return false;
      }
      yield put({
        type: 'saveDetail',
        payload: { ...response, isMe: true },
      });
      return response;
    },

    * getById ({ payload }, { call, put }) {
      try {
        const response = yield call(getDetailById, payload);
        yield put({
          type: 'saveDetail',
          payload: response,
        });
        return response;
      } catch (e) {
        notification.error({ message: `Something went wrong when try to get book's information with #${payload}`})
        console.error(e);
        return false;
      }
    },

    * get ({ payload }, { call, put }) {
      try {
        const response = yield call(getAll, payload);
        if (!response || response.status) {
          notification.error({ message: response.message || 'Something went wrong when try to get all of books in database' });
          return false;
        }
        yield put({
          type: 'saveMap',
          payload: response.data,
        });
        yield put({
          type: 'saveList',
          payload: response.data,
        });
        return response;
      } catch (e) {
        notification.error({ message: 'Something went wrong when trying to get all of books in database' });
        console.error(e);
        return false;
      }
    },
    * getIO ({ payload }, { call, put }) {
      const response = yield call(getIO, payload);
      if (!response || response.status) {
        notification.error({ message: response.message || "Something went wrong when try to get borrower's history" });
        return false;
      }
      const IOs = response.data.map(io => io.books.map(book => ({ ...book, account: io.account, bookId: book.id, id: io.id })).flat()).flat();
      yield put({
        type: 'accounts/saveIO',
        payload: {
          id: payload.account,
          io: IOs,
        },
      });
      return response;
    },

    
    // ================================== CREATE =====================================
    * register({ payload }, { call }) {
      const response = yield call(register, payload);
      if ((!response && response !== false) || response.isError) {
        notification.error({ message: response.message || 'Something went wrong when send request to create a new account' });
        return false;
      }
      return response;
    },
    * createByAdmin({ payload }, { call }) {
      const response = yield call(createByAdmin, payload);
      if ((!response && response !== false) || response.isError) {
        notification.error({ message: response.message || 'Something went wrong when send request to create a new account' });
        return false;
      }
      return response;
    },
    * createPassword({ payload }, { call }) {
      const { token, password } = payload;
      const response = yield call(createPassword, token, password);
      if ((!response && response !== false) || response.isError) {
        notification.error({ message: response.message || 'Something went wrong when send request to create password for this account' });
        return false;
      }
      return response;
    },
    
    // ================================== UPDATE =====================================


    * update({ payload }, { call, put }) {
      try {
        const { id, ...data } = payload;
        const response = yield call(updateAccount, id, data);
        yield put({
          type: 'saveDetail',
          payload: response,
        });
        return response;
      } catch (e) {
        notification.error({ message: `Something went wrong when edit account #${id}` });
        console.error(e);
        return false;
      }
    },

    * updateStatus({ payload }, { call, put }) {
      const { id, status } = payload;
      const response = yield call(updateAccountStatus, id, status);
      if ((!response && response !== false) || response.isError) {
        notification.error({ message: response.message || `Something went wrong when update status for this account #${id}` });
        return false;
      }
      if (!response) {
        notification.error({ message: `Account was ${status} failed!` });
        return false;
      }
      yield put({
        type: 'saveStatus',
        payload,
      });
      return response;
    }
  },

  reducers: {
    saveCurrent(state, { payload }) {
      const { isMe } = payload;
      if (isMe) {
        return {
          ...state,
          myId: payload.id,
          current: payload,
          accountMap: {
            ...state.accountMap,
            [payload.id]: payload,
          },
        }
      }
      return {
        ...state,
        current: payload,
        accountMap: {
          ...state.accountMap,
          [payload.id]: payload,
        },
      };
    },
    saveDetail(state, { payload }) {
      const { isMe } = payload;
      if (isMe) {
        return {
          ...state,
          myId: payload.id,
          accountMap: {
            ...state.accountMap,
            [payload.id]: payload,
          },
        }
      }
      return {
        ...state,
        accountMap: {
          ...state.accountMap,
          [payload.id]: payload,
        },
      };
    },
    saveStatus(state, { payload }) {
      const field = payload.status === 'active' ? 'is_waiting_accept' : 'is_locked';
      const fieldValue = payload.status === 'lock' ? true : false;
      return {
        ...state,
        accountMap: {
          ...state.accountMap,
          [payload.id]: {
            ...state.accountMap[payload.id],
            [field]: fieldValue
          }
        }
      }
    },
    saveIO(state, { payload }) {
      return {
        ...state,
        accountMap: {
          ...state.accountMap,
          [payload.id]: {
            ...state.accountMap[payload.id],
            IOs: payload.io,
          },
        },
      };
    },

    saveMap(state, { payload }) {
      const mapSave = {};
      (payload || []).forEach(account => {
        mapSave[account.id] = account;
      });
      return {
        ...state,
        accountMap: {
          ...state.accountMap,
          ...mapSave,
        }
      }
    },
    deleteAccount(state, { payload }) {
      const { id } = payload;
      return {
        ...state,
        accountMap: {
          ...state.accountMap,
          [id]: undefined,
        },
        accountList: state.accountList.length
          ? state.accountList.filter(accountId => accountId !== id)
          : [],
      }
    },
    saveList(state, { payload }) {
      const listSave = (payload || []).map(account => account.id);
      return {
        ...state,
        accountList: [...listSave],
      };
    },
    resetState() {
      return {
        accountMap: {},
        accountList: [],
      }
    }
  },
};

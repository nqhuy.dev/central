import { notification } from 'antd';
import {
  getDetailById,
  getAll,
  getSources,
  getCategories,
  addBook,
  updateBook,
  updateBookStatus,
} from '@/services/books';

export default {
  namespace: 'books',

  state: {
    bookList: [],
    bookMap: {},
    categoryList: [],
    categoryMap: {},
    sourceList: [],
    sourceMap: {},
  },

  effects: {
    * getById ({ payload }, { call, put }) {
      try {
        const response = yield call(getDetailById, payload);
        yield put({
          type: 'saveDetail',
          payload: response,
        });
        return response;
      } catch (e) {
        notification.error({ message: `Something went wrong when try to get book's information with #${payload}`})
        console.error(e);
        return false;
      }
    },

    * get ({ payload }, { call, put }) {
      try {
        const response = yield call(getAll, payload);
        if (!response || response.status) {
          notification.error({ message: response.message || 'Something went wrong when try to get all of books in database' });
          return false;
        }
        yield put({
          type: 'saveMap',
          payload: response.data,
        });
        yield put({
          type: 'saveList',
          payload: response.data,
        });
        return response;
      } catch (e) {
        notification.error({ message: 'Something went wrong when trying to get all of books in database' });
        console.error(e);
        return false;
      }
    },
    * getCategories (_, { call, put }) {
      const response = yield call(getCategories);
      if (!response || response.status || !response.count) {
        return [];
      }
      yield put({
        type: 'saveCategories',
        payload: response.data,
      });
      return response;
    },
    * getSources (_, { call, put }) {
      const response = yield call(getSources);
      if (!response || response.status || !response.count) {
        return [];
      }
      yield put({
        type: 'saveSources',
        payload: response.data,
      });
      return response.data;
    },

    
    // ================================== CREATE =====================================


    * add({ payload }, { call, put }) {
      try {
        const response = yield call(addBook, payload);
        if (!response || response.status) {
          notification.error({ message: response.message || 'Failed to add a new book' });
          return false;
        }
        yield put({
          type: 'saveDetail',
          payload: response,
        });
        notification.success({ message: 'Add a new book successfully' });
        return response;
      } catch (e) {
        notification.error({ message: 'Something went wrong when add a new book to database' });
        console.error(e);
        return false;
      }
    },

    
    // ================================== UPDATE =====================================


    * update({ payload }, { call, put }) {
      try {
        const { id, ...data } = payload;
        const response = yield call(updateBook, id, data);
        yield put({
          type: 'saveDetail',
          payload: response,
        });
        return response;
      } catch (e) {
        notification.error({ message: `Something went wrong when edit book #${id}` });
        console.error(e);
        return false;
      }
    },

    * updateStatus({ payload }, { call, put }) {
      try {
        const { id, status } = payload;
        const response = yield call(updateBookStatus, id, status);
        yield put({
          type: 'saveDetail',
          payload: response,
        });
        return response;
      } catch (e) {
        notification.error({ message: `Something went wrong when update book's status #${id}` });
        console.error(e);
        return false;
      }
    }
  },

  reducers: {
    saveDetail(state, { payload }) {
      return {
        ...state,
        bookMap: {
          ...state.bookMap,
          [payload.id]: payload,
        },
      };
    },
    saveMap(state, { payload }) {
      const mapSave = {};
      (payload || []).forEach(book => {
        mapSave[book.id] = book;
      });
      return {
        ...state,
        bookMap: {
          ...state.bookMap,
          ...mapSave,
        }
      }
    },
    deleteBook(state, { payload }) {
      const { id } = payload;
      return {
        ...state,
        bookMap: {
          ...state.bookMap,
          [id]: undefined,
        },
        bookList: state.bookList.length
          ? state.bookList.filter(bookId => bookId !== id)
          : [],
      }
    },
    saveList(state, { payload }) {
      const listSave = (payload || []).map(book => book.id);
      return {
        ...state,
        bookList: listSave,
      };
    },

    // Categories
    saveCategories(state, { payload }) {
      const categoryIds = payload.map(({ id }) => id);
      const newCategoryMap = {};
      payload.forEach(category => {
        newCategoryMap[category.id] = category;
      });
      return {
        ...state,
        categoryMap: {
          ...state.categoryMap,
          ...newCategoryMap,
        },
        categoryList: categoryIds,
      }
    },

    // Sources
    saveSources(state, { payload }) {
      const sourceIds = payload.map(({ id }) => id);
      const newSourceMap = {};
      payload.forEach(source => {
        newSourceMap[source.id] = source;
      });
      return {
        ...state,
        sourceMap: {
          ...state.sourceMap,
          ...newSourceMap,
        },
        sourceList: sourceIds,
      };
    }
  },
};

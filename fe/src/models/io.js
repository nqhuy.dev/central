import { notification } from 'antd';
import {
  getAll,
  create,
  returnBooks
} from '@/services/io';
import { BOOK_STATUS } from '@/utils/constants/book';

export default {
  namespace: 'io',

  state: {
    accountIdSelected: '',
    ioMap: {},
    ioList: [],
  },

  effects: {
    * getCurrentInfo ({ isNotify }, { call, put }) {
      const response = yield call(getCurrentInfo, payload);
      if (!response || response.status) {
        if (isNotify) {
          notification.error({ message: response.message || 'Something went wrong when try to get your information' });
        }
        return false;
      }
      yield put({
        type: 'saveDetail',
        payload: response,
      });
      return response;
    },

    * getById ({ payload }, { call, put }) {
      try {
        const response = yield call(getDetailById, payload);
        yield put({
          type: 'saveDetail',
          payload: response,
        });
        return response;
      } catch (e) {
        notification.error({ message: `Something went wrong when try to get IO's information with #${payload}`})
        console.error(e);
        return false;
      }
    },

    * get ({ payload }, { call, put }) {
      try {
        const response = yield call(getAll, payload);
        if (!response || response.status) {
          notification.error({ message: response.message || 'Something went wrong when try to get all of IO in database' });
          return false;
        }
        yield put({
          type: 'saveMap',
          payload: response.data,
        });
        yield put({
          type: 'saveList',
          payload: response.data,
        });
        return response;
      } catch (e) {
        notification.error({ message: 'Something went wrong when trying to get all of IO in database' });
        console.error(e);
        return false;
      }
    },

    
    // ================================== CREATE =====================================

    * create({ payload }, { call, put }) {
      const { accountId } = payload;
      const response = yield call(create, payload);
      if ((!response && response !== false) || response.isError) {
        notification.error({ message: response.message || 'Something went wrong when create a new IO' });
        return false;
      }
      
      yield put({
        type: 'io/get',
        payload: {
          account: accountId,
          isBorrowing: '1',
        }
      });
      return response;
    },
    // ================================== UPDATE =====================================

    * returnBooks({ payload }, { call, put }) {
      const { accountId, IOId, ...data } = payload;
      const response = yield call(returnBooks, IOId, data);
      if ((!response && response !== false) || response.isError) {
        notification.error({ message: response.message || `Something went wrong when update status for this IO #${IOId}` });
        return false;
      }
      if (!response) {
        notification.error({ message: `Unknown action to continue...!` });
        return false;
      }
      if (response.id === IOId) {
        if (response.current_status === BOOK_STATUS.unknown) {
          notification.success({ message: 'Updated status for this IO to UNKNOWN' });
          yield put({
            type: 'io/get',
            payload: {
              account: accountId,
              isBorrowing: '1',
            }
          });
          return response;
        }
        if (response.current_status === BOOK_STATUS.returned) {
          notification.success({ message: 'Gave book successfully' });
          yield put({
            type: 'io/get',
            payload: {
              account: accountId,
              isBorrowing: '1',
            }
          });
          return response;
        }
        notification.error({ message: `Something went wrong when update status for this IO #${IOId}` });
        return response;
      }
      if (response.id !== IOId) {
        notification.success({ message: `Created a new record to save missed books` });
        yield put({
          type: 'io/get',
          payload: {
            account: accountId,
            isBorrowing: '1',
          }
        });
        return response;
      }
      yield put({
        type: 'io/get',
        payload: {
          account: accountId,
          isBorrowing: '1',
        }
      });
      return response;
    }
  },

  reducers: {
    saveCurrentAccount(state, { payload }) {
      return {
        ...state,
        accountIdSelected: payload,
      };
    },

    saveDetail(state, { payload }) {
      return {
        ...state,
        ioMap: {
          ...state.ioMap,
          [payload.id]: payload,
        },
      };
    },
    saveStatus(state, { payload }) {
      const field = payload.status === 'active' ? 'is_waiting_accept' : 'is_locked';
      const fieldValue = payload.status === 'lock' ? true : false;
      return {
        ...state,
        ioMap: {
          ...state.ioMap,
          [payload.id]: {
            ...state.ioMap[payload.id],
            [field]: fieldValue
          }
        }
      }
    },
    saveIO(state, { payload }) {
      return {
        ...state,
        ioMap: {
          ...state.ioMap,
          [payload.ioId]: {
            ...state.ioMap[payload.ioId],
            IOs: payload.io,
          },
        },
      };
    },

    saveMap(state, { payload }) {
      const mapSave = {};
      (payload || []).forEach(io => {
        mapSave[io.id] = io;
      });
      return {
        ...state,
        ioMap: {
          ...state.ioMap,
          ...mapSave,
        },
      }
    },
    saveList(state, { payload }) {
      const listSave = (payload || []).map(io => io.id);
      return {
        ...state,
        ioList: [...listSave],
      };
    },
    reset(state) {
      return {
        ...state,
        accountIdSelected: '',
        ioList: [],
      }
    }
  },
};

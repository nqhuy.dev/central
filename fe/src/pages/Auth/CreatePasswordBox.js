import { ROLE_MAP } from '@/utils/constants/account';
import { SwapLeftOutlined, SwapRightOutlined } from '@ant-design/icons';
import {
  Form, Input, Button, notification,
} from 'antd';
import { connect } from 'dva';
import React from 'react';
import { Link } from 'react-router-dom';
import { history, Redirect, useModel } from 'umi';
import styles from './styles.less';


const CreatePasswordBox = ({ isCreating, dispatch, location: { query } }) => {
  const handleUpdate = (values) => {
    dispatch({
      type: 'accounts/createPassword',
      payload: {...values, token: query.token },
    }).then(res => {
      if (!res || res.isError) {
        notification.error({ message: res.message || 'Your password is updated failed!' });
        return;
      }
      notification.success({ message: 'Your password is updated successfully' });
      history.push('/auth');
    });
  }

  if (!query || !query.token) {
    notification.error({ message: 'No token was found...' });
    return (
      <Redirect to="/auth" />
    )
  }

  return (
    <div className={styles.login}>
      <div className={styles.icon}>
        <img src="/login.png" alt="create-password-icon" />
      </div>
      <Form size="large" onFinish={handleUpdate} requiredMark={false} className="login">
        <Form.Item name="password" rules={[{ required: true, message: 'This field is required' }]}>
          <Input.Password placeholder="Your password..." />
        </Form.Item>
        <Button block className="action" type="primary" htmlType="submit" loading={isCreating}>Create my password</Button>
      </Form>
    </div>
  );
}

export default connect(({ loading }) => ({
  isCreating: loading.effects['accounts/createPassword'],
}))(CreatePasswordBox);

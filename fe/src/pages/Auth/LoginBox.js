import { ROLE_MAP } from '@/utils/constants/account';
import { SwapLeftOutlined, SwapRightOutlined } from '@ant-design/icons';
import {
  Form, Input, Button,
} from 'antd';
import { connect } from 'dva';
import React from 'react';
import { Link } from 'react-router-dom';
import { history, useModel } from 'umi';
import styles from './styles.less';


const LoginBox = ({ isAuthenticating, dispatch }) => {
  const handleLogin = (values) => {
    dispatch({
      type: 'accounts/authLogin',
      payload: values,
    }).then(account => {
      if (account) {
        localStorage.setItem('ACC_ROLE', account.role);
        setTimeout(() => {
          window.location.href = `/${account.role === ROLE_MAP.user ? '' : ROLE_MAP.admin}`;
        }, 500)
        return;
      }
    });
  }

  return (
    <div className={styles.login}>
      <div className={styles.icon}>
        <img src="/login.png" alt="login-icon" />
      </div>
      <Form size="large" onFinish={handleLogin} requiredMark={false} className="login">
        <Form.Item name="email" rules={[{ required: true, message: 'This field is required' }]}>
          <Input placeholder="Username" />
        </Form.Item>
        <Form.Item name="password" rules={[{ required: true, message: 'This field is required' }]}>
          <Input.Password placeholder="Password" />
        </Form.Item>
        <Button block className="action" type="primary" htmlType="submit" loading={isAuthenticating}>Login</Button>
        <Link to="/auth/register">Register an account</Link>
      </Form>
    </div>
  );
}

export default connect(({ loading }) => ({
  isAuthenticating: loading.effects['accounts/authLogin'],
}))(LoginBox);

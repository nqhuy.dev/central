import { ROLE_MAP } from '@/utils/constants/account';
import {
  Form, Input, Button, notification,
} from 'antd';
import { connect } from 'dva';
import React from 'react';
import { Link } from 'react-router-dom';
import { history } from 'umi';
import styles from './styles.less';


const RegisterBox = ({ isRegistering, dispatch }) => {
  const handleRegister = (values) => {
    dispatch({
      type: 'accounts/register',
      payload: values,
    }).then(account => {
      if (account) {
        notification.success({ message: 'Send request successfully! You will receive an email when admin accept for your account' });
        return;
      }
    });
  }

  return (
    <div className={styles.register}>
      <div className={styles.iconWrapper}>
        <div className={styles.icon}>
          <img src="/register.png" alt="register-icon" />
        </div>
      </div>
      <Form size="large" onFinish={handleRegister} requiredMark={false}>
        <Form.Item name="email" rules={[{ required: true, message: 'This field is required' }]}>
          <Input placeholder="Your email..." />
        </Form.Item>
        <Form.Item name="name" rules={[{ required: true, message: 'This field is required' }]}>
          <Input placeholder="Your name..." />
        </Form.Item>
        <Form.Item name="std_code" rules={[{ required: true, message: 'This field is required' }]}>
          <Input placeholder="Your student's code..." />
        </Form.Item>
        <Form.Item name="school" rules={[{ required: true, message: 'This field is required' }]}>
          <Input placeholder="Your school..." />
        </Form.Item>
        <Button block className="action" htmlType="submit" loading={isRegistering}>Register</Button>
        <Link to="/auth/sign-in">Access to current account</Link>
      </Form>
    </div>
  );
}

export default connect(({ loading }) => ({
  isRegistering: loading.effects['accounts/register'],
}))(RegisterBox);

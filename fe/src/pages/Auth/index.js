import React from 'react';
import NoFoundPage from '../404';
import CreatePasswordBox from './CreatePasswordBox';
import LoginBox from './LoginBox';
import RegisterBox from './RegisterBox';
import styles from './styles.less';

const Auth = (props) => {
  const { match: { params} } = props;
  if (params.action === 'sign-in') return <div className={styles.loginPage}><LoginBox {...props} /></div>;
  if (params.action === 'register') return <div className={styles.registerPage}><RegisterBox {...props} /></div>;
  if (params.action === 'create-password') return <div className={styles.loginPage}><CreatePasswordBox {...props} /></div>;
  return <NoFoundPage />
}

export default Auth;

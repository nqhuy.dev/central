import moment from 'moment';
import React from 'react';
import {
  ArrowDownOutlined, CaretRightOutlined, PauseOutlined, PlusOutlined,
} from '@ant-design/icons';
import { Button, Input } from 'antd';
import styles from './index.less';
import Task from './Task';
import { connect } from 'dva';
import catchError from '@/utils/ErrorBoundary';

const numToString = (n) => (n >= 10 ? n : `0${n}`);

function numToTime(sec) {
  if (sec < 60) return `00:${numToString(sec)}`;
  const min = Math.round(sec / 60);
  const second = sec - 60 * min;
  return `${numToString(min)}:${numToString(second)}`;
}

@catchError
@connect(({ task, loading }) => ({
  tasks: Object.values(task.task),
  onLoadingTask: loading.effects['task/getAll'],
  onAddingTask: loading.effects['task/add'],
  onDeletingTask: loading.effects['task/delete'],
}))
class AboutPage extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      isAudioPausing: false,
      isTimeCounting: false,
      isHideIntroduce: true,
      isAddingNewTask: false,
    };
    this.currentTimeCount = 0;
    this.timeInterval = null;
  }

  componentDidMount() {
    // Set is seen about in localStorage
    const currentUser = localStorage.getItem('ELIB_STUDY_USER');
    const newUserIfNotExist = `user${Date.now()}`;
    if (!currentUser) {
      localStorage.setItem('ELIB_STUDY_USER', newUserIfNotExist);
    } else {
      const { dispatch } = this.props;
      dispatch({ type: 'task/getAll', payload: currentUser || newUserIfNotExist });
    }
  }

  componentWillUnmount() {
    if (this.timeInterval || this.currentTimeCount) {
      this.currentTimeCount = 0;
      clearInterval(this.timeInterval);
    }
  }

  toggleAudio = () => {
    const { isAudioPausing } = this.state;
    this.setState({
      isAudioPausing: !isAudioPausing,
    }, () => {
      if (isAudioPausing) {
        document.getElementById('musicControlId').pause();
      } else {
        document.getElementById('musicControlId').play();
      }
    });
  };

  toggleTime = () => {
    const { isTimeCounting } = this.state;
    this.setState({
      isTimeCounting: !isTimeCounting,
    }, () => {
      if (isTimeCounting) {
        clearInterval(this.timeInterval);
      } else {
        this.timeInterval = setInterval(() => {
          this.currentTimeCount += 1;
        }, 1000);
      }
    });
  };

  addNewTask = () => {
    this.setState({ isAddingNewTask: true });
  };

  handleAddNewTask = (data) => {
    const author = localStorage.getItem('ELIB_STUDY_USER');
    const { dispatch } = this.props;
    dispatch({
      type: 'task/add',
      payload: {
        author,
        ...data,
      }
    }).then(s => {
      if (s) {
        this.setState({ isAddingNewTask: false });
      }
    })
  }

  hideIntroduce = () => {
    localStorage.setItem('ELIB_STUDY_INTRODUCE', 'hide');
    this.setState({ isHideIntroduce: true });
  };

  render() {
    const { tasks } = this.props;
    const { isAudioPausing, isHideIntroduce, isAddingNewTask } = this.state;
    const localHide = localStorage.getItem('ELIB_STUDY_INTRODUCE');
    const isLocalHide = localHide === 'hide';
    return (
      <div className={styles.wrapper}>
        <div className={styles.audioActionWrapper} data-pause={isAudioPausing}>
          <div className={styles.audioAction} role="button" tabIndex="0" onClick={() => this.toggleAudio()} aria-hidden="true">
            { isAudioPausing ? <PauseOutlined style={{ fontSize: 30, color: 'white' }} /> : <CaretRightOutlined style={{ fontSize: 30, color: 'white' }} /> }
          </div>
          <audio controls id="musicControlId">
            <source src="lofi-music.mp3" type="audio/mpeg" />
          </audio>
        </div>
        <section className={styles.main} data-hide={isHideIntroduce || isLocalHide}>
          <div className={styles.mainImage}>
            <img src="/study-flat.png" alt="study-flat" />
          </div>
          <div className={styles.mainContent}>
            <div className={styles.title}>Help you work better!</div>
            <div className={styles.desc}>
              An simple web application, what will help you work or study better!
            </div>
          </div>
          <div className={styles.hideAction} role="button" tabIndex="0" onClick={() => this.hideIntroduce()} aria-hidden="true">
            <ArrowDownOutlined style={{ fontSize: 30, color: 'white' }} />
          </div>
        </section>
        <section className={styles.studySection}>
          <div className={styles.studyBar}>
            <div className={styles.timeActionWrapper}>
              <div className={styles.timeAction} role="button" tabIndex="0" onClick={() => this.toggleTime()} aria-hidden="true" />
              <div className={`${styles.timeAction} ${styles.clear}`} role="button" tabIndex="0" onClick={() => this.toggleTime()} aria-hidden="true" />
              <div className={styles.timeContent}>
                {numToTime(this.currentTimeCount)}
              </div>
            </div>
          </div>
          <div className={styles.studyActions}>
            <div className={styles.write}>
              <Input.TextArea placeholder="Write your thoughts here..." />
            </div>
            <div className={styles.tasksManagement}>
              { tasks.map(t => <Task data={t} />) }
              { isAddingNewTask && <Task add onAdd={this.handleAddNewTask} /> }
              <Button type="primary" disabled={isAddingNewTask} onClick={this.addNewTask} className={styles.tasksManagementAdd}>
                <PlusOutlined style={{ fontSize: 25 }} />
              </Button>
            </div>
          </div>
        </section>
      </div>
    );
  }
}

export default AboutPage;

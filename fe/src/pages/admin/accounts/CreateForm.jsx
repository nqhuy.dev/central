import React from 'react';
import { Button, Form, Input, Select } from 'antd';
import { connect } from 'dva';
import catchError from '@/utils/ErrorBoundary';
import { ROLES, ROLE_MAP } from '@/utils/constants/account';

@catchError
@connect(({ loading }) => ({
  onAddingAccount: loading.effects['account/createByAdmin'],
}))
class CreateAccountForm extends React.Component {
  onSubmit = (values) => {
    const { dispatch, onCancel } = this.props;
    dispatch({
      type: 'accounts/createByAdmin',
      payload: values,
    }).then(res => {
      if (res) {
        dispatch({ type: 'accounts/get' });
        onCancel();
      }
    });
  }

  render() {
    const { onAddingAccount } = this.props;

    return (
      <Form layout="vertical" onFinish={this.onSubmit} initialValues={{ role: ROLE_MAP.admin }}>
        <Form.Item name="email" label="Username" rules={[{ required: true, message: "Please fill account's name"}]}>
          <Input type="email" placeholder="Email" />
        </Form.Item>
        <Form.Item name="name" label="Name" rules={[{ required: true, message: "Please fill account's name"}]}>
          <Input placeholder="Full name" />
        </Form.Item>
        <Form.Item name="role" label="Role" rules={[{ required: true, message: "Please select a role"}]}>
          <Select options={ROLES.map(role => ({ value: role, label: role }))} />
        </Form.Item>
        <Form.Item noStyle>
          <Button type="primary" htmlType="submit" loading={onAddingAccount}>Create</Button>
        </Form.Item>
      </Form>
    );
  }
}

export default CreateAccountForm;

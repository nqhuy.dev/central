import React from 'react';
import { Button, Empty, Form, Input, notification, Skeleton } from 'antd';
import { connect } from 'dva';
import catchError from '@/utils/ErrorBoundary';
import { ROLE_MAP } from '@/utils/constants/account';

@catchError
@connect(({ loading }) => ({
  editingBook: loading.effects['accounts/update'],
}))
class EditForm extends React.Component {
  onSubmit = (values) => {
    const { dispatch, data, onCancel } = this.props;
    const { id, name, description } = data;
    const dataUpdated = {
      id,
      name,
      ...values,
    }
    dispatch({
      type: 'accounts/update',
      payload: dataUpdated,
    }).then(res => {
      if (res) {
        notification.success({ message: 'Update information successfully' });
        onCancel();
      }
    });
  }

  render() {
    const { editingBook, loading, data } = this.props;

    if (loading) return <Skeleton active />;
    if (!data) {
      return <Empty />;
    }
    const initialValues = {
      ...data,
    }
    if (data.role === ROLE_MAP.admin) {
      return (
        <Form layout="vertical" onFinish={this.onSubmit} initialValues={initialValues}>
          <Form.Item name="name" label="Name" rules={[{ required: true, message: "Please fill account's name"}]}>
            <Input placeholder="Account's full name" />
          </Form.Item>
          <Form.Item noStyle>
            <Button block type="primary" htmlType="submit" loading={editingBook}>Update</Button>
          </Form.Item>
        </Form>
      )
    }
    return (
      <Form layout="vertical" onFinish={this.onSubmit} initialValues={initialValues}>
        <Form.Item name="name" label="Name" rules={[{ required: true, message: "Please fill account's name"}]}>
          <Input placeholder="Full name..." />
        </Form.Item>
        <Form.Item name="std_code" label="Student's code">
          <Input placeholder="Student's code..." />
        </Form.Item>
        <Form.Item name="school" label="School">
          <Input placeholder="School..." />
        </Form.Item>
        
        <Form.Item noStyle>
          <Button block type="primary" htmlType="submit" loading={editingBook}>Update</Button>
        </Form.Item>
      </Form>
    );
  }
}

export default EditForm;

import catchError from '@/utils/ErrorBoundary';
import { Empty, Skeleton, Table } from 'antd';
import { connect } from 'dva';
import React from 'react';
import Barcode from 'react-barcode';

import styles from './styles.less';

const columnIOHistory = [
  {
    title: 'Book title',
    dataIndex: 'name',
  },
  {
    title: 'Book author',
    dataIndex: 'author',
  },
];

@catchError
@connect(({ accounts, loading }, { currentId }) => ({
  data: accounts.accountMap[currentId],
  onGettingAccount: loading.effects['accounts/getById'],
  onGettingIOs: loading.effects['accounts/getIO'],
}))
class Information extends React.Component {
  render() {
    const { onGettingAccount, onGettingIOs, data } = this.props;
    if (onGettingAccount) return <Skeleton active />
    if (!data) return <Empty />

    return (
      <div className={styles.AccountInformation}>
        <div className={styles.imgContent}>
          <div className={styles.img} data-url={data.image || ''} data-user={data.role}>
            { data.image ? <img src={data.image} alt={`account-${data.id}--image`} /> : null }
          </div>
          <div className={styles.content}>
            <div className={styles.main}>
              {data.name}
            </div>
            <div className={styles.description}>
              {data.email}
            </div>
          </div>
        </div>
        {/* <div className={styles.labelContent}>
          <div className={styles.label}>
            Category
          </div>
          <div className={styles.content}>
            {categoryMap[data.category.id]?.name || 'Unknown category'}
          </div>
        </div> */}
        
        <div className={styles.divide} />

        <div className={styles.title}>IO's history</div>
        <Table columns={columnIOHistory} dataSource={data.IOs} rowKey="id" pagination={false} loading={onGettingIOs} />

        <div className={styles.divide} />

        <div className={styles.barcode}>
          <Barcode value={data.id} />
        </div>
      </div>
    )
  }
}

export default Information;

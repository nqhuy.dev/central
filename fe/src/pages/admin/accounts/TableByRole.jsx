import { ACCOUNT_STATUS, getAccountStatus } from '@/utils/constants/account';
import catchError from '@/utils/ErrorBoundary';
import { CheckOutlined, EditOutlined, LockOutlined, UnlockOutlined } from '@ant-design/icons';
import { Skeleton, Table, Tooltip } from 'antd';
import { connect } from 'dva';
import moment from 'moment';
import React from 'react';

import styles from './styles.less';

@catchError
@connect(({ accounts, loading }, { role }) => {
  return {
    accounts: accounts.accountList.map(accountId => accounts.accountMap[accountId]),
    onLoadingAccounts: loading.effects['accounts/get'],
  }
})
class TableByRole extends React.Component {
  componentDidMount() {
    this.getAccounts();
  }

  // Component actions
  getAccounts = (page) => {
    const { dispatch, role } = this.props;
    dispatch({
      type: 'accounts/get',
      payload: {
        role,
        page,
      }
    })
  }
  // Table action
  onRowAction = (record) => {
    const { onViewDetail } = this.props;
    return {
      onClick: () => {
        onViewDetail(null, record.id);
      }
    }
  }

  render() {
    const { accounts, role: roleFilter, onLoadingAccounts, onEdit, onUpdateStatus } = this.props;
    if (onLoadingAccounts && !accounts.length) return <Skeleton active />

    const columnsRender = [
      {
        title: 'Name',
        dataIndex: 'name',
      },
      {
        title: 'Email',
        dataIndex: 'email',
      },
      {
        title: 'Role',
        dataIndex: 'role',
      },
      {
        title: 'Created at',
        dataIndex: 'created_at',
        render: time => (
          <>
            {moment(time).format('HH:mm - DD/MM/YYYY')}
            <br />
            ({moment(time).fromNow()})
          </>
        )
      },
      {
        title: 'Status',
        dataIndex: 'id',
        render: (_, record) => {
          const accountStatus = getAccountStatus(record, true);
          return accountStatus;
        }
      },
      {
        title: 'Actions',
        dataIndex: 'id',
        render: (id, record) => {
          const accountStatus = getAccountStatus(record);
          if (accountStatus === ACCOUNT_STATUS.ready) {
            return (
              <div className={styles.tableActions}>
                <Tooltip title="Edit information"><EditOutlined onClick={(e) => onEdit(e, id)} className='edit-icon' /></Tooltip>
                <Tooltip title="Lock this account"><LockOutlined onClick={(e) => onUpdateStatus(e, id, true)} className='delete-icon' /></Tooltip>
              </div>
            );
          }
          if (accountStatus === ACCOUNT_STATUS.wait) {
            return (
              <div className={styles.tableActions}>
                <Tooltip title="Accept request"><CheckOutlined onClick={(e) => onUpdateStatus(e, id, false, true)} className='edit-icon' /></Tooltip>
              </div>
            )
          }
          return (
            <div className={styles.tableActions}>
              <Tooltip title="Unlock this account"><UnlockOutlined onClick={(e) => onUpdateStatus(e, id)} className='edit-icon' /></Tooltip>
            </div>
          )
        },
      }
    ];

    return (
      <Table
        className={styles.TableByRole}
        dataSource={accounts}
        columns={columnsRender}
        onRow={this.onRowAction}
        loading={onLoadingAccounts}
        rowKey="id"
      />
    )
  }
}

export default TableByRole;

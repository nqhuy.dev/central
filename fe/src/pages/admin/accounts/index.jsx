import CardWrapper from '@/components/CardWrapper';
import ModalCustom from '@/components/ModalCustom';
import catchError from '@/utils/ErrorBoundary';
import { CheckOutlined, DeleteOutlined, EditOutlined, EyeOutlined, FileAddOutlined, PlusOutlined, UserAddOutlined } from '@ant-design/icons';
import { Button, Card, Modal, notification, Skeleton, Table, Tabs, Tooltip } from 'antd';
import { connect } from 'dva';
import debounce from 'lodash.debounce';
import React from 'react';
import Information from './Information';
import CreateForm from './CreateForm';
import EditForm from './EditForm';
import { ROLES } from '@/utils/constants/account';

import styles from './styles.less';
import TableByRole from './TableByRole';

@catchError
@connect(({ accounts, loading }) => ({
  accountMap: accounts.accountMap,
  onLoadingAccounts: loading.effects['accounts/get'],
  onGettingAccount: loading.effects['accounts/getById']
}))
class AccountsPage extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      isShowModalAddAccount: false,
      accountIdViewing: null,
      accountIdEditing: null,
      accountIdUpdating: null,
    }
  }

  _debounceSearch = debounce(v => {
    const { dispatch } = this.props;
    dispatch({
      type: 'accounts/get',
      payload: {
        search: v,
      }
    })
  }, 500);

  componentDidMount() {
    const { dispatch } = this.props;
    dispatch({ type: 'accounts/get' });
  }

  // Page action

  handleEdit = (e, id) => {
    const { dispatch } = this.props;
    if (e) {
      e.stopPropagation();
    }
    this.setState({
      accountIdEditing: id,
    });
    dispatch({ type: 'accounts/getById', payload: id });
  }

  handleViewDetail = (e, id) => {
    const { dispatch } = this.props;
    if (e) {
      e.stopPropagation();
    }
    this.setState({
      accountIdViewing: id,
    });
    dispatch({ type: 'accounts/getById', payload: id });
    dispatch({ type: 'accounts/getIO', payload: { account: id } });
  }

  handleUpdateStatus = (e, id, isLock = false, isAccepting = false) => {
    const { dispatch, accountMap } = this.props;
    if (e) {
      e.stopPropagation();
    }
    const accountUpdating = accountMap[id];
    if (!accountUpdating) {
      notification.error({ message: 'No account was found to handle this action' });
      return;
    }
    this.setState({
      accountIdUpdating: id,
    });
    // Handle accept request for current account
    if (isAccepting) {
      dispatch({
        type: 'accounts/updateStatus',
        payload: { id, status: 'active'},
      });
      return;
    } 
    // Handle lock/unlock for current account
    const action = isLock ? 'lock' : 'unlock';
    Modal.warning({
      title: `You are ${action}ing "${accountUpdating.name}"`,
      content: `Click confirm to ${action} this account (or cancel if not)`,
      visible: !!this.state.accountIdUpdating,
      onOk: () => {
        dispatch({
          type: 'accounts/updateStatus',
          payload: { id, status: action }
        }).then(res => {
          if (res) {
            notification.success({ message: `Account was ${action}ed successfully` });
          } else {
            notification.error({ message: `Account was ${action}ed failed` });
          }
          this.setState({ accountIdUpdating: null });
        });
        ;
      },
      onCancel: () => this.setState({ accountIdUpdating: null }),
    });
  }

  render() {
    const { onGettingAccount, accountMap } = this.props;
    const { isShowModalAddAccount, accountIdEditing, accountIdViewing } = this.state;

    const currentEditing = accountMap[accountIdEditing] || null;
    const currentViewing = accountMap[accountIdViewing] || null;

    return (
      <div>
        <CardWrapper
          onSearch={{
            size: 'large',
            placeholder: "Find accounts with name, email (or student's code if student)...",
            search: this._debounceSearch,
          }}
          actions={[
            { size: 'large', type: 'primary', icon: <PlusOutlined />, content: 'Add a new account', click: () => this.setState({ isShowModalAddAccount: true }) },
          ]}
        >
          <TableByRole
            onViewDetail={this.handleViewDetail}
            onEdit={this.handleEdit}
            onUpdateStatus={this.handleUpdateStatus}
          />
        </CardWrapper>

        <ModalCustom
          title="Add a new account"
          visible={isShowModalAddAccount}
          onCancel={() => this.setState({ isShowModalAddAccount: false })}
        >
          <CreateForm onCancel={() => this.setState({ isShowModalAddAccount: false })} />
        </ModalCustom>

        <ModalCustom
          title={`Edit ${currentEditing ? `"${currentEditing.name}"` : 'account...'}`}
          visible={!!accountIdEditing}
          onCancel={() => this.setState({ accountIdEditing: null })}
        >
          <EditForm
            loading={onGettingAccount}
            data={currentEditing}
            onCancel={() => this.setState({ accountIdEditing: null })}
          />
        </ModalCustom>

        <ModalCustom
          title={currentViewing ? currentViewing.name : "Account's information..."}
          visible={!!accountIdViewing}
          position="right"
          onCancel={() => this.setState({ accountIdViewing: null })}
          width={480}
        >
          <Information currentId={accountIdViewing} />
        </ModalCustom>
      </div>
    );
  }
}

export default AccountsPage;

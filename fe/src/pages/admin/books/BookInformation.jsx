import { BOOK_STATUS, BOOK_STATUS_MAPPING, getBookStatus } from '@/utils/constants/book';
import catchError from '@/utils/ErrorBoundary';
import { CheckOutlined } from '@ant-design/icons';
import { Empty, Skeleton, Table } from 'antd';
import { connect } from 'dva';
import * as moment from 'moment';
import React from 'react';
import Barcode from 'react-barcode';

import styles from './styles.less';

@catchError
@connect(({ books, loading }) => ({
  // sourceList: books.sourceList.map(sourceId => books.sourceMap[sourceId]),
  // categoryList: books.categoryList.map(categoryId => books.categoryMap[categoryId]),
  sourceMap: books.sourceMap,
  categoryMap: books.categoryMap,
  loadingCategory: loading.effects['books/getCategory'],
  loadingSource: loading.effects['books/getSource'],
}))
class BookInformation extends React.Component {
  render() {
    const { loading, data, categoryMap, sourceMap, loadingCategory, loadingSource } = this.props;
    if (loading || loadingCategory || loadingSource) return <Skeleton active />
    if (!data) return <Empty />

    const columnIOHistory = [
      {
        title: 'Borrower',
        dataIndex: ['account', 'name'],
      },
      {
        title: 'Status',
        dataIndex: 'current_status',
        render: status => BOOK_STATUS_MAPPING[status] || 'Unknown',
      },
      {
        title: 'Last updated at',
        dataIndex: 'updated_at',
        render: time => moment(time).format('HH:mm - DD/MM/YYYY'),
      },
    ];
    const currentBookStatus = getBookStatus(data.borrower_record);

    return (
      <div className={styles.BookInformation}>
        <div className={styles.imgContent}>
          <div className={styles.img} data-url={data.image || ''} data-status={currentBookStatus}>
            { data.image ? <img src={data.image} alt={`book-${data.id}--image`} /> : null }
            { currentBookStatus === BOOK_STATUS.available ? <CheckOutlined /> : null }
          </div>
          <div className={styles.content}>
            <div className={styles.main}>
              {data.name}
            </div>
            <div className={styles.description}>
              {data.description || 'No description for this book'}
            </div>
          </div>
        </div>
        <div className={styles.labelContent}>
          <div className={styles.label}>
            Category
          </div>
          <div className={styles.content}>
            {categoryMap[data.category.id]?.name || 'Unknown category'}
          </div>
        </div>
        <div className={styles.labelContent}>
          <div className={styles.label}>
            Source
          </div>
          <div className={styles.content}>
            {sourceMap[data.source.id]?.name || 'Unknown source'}
          </div>
        </div>
        
        <div className={styles.divide} />

        <div className={styles.title}>IO's history</div>
        <Table columns={columnIOHistory} dataSource={data.borrower_record || []} rowKey="id" bordered />

        <div className={styles.divide} />

        <div className={styles.barcode}>
          <Barcode value={data.id} displayValue={false} />
        </div>
      </div>
    )
  }
}

export default BookInformation;

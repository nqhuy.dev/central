import React from 'react';
import { Button, Form, Input } from 'antd';
import SelectAdd from '@/components/SelectAdd';
import debounce from 'lodash.debounce';
import { connect } from 'dva';
import catchError from '@/utils/ErrorBoundary';

@catchError
@connect(({ books, loading }) => ({
  sourceList: books.sourceList.map(sourceId => books.sourceMap[sourceId]),
  categoryList: books.categoryList.map(categoryId => books.categoryMap[categoryId]),
  loadingCategory: loading.effects['books/getCategory'],
  loadingSource: loading.effects['books/getSource'],
  addingBook: loading.effects['books/add'],
}))
class CreateBookForm extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      categoryKeyword: '',
      sourceKeyword: '',
      authorKeyword: '',
    }
  }

  onSubmit = (values) => {
    const { dispatch, onCancel } = this.props;
    dispatch({
      type: 'books/add',
      payload: values,
    }).then(res => {
      if (res) {
        dispatch({ type: 'books/get' });
        dispatch({ type: 'books/getCategories' });
        dispatch({ type: 'books/getSources' });
        onCancel();
      }
    });
  }

  render() {
    const { categoryList, sourceList, loadingSource, loadingCategory, addingBook } = this.props;
    const { categoryKeyword, sourceKeyword } = this.state;

    const categoryOptions = categoryList
      .filter(c => c.name.toUpperCase().includes(categoryKeyword.toUpperCase()))
      .map(c => ({
        label: c.name,
        value: c.id,
      }));

    const sourceOptions = sourceList
      .filter(s => s.name.toUpperCase().includes(sourceKeyword.toUpperCase()))
      .map(s => ({
        label: s.name,
        value: s.id,
      }));
    return (
      <Form layout="vertical" onFinish={this.onSubmit}>
        <Form.Item name="name" label="Name" rules={[{ required: true, message: "Please fill book's name"}]}>
          <Input placeholder="Book's name" />
        </Form.Item>
        <Form.Item name="description" label="Description">
          <Input.TextArea placeholder="Book's description" />
        </Form.Item>
        <Form.Item name="author" label="Author" rules={[{ required: true, message: "Please fill book's author"}]}>
          <Input placeholder="Book's author" />
        </Form.Item>
        <Form.Item label="Category" name="category" rules={[{ required: true, message: 'Please select or create a category'}]}>
          <SelectAdd
            options={categoryOptions}
            placeholder="Book's category"
            onSearch={v => this.setState({ categoryKeyword: v })}
            addText="Create new category"
            loading={loadingCategory}
          />
        </Form.Item>
        <Form.Item label="Source" name="source" rules={[{ required: true, message: 'Please select or create a source'}]}>
          <SelectAdd
            options={sourceOptions}
            placeholder="Book's source"
            onSearch={v => this.setState({ sourceKeyword: v })}
            addText="Create new source"
            loading={loadingSource}
          />
        </Form.Item>
        <Form.Item noStyle>
          <Button type="primary" htmlType="submit" loading={addingBook}>Save</Button>
        </Form.Item>
      </Form>
    );
  }
}

export default CreateBookForm;

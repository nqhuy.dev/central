import CardWrapper from '@/components/CardWrapper';
import ModalCustom from '@/components/ModalCustom';
import { BOOK_STATUS, BOOK_STATUS_MAPPING, getBookStatus } from '@/utils/constants/book';
import catchError from '@/utils/ErrorBoundary';
import { CheckOutlined, DeleteOutlined, EditOutlined, EyeOutlined, FileAddOutlined, PlusOutlined, UserAddOutlined } from '@ant-design/icons';
import { Button, Card, Modal, notification, Skeleton, Table, Tooltip } from 'antd';
import { connect } from 'dva';
import debounce from 'lodash.debounce';
import React from 'react';
import BookInformation from './BookInformation';
import CreateBookForm from './CreateBookForm';
import EditBookForm from './EditBookForm';

import styles from './styles.less';

@catchError
@connect(({ books, loading }) => ({
  bookMap: books.bookMap,
  books: books.bookList.map(bookId => books.bookMap[bookId]),
  onLoadingBooks: loading.effects['books/get'],
  onGettingBook: loading.effects['books/getById']
}))
class BooksPage extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      isShowModalAddBook: false,
      bookIdViewing: null,
      bookIdEditing: null,
      bookIdDeleting: null,
    }
  }

  _debounceSearch = debounce(v => {
    const { dispatch } = this.props;
    dispatch({
      type: 'books/get',
      payload: {
        keyword: v,
      }
    })
  }, 500);

  componentDidMount() {
    const { dispatch } = this.props;
    dispatch({ type: 'books/get' });
    dispatch({ type: 'books/getCategories' });
    dispatch({ type: 'books/getSources' });
  }

  // Page action

  // Table action
  onRowAction = (record) => {
    return {
      onClick: () => {
        this.handleViewBook(null, record.id);
      }
    }
  }

  handleEditBook = (e, id) => {
    const { dispatch } = this.props;
    if (e) {
      e.stopPropagation();
    }
    this.setState({
      bookIdEditing: id,
    });
    dispatch({ type: 'books/getById', payload: id });
  }

  handleViewBook = (e, id) => {
    const { dispatch } = this.props;
    if (e) {
      e.stopPropagation();
    }
    this.setState({
      bookIdViewing: id,
    });
    dispatch({ type: 'books/getById', payload: id });
  }

  handleDeleteBook = (e, id) => {
    const { dispatch, bookMap } = this.props;
    if (e) {
      e.stopPropagation();
    }
    const bookDeleting = bookMap[id];
    if (!bookDeleting) {
      notification.error({ message: 'No book was found to delete' });
      return;
    }
    this.setState({
      bookIdDeleting: id,
    });
    Modal.warning({
      title: `You are deleting "${bookDeleting.name}"`,
      content: 'Click confirm to delete this book (or cancel if not)',
      visible: !!this.state.bookIdDeleting,
      onOk: () => {
        dispatch({
          type: 'books/updateStatus',
          payload: { id, status: 1 }
        }).then(res => {
          if (res) {
            notification.success({ message: 'Archive book successfully' });
          } else {
            notification.error({ message: 'Archive book failed' });
          }
          this.setState({ bookIdDeleting: null });
        });
        ;
      },
      onCancel: () => this.setState({ bookIdDeleting: null }),
    });
  }
  handleUnArchiveBook = (e, id) => {
    const { dispatch } = this.props;
    if (e) {
      e.stopPropagation();
    }
    dispatch({
      type: 'books/updateStatus',
      payload: { id, status: 0 },
    }).then(res => {
      if (res) {
        notification.success({ message: 'Un-archive book successfully' });
      }
    });
  }

  render() {
    const { books, onLoadingBooks, onGettingBook, bookMap } = this.props;
    const { isShowModalAddBook, bookIdEditing, bookIdViewing } = this.state;

    const currentEditing = bookMap[bookIdEditing] || null;
    const currentViewing = bookMap[bookIdViewing] || null;

    const columnsRendered = [
      {
        title: 'Name',
        dataIndex: 'name'
      },
      {
        title: 'Author',
        dataIndex: 'author',
      },
      {
        title: 'Category',
        dataIndex: ['category', 'name'],
      },
      {
        title: 'Source',
        dataIndex: ['source', 'name'],
        render: (name, record) => record
          ? (<>{name}{' '}{record.is_owned ? <CheckOutlined /> : ''}</>)
          : '--'
      },
      {
        title: 'Status',
        dataIndex: 'name',
        render: (_, record) => {
          if (!record) return '--';
          if (record.is_archived) return 'Archived';
          const bookStatus = getBookStatus(record.borrower_record);
          return BOOK_STATUS_MAPPING[bookStatus] || 'Available';
        }
      },
      {
        title: 'Actions',
        dataIndex: 'id',
        render: (id, record) => {
          if (record.is_archived) {
            return (
              <div className={styles.tableActions}>
                <Tooltip title="Un-archive book"><EyeOutlined onClick={e => this.handleUnArchiveBook(e, id)} /></Tooltip>
              </div>
            )
          }
          return (
            <div className={styles.tableActions}>
              <Tooltip title="View detail"><EditOutlined onClick={e => this.handleEditBook(e, id)} /></Tooltip>
              <Tooltip title="Archive book"><DeleteOutlined className='delete-icon' onClick={e => this.handleDeleteBook(e, id)} /></Tooltip>
            </div>
          )
        }
      }
    ];
    return (
      <div>
        <CardWrapper
          onSearch={{
            size: 'large',
            placeholder: 'Find your book with name, author, or category...',
            search: this._debounceSearch,
          }}
          actions={[
            { size: 'large', type: 'primary', icon: <PlusOutlined />, content: 'Add a new book', click: () => this.setState({ isShowModalAddBook: true }) },
            // { size: 'large', icon: <FileAddOutlined />, content: 'Import a file' },
          ]}
        >
          {
            onLoadingBooks
            ? (
              <Skeleton active />
            )
            : (
              <Table
                columns={columnsRendered}
                dataSource={books || []}
                rowKey={r => r ? r.id : 'row-key'}
                onRow={this.onRowAction}
              />
            )
          }
        </CardWrapper>

        <ModalCustom
          title="Add a new book"
          visible={isShowModalAddBook}
          onCancel={() => this.setState({ isShowModalAddBook: false })}
        >
          <CreateBookForm onCancel={() => this.setState({ isShowModalAddBook: false })} />
        </ModalCustom>

        <ModalCustom
          title={`Edit ${currentEditing ? `"${currentEditing.name}"` : 'book...'}`}
          visible={!!bookIdEditing}
          onCancel={() => this.setState({ bookIdEditing: null })}
        >
          <EditBookForm
            loading={onGettingBook}
            data={currentEditing}
            onCancel={() => this.setState({ bookIdEditing: null })}
          />
        </ModalCustom>

        <ModalCustom
          title={currentViewing ? currentViewing.name : "Book's information..."}
          visible={!!bookIdViewing}
          position="right"
          onCancel={() => this.setState({ bookIdViewing: null })}
          width={480}
        >
          <BookInformation loading={onGettingBook} data={currentViewing} />
        </ModalCustom>
      </div>
    );
  }
}

export default BooksPage;

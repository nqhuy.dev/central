import catchError from '@/utils/ErrorBoundary';
import { UserOutlined } from '@ant-design/icons';
import { Avatar, Button, Card, Select } from 'antd';
import { connect } from 'dva';
import { debounce } from 'lodash';
import React from 'react';
import styles from './styles.less';

@catchError
@connect(({ io, accounts, loading }) => ({
  currentAccount: io.accountIdSelected ? (accounts.accountMap[io.accountIdSelected] || null) : null,
  accounts: [...accounts.accountList].map(id => accounts.accountMap[id]),
  onLoadingAccounts: loading.effects['accounts/get'],
  onLoadingIOs: loading.effects['io/get']
}))
class AccountSelector extends React.Component {
  _searchDebounce = debounce((search) => this.handleGetAccount(search), 500);

  componentDidMount() {
    this.handleGetAccount();
  }

  handleSelect = (accountId) => {
    const { dispatch } = this.props;
    dispatch({
      type: 'io/saveCurrentAccount',
      payload: accountId,
    });
    dispatch({
      type: 'io/get',
      payload: {
        account: accountId,
        isBorrowing: '1',
      },
    });
  }

  handleGetAccount = (search = '') => {
    const { dispatch } = this.props;
    dispatch({
      type: 'accounts/get',
      payload: {
        search,
      }
    })
  }

  render () {
    const { currentAccount, accounts, onLoadingAccounts } = this.props;
    if (!currentAccount) {
      const accountOptions = accounts.map(acc => ({ label: `${acc.name} (${acc.email})`, value: acc.id }));
      return (
        <Card className={`${styles.AccountSelector} ${styles.empty}`}>
          <div className={styles.label}>
            Select an account:
            {' '}
          </div>
          <Select showSearch size="large" placeholder="Search accounts..." options={accountOptions} loading={onLoadingAccounts} onSearch={this._searchDebounce} onChange={this.handleSelect} />
        </Card>
      )
    }
    return (
      <Card className={styles.AccountSelector}>
        <Avatar src={currentAccount.avatar} icon={<UserOutlined />} size={50} />
        <div className={styles.info}>
          <div className={styles.name}>
            {currentAccount.name}
          </div>
          <div className={styles.email}>
            {currentAccount.email}
          </div>
        </div>
        <Button type="link" onClick={() => this.handleSelect()}>
          Select another
        </Button>
      </Card>
    )
  }
}

export default AccountSelector;

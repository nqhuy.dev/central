import { getBookStatus, BOOK_STATUS } from '@/utils/constants/book';
import catchError from '@/utils/ErrorBoundary';
import { CheckOutlined, CloseOutlined } from '@ant-design/icons';
import { Button, Card, Modal, notification, Select, Spin, Table } from 'antd';
import { connect } from 'dva';
import { debounce } from 'lodash';
import moment from 'moment';
import React from 'react';
import styles from './styles.less';

@catchError
@connect(({ io, accounts, books, loading }) => {
  const IOs = [...io.ioList].map(id => io.ioMap[id]);
  return {
    currentAccount: io.accountIdSelected ? (accounts.accountMap[io.accountIdSelected] || null) : null,
    IO: IOs.length ? IOs[0] : null,
    books: [...books.bookList].map(id => books.bookMap[id]),
    bookMap: books.bookMap,
    onGettingIO: loading.effects['io/get'],
  };
})
class IOActions extends React.Component {
  selectRef = React.createRef();

  constructor(props) {
    super(props);
    this.state = {
      bookGiveBackSelected: [],
      bookBorrowSelected: [],
    }
  }

  componentDidMount() {
    const { currentAccount, dispatch } = this.props;
    if (currentAccount) {
      dispatch({
        type: 'io/get',
        payload: {
          account: currentAccount.id,
          isBorrowing: '1',
        }
      });
    }
    this.handleGetBooks();
  }

  componentWillUnmount() {
    const { dispatch } = this.props;
    dispatch({ type: 'io/reset' })
  }

  _searchDebounce = debounce((search) => this.handleGetBooks(search), 500);

  handleGetBooks = (search = '') => {
    const { dispatch } = this.props;
    dispatch({
      type: 'books/get',
      payload: {
        keyword: search,
      },
    });
  }

  handleSelect = ({ borrowing }) => (bookId) => {
    const { bookGiveBackSelected, bookBorrowSelected } = this.state;
    const bookNeedReturn = [...borrowing].map(book => book.id);
    if (bookNeedReturn.includes(bookId)) {
      this.setState({
        bookGiveBackSelected: [...bookGiveBackSelected, bookId],
      })
    } else {
      this.setState({
        bookBorrowSelected: [...bookBorrowSelected, bookId],
      })
    }
  }

  handleDeselect = ({ borrowing }) => async (bookId) => {
    const { bookGiveBackSelected, bookBorrowSelected } = this.state;
    const bookNeedReturn = [...borrowing].map(book => book.id);
    if (bookNeedReturn.includes(bookId)) {
      await this.setState({
        bookGiveBackSelected: [...bookGiveBackSelected].filter(id => id !== bookId),
      })
    } else {
      await this.setState({
        bookBorrowSelected: [...bookBorrowSelected].filter(id => id !== bookId),
      })
    }
  }

  handleGetBookNeedReturn = () => {
    const { IO } = this.props;
    const bookBorrowing = [...(IO?.books || [])];
    return {
      isHasData: bookBorrowing.length > 0,
      borrowing: bookBorrowing,
      at: IO?.created_at,
      length: bookBorrowing.length,
    }
  }

  _onReturn = () => {
    const { dispatch, IO, currentAccount } = this.props;
    const { bookGiveBackSelected } = this.state;
    const booksData = this.handleGetBookNeedReturn();
    if (booksData.length > bookGiveBackSelected.length) {
      notification.warn({ message: "Please mark 'unknown' or 'missed' to continue" });
      return;
    }
    dispatch({
      type: 'io/returnBooks',
      payload: {
        IOId: IO.id,
        accountId: currentAccount.id,
        books: bookGiveBackSelected,
      }
    }).then(res => {
      if (res) {
        this.setState({
          bookBorrowSelected: [],
          bookGiveBackSelected: [],
        })
      }
    })
  }
  _onMarkMissed = () => {
    const { dispatch, IO, currentAccount } = this.props;
    const { bookGiveBackSelected } = this.state;
    const booksData = this.handleGetBookNeedReturn();

    Modal.confirm({
      title: 'Mark as MISSED...',
      content: "Do you want to submit and mark as 'missed' for remaining books!",
      okText: 'Mark as missed',
      cancelText: 'Cancel',
      onOk: () => {
        dispatch({
          type: 'io/returnBooks',
          payload: {
            IOId: IO.id,
            accountId: currentAccount.id,
            books: bookGiveBackSelected,
            forceMissed: booksData.length > bookGiveBackSelected.length,
          }
        }).then(res => {
          if (res) {
            this.setState({
              bookBorrowSelected: [],
              bookGiveBackSelected: [],
            })
          }
        })
      },
    });
    
  }
  _onMarkUnknown = () => {
    const { dispatch, IO, currentAccount } = this.props;
    const { bookGiveBackSelected } = this.state;
    const booksData = this.handleGetBookNeedReturn();

    Modal.confirm({
      title: 'Mark as unknown...',
      content: 'Do you want to submit and mark as unknown for remaining books',
      okText: 'Mark as unknown',
      cancelText: 'Cancel',
      onOk: () => {
        dispatch({
          type: 'io/returnBooks',
          payload: {
            IOId: IO.id,
            accountId: currentAccount.id,
            books: bookGiveBackSelected,
            forceUnknown: booksData.length > bookGiveBackSelected.length,
          }
        }).then(res => {
          if (res) {
            this.setState({
              bookBorrowSelected: [],
              bookGiveBackSelected: [],
            })
          }
        })
      },
    });
  }
  _onBorrowNew = () => {
    const { dispatch, currentAccount, IO } = this.props;
    const { bookBorrowSelected } = this.state;
    const booksData = this.handleGetBookNeedReturn();

    if (booksData.length) {
      Modal.confirm({
        title: 'Join with current borrowing',
        content: 'This user seems to be borrowing some other books, do you want to combine them?',
        okText: 'Combine', 
        cancelText: 'Cancel',
        onOk: () => {
          dispatch({
            type: 'io/create',
            payload: {
              accountId: currentAccount.id,
              books: bookBorrowSelected,
              isJoinWithBorrowing: '1',
            }
          }).then(res => {
            if (res) {
              this.setState({
                bookBorrowSelected: [],
                bookGiveBackSelected: [],
              })
            }
          })
        },
      });
      return;
    }
    dispatch({
      type: 'io/create',
      payload: {
        accountId: currentAccount.id,
        books: bookBorrowSelected,
      }
    }).then(res => {
      if (res) {
        this.setState({
          bookBorrowSelected: [],
          bookGiveBackSelected: [],
        })
      }
    });
  }

  renderGiveBackContent = (booksData) => {
    const { bookGiveBackSelected } = this.state;
    if (!booksData.isHasData) return null;
    return (
      <>
        <div className={styles.divide} />
        <div className={styles.title}>
          {`Book borrowed - (at ${moment(booksData.at).format('HH:mm - DD/MM/YYYY')})`}:
        </div>
        <Table
          columns={[
            {
              title: "Book's name",
              dataIndex: 'name',
              width: '75%',
            },
            {
              title: "Author",
              dataIndex: 'author',
              width: '25%',
            },
            {
              title: 'Actions',
              dataIndex: 'id',
              width: '20%',
              render: (id) => {
                if (!bookGiveBackSelected.includes(id)) {
                  return (
                    <div className='actions'>
                      <CheckOutlined onClick={() => this.handleSelect(booksData)(id)} />
                    </div>
                  )
                }
                return (
                  <div className='actions'>
                    <CloseOutlined onClick={() => this.handleDeselect(booksData)(id)} />
                  </div>
                )
              }
            }
          ]}
          dataSource={booksData.borrowing}
          pagination={false}
          rowClassName={r => bookGiveBackSelected.includes(r.id) ? '' : 'bookRow__opacity'}
          rowKey="id"
          bordered
        />
        { bookGiveBackSelected.length === booksData.length && <Button type="primary" onClick={this._onReturn}>Done</Button> }
        { bookGiveBackSelected.length > 0 && bookGiveBackSelected.length < booksData.length && <Button type="dash" onClick={this._onMarkMissed}>Submit and mark missed for remain books</Button>}
        { bookGiveBackSelected.length < booksData.length && <Button type="primary" danger onClick={this._onMarkUnknown}>Submit and mark unknown for remain books</Button>}
      </>
    )
  }

  renderBorrowContent = (booksData) => {
    const { bookMap, currentAccount } = this.props;
    const { bookBorrowSelected } = this.state;
    const bookSource = bookBorrowSelected.map(id => bookMap[id]);
    if (!bookBorrowSelected.length) return null;
    return (
      <>
        <div className={styles.divide} />
        <div className={styles.title}>
          Borrow new book(s):
        </div>
        <Table
          columns={[
            {
              title: "Book's name",
              dataIndex: 'name',
              width: '75%',
            },
            {
              title: "Author",
              dataIndex: 'author',
              width: '25%',
            },
            {
              title: 'Actions',
              dataIndex: 'id',
              width: '20%',
              render: (id) => {
                return (
                  <div className='actions'>
                    <CloseOutlined onClick={() => this.handleDeselect(booksData)(id)} />
                  </div>
                )
              }
            }
          ]}
          dataSource={bookSource}
          pagination={false}
          rowKey="id"
          bordered
        />
        <Button type="primary" onClick={this._onBorrowNew} disabled={!currentAccount}>Borrow</Button>
      </>
    )
  }

  render () {
    const { books, currentAccount, onGettingIO } = this.props;
    const { bookGiveBackSelected, bookBorrowSelected } = this.state;
    const bookOptions = books
      .filter(book => !bookGiveBackSelected.includes(book.id))
      .filter(book => !bookBorrowSelected.includes(book.id))
      .map(book => ({ label: `${book.name} - ${book.author}`, value: book.id }));
    const booksData = this.handleGetBookNeedReturn();

    if (!currentAccount && !onGettingIO) {
      return (
        <Card className={styles.IOActions}>
          Please select an account to continue...
        </Card>
      )
    }
    return (
      <Spin spinning={!!onGettingIO} tip="Checking account's status...">
        <Card className={styles.IOActions}>
          <div className={styles.header}>
            <div className={styles.info}>
              {booksData.isHasData ? `Have ${booksData.length} book${booksData.length > 1 ? 's': ''} need to give back.` : 'No books need to give back.' }
            </div>
            <div className={styles.search}>
              <Select
                onSearch={this._searchDebounce}
                options={bookOptions}
                onChange={this.handleSelect(booksData)}
                value=""
                showSearch
                placeholder='Search and select book here...'
              />
            </div>
          </div>
          {this.renderGiveBackContent(booksData)}
          {this.renderBorrowContent(booksData)}
        </Card>
      </Spin>
    )
  }
}

export default IOActions;

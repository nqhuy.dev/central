import { BOOK_STATUS } from '@/utils/constants/book';
import catchError from '@/utils/ErrorBoundary';
import { CheckOutlined, WarningOutlined } from '@ant-design/icons';
import { Card, Switch, Table, Tooltip } from 'antd';
import { connect } from 'dva';
import moment from 'moment';
import React from 'react';
import { Link } from 'react-router-dom';
import { history, Redirect } from 'umi';
import styles from './styles.less';


@catchError
@connect(({ io, loading }) => ({
  data: [...io.ioList].map(id => io.ioMap[id]),
  onGettingIOs: loading.effects['io/get'],
}))
class IOList extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      isShowBorrowing: false,
    }
  }

  componentDidMount() {
    this.handleGetIOs();
  }

  handleGetIOs = () => {
    const { dispatch } = this.props;
    const { isShowBorrowing } = this.state;
    dispatch({
      type: 'io/get',
      payload: {
        isBorrowing: isShowBorrowing ? '1' : '0',
      }
    });
  }

  handleAction = (e, id) => {
    e.preventDefault();
    e.stopPropagation();
    const { dispatch } = this.props;
    dispatch({
      type: 'io/saveCurrentAccount',
      payload: id,
    });
    history.push('/admin/io/actions');
  }

  handleSwitch = (s) => {
    this.setState({
      isShowBorrowing: s,
    }, () => this.handleGetIOs());
  }

  render () {
    const { data, onGettingIOs } = this.props;
    const { isShowBorrowing } = this.state;
    const columns = [
      {
        title: 'Account',
        dataIndex: 'account',
        render: account => (
          <Link
            to={`/admin/accounts?view=${account.id}`}
            onClick={e => {
              e.preventDefault();
              e.stopPropagation();
            }}
          >
            {account.name}
          </Link>
        ),
      },
      {
        title: 'Num of books',
        align: 'center',
        dataIndex: 'books',
        render: books => books && Array.isArray(books) ? books.length : '--',
      },
      {
        title: 'Borrowed at',
        dataIndex: 'created_at',
        render: time => moment(time).format('HH:mm - DD/MM/YYYY'),
      },
      {
        title: 'Actions',
        align: 'center',
        dataIndex: 'id',
        render: (_, record) => {
          if (record.current_status === BOOK_STATUS.unknown) {
            return <Tooltip title="This record is unknown"><WarningOutlined color="var(--color-red-3)" /></Tooltip>
          }
          if (record.current_status === BOOK_STATUS.borrowing) {
            return (
              <Tooltip title="Handle this IO">
                <CheckOutlined
                  onClick={e => this.handleAction(e, record.account.id)}
                  className='edit-icon'
                />
              </Tooltip>
            );
          }
          return '--';
        },
      }
    ]

    return (
      <Card>
        <Switch checkedChildren="Show only borrowing" unCheckedChildren="Show all" onChange={this.handleSwitch} checked={isShowBorrowing} />
        <Table columns={columns} dataSource={data} rowClassName={record => `ioRow__${record.current_status}`} loading={onGettingIOs} className={styles.IOList} rowKey="id" />
      </Card>
    )
  }
}

export default IOList;

import catchError from '@/utils/ErrorBoundary';
import React from 'react';
import AccountSelector from './AccountSelector';
import IOActions from './IOActions';
import IOList from './IOList';

@catchError
class IOMgntPage extends React.Component {
  render() {
    const { match: { params: { action } } } = this.props;

    if (action === 'list') {
      return (
        <IOList />
      )
    }
    if (action === 'actions') {
      return (
        <div>
          <AccountSelector />
          <IOActions />
        </div>
      );
    }
  }
}

export default IOMgntPage;

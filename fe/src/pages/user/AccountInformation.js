import catchError from '@/utils/ErrorBoundary';
import { LogoutOutlined, UserOutlined } from '@ant-design/icons';
import { Avatar, Card } from 'antd';
import { connect } from 'dva';
import React from 'react';
import styles from './styles.less';

@catchError
@connect(({ accounts }) => ({
  account: accounts.accountMap[accounts.myId] || {},
}))
class AccountInformation extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      accountId: '',
    }
  }

  componentDidMount() {
    const { dispatch } = this.props;
    dispatch({ type: 'accounts/getCurrentInfo' });
  }

  handleLogout = () => {
    const { dispatch } = this.props;
    dispatch({ type: 'accounts/authLogout' }).then((res) => {
      if (res) {
        window.location.href = '/auth';
      }
    });
  }

  render () {
    const { account } = this.props;
    return (
      <Card className={styles.AccountInformation}>
        <div className={styles.accountMain}>
          <div className={styles.avatar}>
            <Avatar src={account.avatar} icon={<UserOutlined />} size={90} />
          </div>
          <div className={styles.info}>
            <div className={styles.name}>
              {account.name || '--- --- ---'}
            </div>
            <div className={styles.desc}>
              {account.email || '--@--.--'}
            </div>
          </div>
        </div>

        <div className={styles.divide} />
        <div className={styles.actions}>
          <div className={styles.actionLogout} onClick={this.handleLogout}>
            <LogoutOutlined />
          </div>
        </div>

        <div className={styles.divide} />

      </Card>
    )
  }
}

export default AccountInformation;

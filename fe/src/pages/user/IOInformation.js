import { BOOK_STATUS } from '@/utils/constants/book';
import catchError from '@/utils/ErrorBoundary';
import { Card, Table } from 'antd';
import { connect } from 'dva';
import moment from 'moment';
import React from 'react';
import styles from './styles.less';

@catchError
@connect(({ accounts, io, loading }) => ({
  currentAccount: accounts.accountMap[accounts.myId],
  IOs: [...io.ioList].map(id => io.ioMap[id]),
  onGettingAccount: loading.effects['accounts/getCurrentInfo'],
  onGettingIOs: loading.effects['io/get'],
}))
class IOInformation extends React.Component {
  componentDidMount() {
    const { dispatch, currentAccount } = this.props;
    dispatch({
      type: 'io/get',
      payload: {
        account: currentAccount.id,
      },
    });
  }
  render () {
    const { IOs } = this.props;
    const borrowing = IOs.find(io => io.current_status === BOOK_STATUS.borrowing);
    let bookDataIds = [];
    const bookData = IOs.map(io => {

      const ioBooks = io.books
        .filter(book => !bookDataIds.includes(book.id))
        .map(book => ({ ...book, borrowStatus: io.current_status }));
      bookDataIds = [...bookDataIds, ...ioBooks.map(book => book.id)];
      return ioBooks;
    }).flat();
    return (
      <Card className={styles.IOInformation}>
        { borrowing ? `You have ${borrowing.books.length} book(s) need to give back (${moment().to(moment(borrowing.created_at).add(3, 'months'))})` : 'You have not borrowed any books currently!' }
        { borrowing && (
          <Table
            columns={[
              { title: 'Name', dataIndex: 'name', width: '75%' },
              { title: 'Author', dataIndex: 'author', width: '25%' }
            ]}
            dataSource={borrowing.books}
            pagination={false}
            bordered
            rowKey="id"
          />
        )}

        <div className={styles.divide} />
        <div className={styles.title}>
          You read {bookData.length} books!
        </div>
        <Table
          columns={[
            { title: 'Name', dataIndex: 'name', width: '75%' },
            { title: 'Author', dataIndex: 'author', width: '25%' }
          ]}
          dataSource={bookData}
          pagination={false}
          rowClassName={record => `bookRow__${record.borrowStatus}`}
          bordered
          rowKey="id"
        />
      </Card>
    )
  }
}

export default IOInformation;

import catchError from '@/utils/ErrorBoundary';
import { Card, Spin } from 'antd';
import { connect } from 'dva';
import React from 'react';
import AccountInformation from './AccountInformation';
import IOInformation from './IOInformation';
import styles from './styles.less';

@catchError
@connect(({ accounts, loading }) => ({
  currentAccountId: accounts.myId,
  onGettingAccount: loading.effects['accounts/getCurrentInfo'],
}))
class UserPage extends React.Component {
  render() {
    const { currentAccountId, onGettingAccount } = this.props;
    return (
      <div className={styles.UserPage}>
        <AccountInformation />
        {
          !currentAccountId
          ? (
            <Spin spinning={onGettingAccount}>
              <Card className={styles.IOInformation} />
            </Spin>
          )
          : <IOInformation />
        }
      </div>
    );
  }
}

export default UserPage;

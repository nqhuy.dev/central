import request from '../utils/request';
import qS from 'query-string';

export async function getCurrentInfo() {
  return request('/accounts/me');
}

export const getDetailById = (id) => {
  return request(`/accounts/${id}`);
}

export const getAll = (query) => {
  return request(`/accounts?${qS.stringify(query)}`);
}

export const getIO = (account) => {
  return request(`/accounts?account=${account}`);
}

// ====================== POST ===========================

export async function authValid(data) {
  return request('/auth/login', {
    method: 'POST',
    data,
  })
}

export const authLogout = () => {
  return request('/auth/logout', {
    method: 'POST'
  });
}

export const register = (data) => {
  return request('/accounts', {
    method: 'POST',
    data,
  })
}
export const createByAdmin = (data) => {
  return request('/accounts/create-account', {
    method: 'POST',
    data,
  })
}

// ====================== PATCH ===========================

export const updateAccount = (id, data) => {
  return request(`/accounts/${id}`, {
    method: 'PATCH',
    data,
  });
}

export const updateAccountStatus = (id, status) => {
  return request(`/accounts/${id}/status/${status}`, {
    method: 'PATCH',
  });
}

export const createPassword = (token, password) => {
  return request(`/accounts/create-password/${token}`, {
    method: 'PATCH',
    data: {
      password,
    }
  })
}

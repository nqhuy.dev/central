import request from '@/utils/request';
import qS from 'query-string';

export const getDetailById = (id) => {
  return request(`/books/${id}`);
}

export const getAll = (query) => {
  return request(`/books?${qS.stringify(query)}`);
}

export const getSources = () => {
  return request('/books/i/sources');
}

export const getCategories = () => {
  return request('/books/i/categories');
}

// ====================== POST ===========================

export const addBook = (data) => {
  return request('/books', {
    method: 'POST',
    data,
  })
}

// ====================== PATCH ===========================

export const updateBook = (id, data) => {
  return request(`/books/${id}`, {
    method: 'PATCH',
    data,
  });
}

export const updateBookStatus = (id, status) => {
  return request(`/books/${id}/status`, {
    method: 'PATCH',
    data: { status },
  });
}

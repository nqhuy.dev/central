import request from '../utils/request';
import qS from 'query-string';


export const getDetailById = (id) => {
  return request(`/io/${id}`);
}

export const getAll = (query) => {
  return request(`/io?${qS.stringify(query)}`);
}

export const getIO = (account) => {
  return request(`/io?account=${account}`);
}

// ====================== POST ===========================
export const create = (data) => {
  return request('/io', {
    method: 'POST',
    data,
  })
}

// ====================== PATCH ===========================

export const returnBooks = (id, data) => {
  return request(`/io/${id}/return-io`, {
    method: 'PATCH',
    data,
  });
}

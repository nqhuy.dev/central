import request from '../utils/request';

export async function getAll(id) {
  return request(`/task/${id}`);
}

export async function add({ author, ...data }) {
  return request(`/task/${author}`, {
    method: 'POST',
    data,
  });
}

export async function authValid(data) {
  return request('/auth/login', {
    method: 'POST',
    data,
  });
}

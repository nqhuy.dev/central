export const ROLES = ['admin', 'user'];

export const ROLE_MAP = {
  admin: 'admin',
  user: 'user',
};

export const ACCOUNT_STATUS = {
  ready: 'ready',
  locked: 'locked',
  wait: 'wait',
}
export const ACCOUNT_STATUS_TEXT = {
  ready: 'Ready',
  locked: 'Locked',
  wait: 'Waiting to accept',
};

export const getAccountStatus = ({ is_locked, is_waiting_accept }, isReturnContext) => {
  if (is_locked) return isReturnContext ? ACCOUNT_STATUS_TEXT.locked : ACCOUNT_STATUS.locked;
  if (is_waiting_accept) return isReturnContext ? ACCOUNT_STATUS_TEXT.wait : ACCOUNT_STATUS.wait;
  return isReturnContext ? ACCOUNT_STATUS_TEXT.ready : ACCOUNT_STATUS.ready;
}
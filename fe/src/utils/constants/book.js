
export const BOOK_STATUS = {
  borrowing: 'borrowing',
  returned: 'returned',
  missed: 'missed',
  unknown: 'unknown',
  available: 'available',
}

export const BOOK_STATUS_MAPPING = {
  borrowing: 'Borrowing',
  missed: 'Missed',
  unknown: 'Unknown',
  available: 'Available',
}

export const getBookStatus = (borrower_record) => {
  const record = borrower_record && Array.isArray(borrower_record)
    ? [...borrower_record]
    : [];
  if (record.find(r => r.current_status === BOOK_STATUS.borrowing)) {
    return BOOK_STATUS.borrowing;
  }
  if (record.find(r => r.current_status === BOOK_STATUS.unknown)) {
    return BOOK_STATUS.unknown;
  }
  if (record.find(r => r.current_status === BOOK_STATUS.missed)) {
    return BOOK_STATUS.missed;
  }
  return BOOK_STATUS.available;
}
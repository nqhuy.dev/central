import { extend } from 'umi-request';
import { notification } from 'antd';

/**
 *
 */
const errorHandler = (error) => {
  const { response, data } = error;
  const { status, statusText } = response || data || {};

  if (status === 401) {
    notification.error({ message: 'Your session is expired, re-login to continue!' });
    return { isError: true, status: 401, message: 'Your session is expired, re-login to continue!' };
  }
  if (status === 404 && statusText === 'Not Found') {
    notification.error({ message: 'Your action can not handle because it was not implemented' });
    return { isError: true, status: 404, message: 'Your action can not handle because it was not implemented' };
  }
  const message = response.message || data.message || ''
  return {
    ...response,
    message: message && Array.isArray(message)
      ? message.join('\n') 
      : message,
    isError: true
  };
};

/**
 * App request
 */
const request = extend({
  // eslint-disable-next-line no-undef
  prefix: API_PREFIX_URL,
  errorHandler,
  credentials: 'include',
});

export default request;
